import axios from "axios";

  /*DEV Kong*/   const UPL_REST_API_URLD = 'https://apigw-ext.bankmuamalat.co.id:443';
  //*Local*/ const UPL_REST_API_URLD = 'http://localhost:19131';
  //*UAT Kong*/const UPL_REST_API_URLD = 'https://apigwint.bankmuamalat.co.id:443';

export default axios.create({
  baseURL: UPL_REST_API_URLD,
  headers: {
    "Content-type": "application/json"
  }
});
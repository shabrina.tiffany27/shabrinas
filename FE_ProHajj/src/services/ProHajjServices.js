import axios from 'axios';
import http from "./http-common";

  /*DEV Kong */  const UPL_REST_API_URLF = 'https://apigw-ext.bankmuamalat.co.id:443/eformntb';
  /*DEV Kong */  const UPL_REST_API_URLS = 'https://apigw-ext.bankmuamalat.co.id:443/eformsts';
  // /*DEV Kong */  const UPL_REST_API_URLF = 'http://10.55.54.8:7199/eformntb';
  //*Local*/const UPL_REST_API_URLF = 'http://localhost:19130/eformntb';
  //*UAT Kong*/  const UPL_REST_API_URLF = 'https://apigwint.bankmuamalat.co.id:443/eformntb';
  /*DEV Kong*/  const UPL_REST_API_URLV = 'https://apigw-ext.bankmuamalat.co.id:443/validasi';
//*Local*/const UPL_REST_API_URLV = 'http://localhost:19117/validasi';
//*UAT Kong*/  const UPL_REST_API_URLV = 'https://apigwint.bankmuamalat.co.id:443/validasi';

class ProHajjServices {

  upload(file, appid, onUploadProgress) {

    let formData = new FormData();

    formData.append("file", file);
    //console.log("upload : URL/eformupload/"+appid+",  formDatat +>"+JSON.stringify(formData));
    return http.post("/eformupload/" + appid, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
      onUploadProgress,
    });
  }

  getDataReferal(refcode) {
    //console.log("getDataReferal +>"+refcode);
    return axios.get(UPL_REST_API_URLF + '/CekRef/' + refcode);
  }

  postDataEform(data) {
    //console.log("postDataEform: URL/inpEformCust +>"+JSON.stringify(data));
    return axios.post(UPL_REST_API_URLF + '/inpEformCust', data);
  }

  postTempEform(data) {
    //console.log("postTempEform: URL/inpEformCustTemp +>"+JSON.stringify(data));
    return axios.post(UPL_REST_API_URLF + '/inpEformCustTemp', data);
  }

  getDataKTP(ektp) {
    //console.log("getDataReferal +>"+ektp);
    return axios.get(UPL_REST_API_URLF + '/CekExData/' + ektp);
  }

  getDataExistKTP(ektp) {
    //console.log("getDataExistKTP: URL/CekKTP/"+ektp);
    return axios.get(UPL_REST_API_URLF + '/CekKTP/' + ektp);
  }

  getTempAccnumKTP(ektp) {
    //console.log("getDataReferal +>"+ektp);
    return axios.get(UPL_REST_API_URLF + '/CekKTPTemp/' + ektp);
  }

  getListWilayah(wildt) {
    //console.log("getListWilayah Post: URL/wilayah +>"+JSON.stringify(wildt));
    return axios.post(UPL_REST_API_URLF + '/wilayah', wildt);
  }
  getEformTempApid(appid) {
    //console.log("getEformTempApid: URL/getappidtemp/"+appid);
    return axios.get(UPL_REST_API_URLF + '/getappidtemp/' + appid);
  }
  getEformApid(appid) {
    //console.log("getEformApid: URL/getappid/"+appid);
    return axios.get(UPL_REST_API_URLF + '/getappid/' + appid);
  }
  getMasterProvKel(data, data2) {
    const message = { wil: data, nama: data2 };
    //console.log("getMasterProvKel Post: URL/wilayah +>"+JSON.stringify(message));
    return axios.post(UPL_REST_API_URLF + '/wilayah', message);
    //console.log("getMasterProvKel get: URL/wilayah/"+data+"/"+data2);
    //return axios.get(UPL_REST_API_URLF+'/wilayah/'+data+"/"+data2);
  }

  validateToken(data) {
    //console.log("validateToken: URL/validate +>"+JSON.stringify(data));
    return axios.post(UPL_REST_API_URLV + '/validate', data);
  }

  getTokenSenById(mode, appid, mail) {
    //console.log("getTokenSenById: URL/token/"+mode+"/"+appid+" +>"+JSON.stringify(mail));
    return axios.post(UPL_REST_API_URLV + '/token/' + mode + '/' + appid, mail);
  }
  getEformParsing(urlparam) {
    //console.log("getEformParsing: URL/"+urlparam);
    return axios.get(UPL_REST_API_URLF + '/' + urlparam);
  }
  postEformRevisi(data) {
    //console.log("postEformRevisi: URL/eformrevisi +>"+JSON.stringify(data));
    return axios.post(UPL_REST_API_URLF + '/eformrevisi', data);
  }

  getUrlview() {
    return UPL_REST_API_URLF;
  }

  getBdgUsh() {
    //console.log("getEformParsing: URL/"+urlparam);
    return axios.get(UPL_REST_API_URLF + '/getbidangusaha');
  }

  postNik(norek) {
    //console.log("getEformParsing: URL/"+urlparam);
    return axios.post('http://10.55.54.8:19130/eformntb/generatecust/'+norek)
  }


  getStatusPengajuan(appid) {
    // return axios.get("http://10.55.54.8:19150/eformntb/getappid/" + appid);
    return axios.get(UPL_REST_API_URLS + '/getappid/' + appid);
  }

}
export default new ProHajjServices();
import React from 'react';
import './App.css';
import CustAcqProhajjMain from './components/CustAcqProhajjMain'
import { BrowserRouter as Router} from "react-router-dom";
import { ToastContainer} from 'react-toastify';
function App() {
  return (
    <div className="App">
      <ToastContainer 
      position="bottom-right"
      autoClose={5000}
      pauseOnHover
      draggable={false}
      pauseOnFocusLoss={false}/>
      <Router path='/' component={CustAcqProhajjMain} />
      <CustAcqProhajjMain/>
    </div>
  );
}

export default App;

import React, { Component } from 'react'
import {Modal, Row, Col, Form} from 'react-bootstrap'
import NumberFormat from 'react-number-format';
import TextField from '@material-ui/core/TextField';

export class ModalACCconfirm extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep(1);
    }
    render() {
        const radioSize = {fontSize: 14};
        const { handleChange } = this.props
        const isHide = (this.props.flgac === "Y");
        const isDisabled = (this.props.flgac === "T" || parseInt(this.props.accn.replace(/[^\w,]/gi, "").length) > 9);
        return (
                <Modal 
                    show={this.props.macc}
                    onHide={()=>this.props.handleModalAcc}
                    backdrop="static"
                    keyboard={false}>
                    <Modal.Header><b>Konfirmasi</b></Modal.Header>
                    <Modal.Body>
                    <Row>
                        <Col md>
                        <div className="p-2 col-example text-center"
                            dangerouslySetInnerHTML={{__html: "<b>Sudah Memiliki Rekening di Bank Muamalat?</b>"
                            }}/>
                        </Col>
                    </Row>
                    <Row>
                    <Col md>
                      <div className="p-2 col-example text-center">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                          <td style={{width:'50%',textAlign:'center'}}>
                            <Form.Check className="radio"
                              required
                              type="radio"
                              label="YA"
                              name="custflg"
                              id="yarekno"
                              style={radioSize}
                              defaultValue={'Y'}
                              onChange={handleChange("custflg")}
                            /></td>
                          <td style={{width:'50%',textAlign:'center'}}>
                            <Form.Check className="radio"
                              required
                              type="radio"
                              label="TIDAK"
                              name="custflg"
                              id="norekno"
                              style={radioSize}
                              defaultValue={'T'}
                              onClick={this.props.radioChange}
                              onChange={handleChange("custflg")}
                            /></td>
                        </tr>
                        </tbody>
                        </table>
                        </div>
                        </Col>
                    </Row>
                    <hr hidden={!isHide} />
                    <Row className="d-flex justify-content-start" >
                      <Col md></Col>
                        <Col md = '6'>
                            <Form.Group>
                                <NumberFormat customInput={TextField}
                                    format="##########"
                                    hinttext=""
                                    placeholder=""
                                    label="Nomor Rekening"
                                    //type="number"
                                    //required
                                    inputProps={{style: { textAlign: 'center'}}}
                                    hidden={!isHide}
                                    fullWidth
                                    size="small"
                                    value={this.props.accn}
                                    name="accnumktp"
                                    onChange={handleChange("accnumktp")}
                                    variant="outlined"
                                    helperText="Rekening Bank Muamalat"
                                />
                            </Form.Group>
                        </Col>
                        <Col md></Col>
                    </Row>
                    </Modal.Body>
                    <Modal.Footer>
                    <button className="btn btn-outline-success btn-sm" 
                            type='button' 
                            id='accconfirm'
                            disabled={!isDisabled}
                            onClick={this.continue}
                            title='Lanjut'>Lanjut</button>
                    </Modal.Footer>
                </Modal>
        )
    }
}

export default ModalACCconfirm
import React, { Component } from 'react'
import {Container, Card} from 'react-bootstrap'
import Header from './Header'
import Footer from './Footer'
import { ToastContainer, toast } from 'react-toastify';


export class Message extends Component {
    carWarning =()=>{
        var sRtn = true;
        var terr = this.props.message;
        var iniserw = terr.substring(0,1) 
        if (iniserw === '#'){
            sRtn = false;
        }else {
            sRtn = true;
        }
    return sRtn ;
    }
    cobaLagi = e =>{
        e.preventDefault();
        var terr = this.props.message;
        var iniser = terr.substring(0,2)
        if (iniser === '#P'){
            this.props.handleSubmit();
        }else {
            this.props.loadData();
        }
    }
    componentDidMount() {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
          });
        toast.error(this.props.message.substring(2));
      }
    render() {
        return (
            <div>
                <Header/>
                <Container >
                <div ><ToastContainer 
                 /*   position="bottom-right"
                  type="default"
                  autoClose={30000}
                  hideProgressBar={false}
                  newestOnTop={false}
                  closeOnClick
                  pauseOnHover*/
                /></div>
                <div className="shadow p-3 mb-5 bg-white rounded">
                <Card>
                    <Card.Header>
                        <div className="p-2 col-example text-center">
                            <h5>PERHATIAN</h5>
                        </div>
                    </Card.Header>
                    <Card.Body>
                        <div className="p-2 col-example text-center">
                            <h5>{this.props.message.substring(2)}</h5><br/>
                            <button hidden={this.carWarning()} id='tryagnbutton' className="btn btn-outline-success btn-sm" onClick={this.cobaLagi}>Coba Lagi</button>
                        </div>
                    </Card.Body>
                </Card>
                </div>
                </Container>
                <Footer/>
            </div>
        )
    }
}

export default Message
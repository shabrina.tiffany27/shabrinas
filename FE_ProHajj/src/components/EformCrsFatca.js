import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {TextField, MenuItem} from '@material-ui/core/';
import ModalConfirm from './ModalConfirm';
//import { ToastContainer } from 'react-toastify';

export class EformCrsFatca extends Component {
    continue = e => {
        e.preventDefault();
        this.props.handleSaveTemp();
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.onTop();  
    }
    
    render() {
        //const numOnly = /^[0-9\b]+$/;
        const { handleChangeFC, handleSaveTemp } = this.props
        const isRequired = (this.props.mfac.funding.fcrgcard === 'YA')
        const isRequitax = (this.props.mfac.funding.fctax === 'YA')

        return (
            <div>
                <Header prod = {this.props.mfac.kd_produk} eprod = {this.props.mfac.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.mfac.batal}/>
                <Form onSubmit={this.continue} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'80%',textAlign:'left'}}><h5>Deklarasi Common Reporting Standards (CRS) {'&'} (FATCA)</h5></td>
                            <td style={{width:'20%',textAlign:'right'}}>Page 3</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                            <div className="p-2 col-example text-left"
                                dangerouslySetInnerHTML={{__html: "<b>Anda memiliki Permanent Residence/Green Card?</b>"
                                }}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Jawab"
                                        required
                                        fullWidth
                                        value={this.props.mfac.funding.fcrgcard}
                                        name="fcrgcard"
                                        onChange={handleChangeFC}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'YA'}>YA</MenuItem>
                                            <MenuItem value={'TIDAK'}>TIDAK</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                <TextField
                                    label="Negara"
                                    required={isRequired}
                                    disabled={!isRequired}
                                    fullWidth
                                    value={this.props.mfac.funding.fcrgcardcountry}
                                    name="fcrgcardcountry"
                                    onChange={handleChangeFC}
                                    variant="standard"
                                    size="small"
                                    helperText={isRequired?"Jika Ya, Sebutkan negara yang dimaksud":""}
                                />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                            <div className="p-2 col-example text-left"
                                dangerouslySetInnerHTML={{__html: "<b>Anda memiliki kewajiban pelaporan pajak di Negara luar Indonesia?</b>"
                                }}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Jawab"
                                        required
                                        fullWidth
                                        value={this.props.mfac.funding.fctax}
                                        name="fctax"
                                        onChange={handleChangeFC}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'YA'}>YA</MenuItem>
                                            <MenuItem value={'TIDAK'}>TIDAK</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Negara"
                                        required={isRequitax}
                                        disabled={!isRequitax}
                                        fullWidth
                                        value={this.props.mfac.funding.fctaxcountry}
                                        name="fctaxcountry"
                                        onChange={handleChangeFC}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'USA'}>USA</MenuItem>
                                            <MenuItem value={'CRS Countries'}>CRS Countries</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Tax Identification Number"
                                        required={isRequitax}
                                        disabled={!isRequitax}
                                        fullWidth
                                        value={this.props.mfac.funding.fctaxid}
                                        name="fctaxid"
                                        onChange={handleChangeFC}
                                        variant="standard"
                                        size="small"
                                        />
                                </Form.Group>
                            </Col>
                        </Row><Row>
                            <Col md>
                            <div className="p-2 col-example text-left"
                                dangerouslySetInnerHTML={{__html: "<b>Apakah Anda memberikan surat Instruksi di Bank manapun untuk melakukan"
                                +" transfer dana pada rekening yang terdapat pada Bank di luar negara Indonesia?</b>"
                                }}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Jawab"
                                        required
                                        fullWidth
                                        value={this.props.mfac.funding.fctrsletter}
                                        name="fctrsletter"
                                        onChange={handleChangeFC}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'YA'}>YA</MenuItem>
                                            <MenuItem value={'TIDAK'}>TIDAK</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='submit' 
                                            id='continue'
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformCrsFatca

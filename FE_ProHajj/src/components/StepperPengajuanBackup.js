import React, { useState } from "react";
import { Box, Stepper, Step, StepLabel } from "@material-ui/core/";
import "../styles/stepper.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faExclamationTriangle, faTimes } from "@fortawesome/free-solid-svg-icons";
import { solid, regular, brands } from '@fortawesome/free-solid-svg-icons';
import { stepIcon, stepActivity, stepActivity1, stepStatus, stepLabelStat, stepLabelStat1, connectorStatus, connectorStatus1, isShow, wrapperStepperBasic, wrapperStepperBasic1 } from "./StepperStyle"
import axios from "axios";

let steps = [
  {
    noSteps: 1,
    label: "Pengisian",
    status: "incomplete",
    activity: "inactive"
  },
  {
    noSteps: 2,
    label: "Pre-screen",
    status: "incomplete",
    activity: "inactive"
  },
  {
    noSteps: 3,
    label: "Verification",
    status: "incomplete",
    activity: "inactive"
  },
  {
    noSteps: 4,
    label: "Approval",
    status: "incomplete",
    activity: "inactive"
  },
  {
    noSteps: 5,
    label: "Akad",
    status: "incomplete",
    activity: "inactive"
  },
  {
    noSteps: 6,
    label: "Dropping Pembiayaan",
    status: "incomplete",
    activity: "inactive"
  },
]

function isActive(activity) {
  if (activity == "active") {
    return true
  } else {
    return false
  }
}

export default function StepperPengajuan() {

  const [dataNotif, setDataNotif] = useState();

  let boxApprove = {
    background: '#28A745',
    width: '100%',
    maxWidth: "289px",
    height: '79px',
    left: '575px',
    marginTop: '41px',
    marginBottom: '48px',
    borderRadius: '8px',
    marginLeft: "auto",
    marginRight: "auto"
  }

  let boxSendBack = {
    background: '#df8020',
    width: '100%',
    maxWidth: "289px",
    height: '79px',
    left: '575px',
    marginTop: '41px',
    marginBottom: '48px',
    borderRadius: '8px',
    marginLeft: "auto",
    marginRight: "auto"
  }

  let boxReject = {
    background: '#b21919',
    width: '100%',
    maxWidth: "289px",
    height: '79px',
    left: '575px',
    marginTop: '41px',
    marginBottom: '48px',
    borderRadius: '8px',
    marginLeft: "auto",
    marginRight: "auto"
  }

  function boxStatus(status) {
    if (status == "approve") {
      return boxApprove;
    } else if (status == "sendBack") {
      return boxSendBack;
    } else if (status == "reject" || status == "cancel") {
      return boxReject;
    }
  }

  let arrayDB = JSON.parse(sessionStorage.getItem('arrayDB'))

  if (arrayDB !== null) {

    arrayDB.map((data, index) => (
      steps[index].label = data.stage_name,
      steps[index].status = data.status_pengajuan,
      steps[index].activity = data.activity
    ))
  }

  return (
    <>
      <div className="stepperBesar">
        <div
          style={{
            display: "flex",
            flexDirection: "row"
          }}
        >
          {
            steps.map((step, index) => {
              return (
                <>
                  {isShow(index) && <hr style={connectorStatus(step.status)} />}
                  <div style={wrapperStepperBasic}>
                    <div style={stepActivity(step)}>
                      <div style={stepStatus(step.status)}>
                        {stepIcon(step.status, index + 1)}
                      </div>
                    </div>
                    <label style={stepLabelStat(step.status)}>{step.label}</label>
                  </div>
                </>
              );
            })
          }

        </div>
      </div>

      <div className="stepperKecil">
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            marginTop: "50px",
          }}
        >
          {
            steps.map((step, index) => {
              return (
                <>
                  {isShow(index) && <hr style={connectorStatus1(step.status)} />}
                  <div style={wrapperStepperBasic1}>
                    <div style={stepActivity1(step)}>
                      <div style={stepStatus(step.status)}>
                        {stepIcon(step.status, index + 1)}
                      </div>
                    </div>
                    <label style={stepLabelStat1(step.status)}>{step.label}</label>
                  </div>
                </>
              );
            })
          }
        </div>
      </div>

      {
        steps.map((step, index) => {
          return (
            <>
              {
                isActive(step.activity) &&
                <div style={boxStatus(step.status)}>
                  <img src='./assets/shape.png' className="rounded float-left" alt="Icon" style={{ marginTop: '18px', marginLeft: '18px', marginRight: '18px' }} />
                  <div style={{ textAlign: 'left', marginLeft: '56px' }}>
                    <h5 style={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: '16px', paddingTop: '16px' }}>Notification</h5>
                    <h6 style={{ color: '#FFFFFF', fontWeight: 'normal', fontSize: '14px', margin: 0, paddingBottom: '16px' }}>Pengisian form berhasil</h6>
                    {/* <h6>{dataNotif}</h6> */}
                  </div>
                </div>
              }
            </>
          )
        })
      }

    </>
  );
}

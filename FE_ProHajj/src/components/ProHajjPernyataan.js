import React, { Component } from 'react'
import {Container, Row, Col, Card, Form, Modal } from 'react-bootstrap'
import './Prohajj.css';

export class ProHajjPernyataan extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.onTop();
    }
    render() {   
        const radioSize = {fontSize: 14};
        const { handleChange } = this.props;
        const isHide = (this.props.prytModal==='YA')
        const ownprod = (this.props.prod === this.props.eprod?"PT. Al Ijarah Indonesia Finance (ALIF)":"PT. Bank Muamalat Indonesia, Tbk")
        const nmprod = (this.props.prod === this.props.eprod?"Al-Hajj":"Multiguna")
       return (
            <Modal 
                size='xl'
                show={this.props.msign}
                onHide={()=>this.props.handleModalpnyata}
                backdrop="static"
                keyboard={false}>

            <Modal.Body>
                <Container >
                <div className="p-2 col-example text-center">
                        <h5>AKAD PEMBUKAAN REKENING NASABAH</h5>
                    </div>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    
                    <Card.Body>            
                        <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: "<font size='2' face=''>Nasabah dan Bank bersepakat melakukan Akad:</br></p>"
                                +"<table style='width:100%;'>"
                                +"<tr><td valign='top'>1.</td><td>Nasabah dengan ini menitipkan dana kepada Bank dengan melakukan setoran awal sebesar jumlah nominal yang tercantum pada isian Setoran Awal (Tabungan/Giro), dan Bank bersedia menerima penitipan serta diperbolehkan untuk mengelola dana yang dibukukan dalam bentuk rekening atas nama Nasabah. Bank dapat memberi bonus kepada Nasabah sesuai ketentuan Bank. Nasabah setuju bahwa keuntungan dari manfaat yang diterima dari hasil pengelolaan dana titipan tersebut menjadi hak Bank;</td></tr>"
                                +"<tr><td valign='top'>2.</td><td>Syarat-syarat lain dari Akad ini mengacu pada Ketentuan dan Persyaratan Pembukaan Tabungan yang merupakan lampiran dan bagian yang tidak terpisahkan dari Akad ini.</td></tr>"
                                +"<tr><td colspan='2'></br></td></tr>"
                                +"<tr><td colspan='2'>Perjanjian ini telah disesuaikan dengan ketentuan Peraturan Perundang-Undangan termasuk ketentuan peraturan Otoritas Jasa Keuangan. Demikian akad ini dibuat secara musyawarah dan mufakat yang berlaku sejak tanggal penandatanganan.</td></tr>"
                                +"</table>"}}/> 
                        <div hidden={!isHide} className="p-2 col-example text-right" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr style='text-align: right;'><td colspan='2'>Page <b>1</b> of <b>1</b></td></tr></table>"}}/>
                    </Card.Body>
                </Card>
                <div className="p-2 col-example text-center">
                        <h5>PERNYATAAN NASABAH</h5>
                    </div>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    
                    <Card.Body>            
                        <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: "<font size='2' face=''>Dengan menandatangani formulir ini, maka saya menyatakan:"
                                +"<table style='width:100%;'>"
                                +"<tr><td valign='top'>1.</td><td>Data pribadi yang saya berikan dalam formulir ini adalah benar sesuai dengan dokumen identitas diri dan dokumen terkait lainnya yang berlaku;</td></tr>"
                                +"<tr><td valign='top'>2.</td><td>"+ownprod+" dapat melakukan pemeriksaan terhadap kebenaran data yang diberikan dan saya dengan ini membebaskan "+ownprod+" dari segala tuntutan, gugatan, dan/atau tindakan hukum lainnya yang timbul sehubungan dengan hal tersebut dan pengisian formulir ini;</td></tr>"
                                +"<tr><td valign='top'>3.</td><td>Telah membaca dan menyetujui segala ketentuan sebagaimana tertuang pada Ketentuan dan Persyaratan Pembukaan Tabungan dan Syarat Umum Pembiayaan yang merupakan lampiran dan bagian yang tidak terpisahkan dari formulir ini;</td></tr>"
                                +"<tr><td valign='top'>4.</td><td>Telah memahami dan menyetujui semua konsekuensi penggunaan produk ini, termasuk manfaat, risiko, dan berbagai biaya yang melekat pada produk yang akan dimanfaatkan sesuai pilihan saya;</td></tr>"
                                +"</table>"}}/> 
                        <div hidden={!isHide} className="p-2 col-example text-right" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr style='text-align: right;'><td colspan='2'>Page <b>1</b> of <b>5</b></td></tr></table>"}}/>
                    </Card.Body>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Body>            
                        <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr><td valign='top'>5.</td><td>Telah mengetahui, memahami, dan bertanggung jawab terhadap risiko penggunaan dana yang terdapat dalam rekening, fasilitas rekening, media penarikan dan/atau pemindahbukuan dana dalam rangka transaksi sesuai dengan prosedur dan ketentuan yang berlaku pada "+ownprod+";</td></tr>"
                                +"<tr><td valign='top'>6.</td><td>Jika transaksi yang dilakukan merupakan obyek dari Foreign Account Tax Compliance Act (FATCA) atau Common Reporting Standard (CRS), saya menyatakan tunduk kepada peraturan FATCA/CRS yang ditetapkan oleh Pemerintah Negara terkait, memberikan kuasa dan persetujuan kepada "+ownprod+", untuk menyerahkan informasi dan data keuangan sesuai ketentuan FATCA/CRS yang berlaku;</td></tr>"
                                +"<tr><td valign='top'>7.</td><td>Dalam rangka memenuhi ketentuan Peraturan Pemerintah Republik Indonesia tentang Besaran Nilai Simpanan yang dijamin Lembaga Penjamin Simpanan (LPS), saya menyatakan setuju dan bersedia menerima risiko bahwa klaim penjaminan atas simpanan tidak akan dibayar apabila simpanan yang saya tempatkan di "+ownprod+", tidak memenuhi ketentuan penjaminan simpanan, atau dinyatakan sebagai klaim penjaminan tidak layak dibayar sebagaimana yang telah ditetapkan oleh LPS;</td></tr>"
                                +"</table>"}}/>  
                        <div hidden={!isHide} className="p-2 col-example text-right" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr style='text-align: right;'><td colspan='2'>Page <b>2</b> of <b>5</b></td></tr></table>"}}/>
                    </Card.Body>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Body>            
                        <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr><td valign='top'>8.</td><td>Jika terdapat perubahan data atau informasi termasuk tapi tidak terbatas perubahan data nomor telepon seluler dan/atau informasi lainnya yang telah saya berikan kepada "+ownprod+", maka saya akan memberitahukan perubahan data atau informasi tersebut kepada "+ownprod+" dalam jangka waktu selambat-lambatnya 30 (tiga puluh) hari kalender terhitung sejak terjadinya perubahan perubahan tersebut atau dalam jangka waktu lain yang akan diberitahukan oleh "+ownprod+" kepada Nasabah dalam bentuk dan melalui sarana apa pun;</td></tr>"
                                +"<tr><td valign='top'>9.</td><td>Setuju bahwa "+ownprod+" berhak mengakhiri hubungan dengan Nasabah jika di kemudian hari dapat dibuktikan bahwa saya telah memberikan data atau informasi yang tidak benar, tidak akurat, atau tidak lengkap, atau saya tidak memberitahukan perubahan data atau informasi terkait Nasabah kepada "+ownprod+" dalam jangka waktu yang ditentukan;</td></tr>"
                                +"<tr><td valign='top'>10.</td><td>"+ownprod+" berhak dan berwenang untuk menolak atau menyetujui seluruh atau sebagian dari jumlah yang saya mohon berdasarkan verifikasi dan analisa "+ownprod+";</td></tr>"
                                +"</table>"}}/> 
                        <div hidden={!isHide} className="p-2 col-example text-right" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr style='text-align: right;'><td colspan='2'>Page <b>3</b> of <b>5</b></td></tr></table>"}}/>
                    </Card.Body>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Body>            
                        <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr><td valign='top'>11.</td><td>Saya akan menyediakan seluruh biaya-biaya prarealisasi di rekening bank atas nama saya yang akan diinformasikan oleh petugas "+ownprod+" pada saat akad pembiayaan.</td></tr>"
                                +"<tr><td valign='top'>12.</td><td>Semua dokumen yang telah diserahkan kepada "+ownprod+" tidak akan saya tarik kembali;</td></tr>"
                                +"<tr><td valign='top'>13.</td><td>Seluruh fasilitas pembiayaan dilaporkan ke dalam Sistem Layanan Informasi Keuangan (SLIK);</td></tr>"
                                +"<tr><td valign='top'>14.</td><td>Saya menyatakan bersedia dan akan mentaati segala persyaratan dan ketentuan yang berlaku di "+ownprod+", ketentuan dari Regulator maupun ketentuan perundangan yang berlaku;</td></tr>"
                                +"<tr><td valign='top'>15.</td><td> Saya bersedia untuk dilakukan penutupan asuransi sesuai ketentuan yang berlaku di "+ownprod+".</td></tr>"
                                +"<tr><td valign='top'>16.</td><td> Nasabah dengan ini menyatakan:</td></tr>"
                                +"</table>"}}/>
                                <Row>
                                <Col md>
                                    <Form.Text className='text-left' style={{ width: "100%" }}> </Form.Text>
                                </Col>
                                <Col md>
                                <div className="p-2 col-example text-center">
                                    <table style={{width:'100%'}}>
                                    <tbody>
                                    <tr>
                                    <td style={{width:'50%',textAlign:'center'}}>
                                        <Form.Check className="radio"
                                        required
                                        type="radio"
                                        label="SETUJU"
                                        name="custsign"
                                        id="ycustsign"
                                        style={radioSize}
                                        defaultValue={'YA'}
                                        onChange={handleChange("custsign")}
                                        checked={(this.props.prytModal==='YA')}
                                        /></td>
                                    <td style={{width:'50%',textAlign:'center'}}>
                                        <Form.Check className="radio"
                                        required
                                        type="radio"
                                        label="TIDAK SETUJU"
                                        name="custsign"
                                        id="tcustsign"
                                        style={radioSize}
                                        defaultValue={'TIDAK'}
                                        checked={(this.props.prytModal==='TIDAK')}
                                        //onClick={this.radioChange}
                                        onChange={handleChange("custsign")}
                                        /></td>
                                    </tr>
                                    </tbody>
                                    </table>
                                    </div>
                                    </Col>
                                    <Col md>
                                        <Form.Text className='text-left' style={{ width: "100%" }}> </Form.Text>
                                    </Col>
                                </Row>
                        <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr><td>"+ownprod+" dapat melakukan pengkinian data Nasabah pada sistem, menggunakan dan memberikan data dan/atau informasi pribadi saya kepada pihak lain yang bekerja sama dengan "+ownprod+", serta menerima informasi produk dan atau layanan melalui sarana komunikasi pribadi pada jam kerja atau di luar jam kerja.</td></tr>"
                                +"</table>"}}/>
                        <div hidden={!isHide} className="p-2 col-example text-right" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr style='text-align: right;'><td colspan='2'>Page <b>4</b> of <b>5</b></td></tr></table>"}}/>
                    </Card.Body>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Body>            
                        <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr><td>Dengan ditandatanganinya Formulir Pengajuan Pembiayaan "+nmprod+" ini, saya menyatakan bahwa seluruh data isian dan pilihan yang saya berikan adalah benar dan saya menyatakan tunduk pada ketentuan produk/atau layanan yang berlaku berikut perubahannya di kemudian hari sebagaimana yang diberitahukan oleh "+ownprod+", melalui jaringan kantor/surat /email/media lainnya serta tunduk dan terikat pada peraturan yang berlaku di "+ownprod+", Bank Indonesia (BI)/Otoritas Jasa Keuangan (OJK), dan Fatwa Dewan Syariah Nasional (DSN) yang merupakan satu kesatuan dan bagian yang tidak terpisahkan dari formulir ini.</td></tr>"
                                +"</table>"}}/> 
                        <div hidden={!isHide} className="p-2 col-example text-right" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr><td style='width:50%;'></td><td align='center'>Nasabah</td></tr>"
                                +"<tr><td style='width:50%;'></td><td align='center'>"+this.props.dateNow('')+"</td></tr>"
                                +"<tr><td style='width:50%;'></td><td align='center'><br/><p/>"+this.props.custname+"</td></tr>"
                                +"<tr><td style='width:50%;'></td><td align='center'></td></tr>"
                                +"</table>"}}/> 
                        <div className="p-2 col-example text-right" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"<font size='2' face=''><table style='width:100%;'>"
                                +"<tr style='text-align: right;'><td colspan='2'>Page <b>5</b> of <b>5</b></td></tr></table>"}}/>
                    </Card.Body>
                </Card>
                </Container>
                </Modal.Body>
            <Modal.Footer>
                    <button className="btn btn-outline-success btn-sm" 
                            type='button' 
                            id="clsmodal"
                            onClick={this.props.handleModalpnyata}
                            title='Kembali'>KEMBALI</button>
            </Modal.Footer>
        </Modal>
        )
    }
}

export default ProHajjPernyataan
/*
<Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'50%',textAlign:'left'}}><h5>Pernyataan Nasabah</h5></td><td style={{width:'50%',textAlign:'right'}}>5 of 5</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
*/

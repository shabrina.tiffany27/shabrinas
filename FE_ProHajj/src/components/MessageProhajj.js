import React, { Component } from 'react'
import {Container, Card} from 'react-bootstrap'
import Header from './Header'
//import { ToastContainer, toast } from 'react-toastify'


export class MessageProhajj extends Component {
    carWarning =()=>{
        var sRtn = true;
        var terr = this.props.message;
        var iniserw = terr.substring(0,1) 
        if (iniserw === '#'){
            sRtn = false;
        }else {
            sRtn = true;
        }
    return sRtn ;
    }
    /*cobaLagi = e =>{
        e.preventDefault();
        var terr = this.props.message;
        var iniser = terr.substring(0,2)
        if (iniser === '#P'){
            this.props.handleSubmit();
        }else {
            this.props.loadData();
        }
    }*/
    componentDidMount() {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
          });
        //toast.error(this.props.message.substring(2));
      }
    render() {
        const heder=(this.props.message.substring(0,2)==='01'?'Alhamdulillah':'PERHATIAN')
        const bdymsg=(this.props.message.substring(0,2)==='01'?'Upload Ulang Dokumen Berhasil':this.props.message.substring(2))
        return (
            <div>
                <Header prod = {this.props.prod} eprod = {this.props.eprod}/>
                <Container >
                
                <div className="shadow p-3 mb-5 bg-white rounded">
                <Card>
                    <Card.Header>
                        <div className="p-2 col-example text-center">
                            <h5>{heder}</h5>
                        </div>
                    </Card.Header>
                    <Card.Body>
                        <div className="p-2 col-example text-center">
                            <h5>{bdymsg}</h5><br/>
                            
                        </div>
                    </Card.Body>
                </Card>
                </div>
                </Container>
            </div>
        )
    }
}

export default MessageProhajj
//<button hidden={this.carWarning()} id='tryagnbutton' className="btn btn-outline-success my-1 my-sm-0" onClick={this.cobaLagi}>Coba Lagi</button>
/*<ToastContainer 
position="bottom-right"
/*  type="default"
  autoClose={30000}
  hideProgressBar={false}
  newestOnTop={false}
  closeOnClick
  pauseOnHover
/>
*/
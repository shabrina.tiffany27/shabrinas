import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {TextField, MenuItem} from '@material-ui/core/';
import ModalConfirm from './ModalConfirm';
//import { ToastContainer } from 'react-toastify';

export class EformPemohon2 extends Component {
    continue = e => {
        e.preventDefault();
        this.props.handleSaveTemp();
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.getWilayah('prov');
        this.props.getWilayah('dati2','pn');
        this.props.getWilayah('kec','pn');
        this.props.getWilayah('kel','pn');
        this.props.onTop();  
    }
    
    render() {
        //const numOnly = /^[0-9\b]+$/;
        const { handleChangeP, handleSaveTemp } = this.props
        const prov = (this.props.pbypmhn.prov?this.props.pbypmhn.prov:[]);
        const kab = (this.props.pbypmhn.nkab?this.props.pbypmhn.nkab:[]);
        const kec = (this.props.pbypmhn.nkec?this.props.pbypmhn.nkec:[]);
        const kel = (this.props.pbypmhn.nkel?this.props.pbypmhn.nkel:[]);
        
        return (
            <div>
                <Header prod = {this.props.pbypmhn.kd_produk} eprod = {this.props.pbypmhn.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.pbypmhn.batal}/>
                <Form onSubmit={this.continue} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'50%',textAlign:'left'}}><h5>Data Pemohon</h5></td><td style={{width:'50%',textAlign:'right'}}>Page 1-3</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                            <div className="p-2 col-example text-center"
                                dangerouslySetInnerHTML={{__html: "<b>Alamat Saat Ini (Domisili)</b>"
                                }}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group >
                                    <TextField
                                    label="Provinsi"
                                    required
                                    fullWidth
                                    value ={this.props.pbypmhn.pemohon.dt1now}
                                    name="dt1now"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    select>
                                        {
                                        prov.map((h) => 
                                        (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                        }
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Kabupaten / Kota"
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.pbypmhn.pemohon.dt2now} 
                                        name="dt2now"
                                        onChange={handleChangeP}
                                        select>
                                            {
                                            kab.map((h) => 
                                            (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                            }
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Kecamatan"
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.pbypmhn.pemohon.kcmtnow} 
                                        name="kcmtnow"
                                        onChange={handleChangeP}
                                        select>
                                            {
                                            kec.map((h) => 
                                            (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                            }
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Kelurahan" 
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.pbypmhn.pemohon.klrhnow}
                                        name="klrhnow"
                                        onChange={handleChangeP}
                                        select>
                                            {
                                            kel.map((h) => 
                                            (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                            }
                                    </TextField>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md='6'>
                                <Form.Group>
                                    <TextField
                                    label="Alamat Domisili"
                                    required
                                    fullWidth
                                    value ={this.props.pbypmhn.pemohon.almnow}
                                    name="almnow"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    helperText='Diisi nama jalan dan nomor rumah'
                                    />
                                </Form.Group>
                            </Col>
                            <Col md>                             
                                    <table style={{width:'100%'}}>
                                        <tbody>
                                            <tr>
                                                <td style={{width:'50%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <TextField
                                                        label="RT"
                                                        //type="number"
                                                        required
                                                        fullWidth
                                                        value ={this.props.pbypmhn.pemohon.nowrt}
                                                        name="nowrt"
                                                        onChange={handleChangeP}
                                                        variant="standard"
                                                        size="small"
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td style={{width:'50%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <TextField 
                                                        label="RW"
                                                        //type="number"
                                                        required
                                                        fullWidth
                                                        value ={this.props.pbypmhn.pemohon.nowrw}
                                                        name="nowrw"
                                                        onChange={handleChangeP}
                                                        variant="standard"
                                                        size="small"
                                                        />
                                                    </Form.Group>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField disabled
                                    label="Kode Pos"
                                    //type="number"
                                    //required
                                    fullWidth
                                    value ={this.props.pbypmhn.pemohon.kdposnow}
                                    name="kdposnow"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    helperText={this.props.pbypmhn.loading && (
                                        <span className="spinner-border spinner-border-sm"></span>
                                     )}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='submit' 
                                            id='continue'
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformPemohon2

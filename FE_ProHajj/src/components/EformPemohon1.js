import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {TextField, MenuItem} from '@material-ui/core/';
import ModalConfirm from './ModalConfirm';
//import { ToastContainer } from 'react-toastify';

export class EformPemohon1 extends Component {
    continue = e => {
        e.preventDefault();
        let i = 1
        if (this.props.pbypmhn.pemohon.almnowktp ==='sda'){ 
            i = 2;
            this.props.emptypemohon();
        }
        this.props.handleSaveTemp();
        this.props.nextStep(i);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.getWilayah('prov');
        this.props.getWilayah('dati2','pp');
        this.props.getWilayah('kec','pp');
        this.props.getWilayah('kel','pp');
        this.props.onTop();
    }
    
    render() {
        //const numOnly = /^[0-9\b]+$/;
        const { handleChangeP, handleSaveTemp } = this.props
        const prov = (this.props.pbypmhn.prov?this.props.pbypmhn.prov:[]);
        const kab = (this.props.pbypmhn.kab?this.props.pbypmhn.kab:[]);
        const kec = (this.props.pbypmhn.kec?this.props.pbypmhn.kec:[]);
        const kel = (this.props.pbypmhn.kel?this.props.pbypmhn.kel:[]);
        
        return (
            <div>
                <Header prod = {this.props.pbypmhn.kd_produk} eprod = {this.props.pbypmhn.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.pbypmhn.batal}/>
                <Form onSubmit={this.continue} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'50%',textAlign:'left'}}><h5>Data Pemohon</h5></td><td style={{width:'50%',textAlign:'right'}}>Page 1-2</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                            <div className="p-2 col-example text-center"
                                dangerouslySetInnerHTML={{__html: "<b>Alamat Sesuai KTP</b>"
                                }}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group >
                                    <TextField
                                    label="Provinsi"
                                    required
                                    fullWidth
                                    value ={this.props.pbypmhn.pemohon.dt1ktp}
                                    name="dt1ktp"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    select>
                                        {
                                        prov.map((h) => 
                                        (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                        }
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Kabupaten / Kota" 
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.pbypmhn.pemohon.dt2ktp}
                                        name="dt2ktp"
                                        onChange={handleChangeP}
                                        select>
                                            {
                                            kab.map((h) => 
                                            (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                            }
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Kecamatan"
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.pbypmhn.pemohon.kcmtktp} 
                                        name="kcmtktp"
                                        onChange={handleChangeP}
                                        select>
                                            {
                                            kec.map((h) => 
                                            (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                            }
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Kelurahan"
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.pbypmhn.pemohon.klrhktp} 
                                        name="klrhktp"
                                        onChange={handleChangeP}
                                        select>
                                            {
                                            kel.map((h) => 
                                            (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                            }
                                    </TextField>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md='6'>
                                <Form.Group>
                                    <TextField
                                    label="Alamat Sesuai KTP"
                                    required
                                    fullWidth
                                    value ={this.props.pbypmhn.pemohon.almktp}
                                    name="almktp"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    helperText='Diisi nama jalan dan nomor rumah'
                                    />
                                </Form.Group>
                            </Col>
                            <Col md>                             
                                    <table style={{width:'100%'}}>
                                        <tbody>
                                            <tr>
                                                <td style={{width:'50%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <TextField
                                                        label="RT"
                                                        //type="number"
                                                        required
                                                        fullWidth
                                                        value ={this.props.pbypmhn.pemohon.ktprt}
                                                        name="ktprt"
                                                        onChange={handleChangeP}
                                                        variant="standard"
                                                        size="small"
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td style={{width:'50%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <TextField 
                                                        label="RW"
                                                        //type="number"
                                                        required
                                                        fullWidth
                                                        value ={this.props.pbypmhn.pemohon.ktprw}
                                                        name="ktprw"
                                                        onChange={handleChangeP}
                                                        variant="standard"
                                                        size="small"
                                                        />
                                                    </Form.Group>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField disabled
                                    label="Kode Pos"
                                    //type="number"
                                    //required
                                    fullWidth
                                    value ={this.props.pbypmhn.pemohon.kdposktp}
                                    name="kdposktp"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    helperText={this.props.pbypmhn.loading && (
                                        <span className="spinner-border spinner-border-sm"></span>
                                     )}
                                    />
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField id="select" 
                                        label="Alamat saat ini" 
                                        name="almnowktp"
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.pbypmhn.pemohon.almnowktp}
                                        onChange={handleChangeP}
                                        select>
                                        <MenuItem value={'sda'} >Sesuai KTP</MenuItem>
                                        <MenuItem value={'tsda'}>Berbeda dengan KTP</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='submit' 
                                            id='continue'
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformPemohon1

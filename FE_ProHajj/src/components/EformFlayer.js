import React, { Component, useEffect } from 'react'
import { Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import { TextField } from '@material-ui/core/';
import NumberFormat from 'react-number-format';
import ModalACCconfirm from './ModalACCconfirm';

export class EformFlayer extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep(1);
    }
    lanjut = e => {
        if (parseInt(this.props.flayer.appid.replace(/[^\w,]/gi, "").length) !== 0) {
            //if (this.props.flayer.appid !== ''){
            this.props.nextStep(this.props.flayer.step);
        } else {
            this.props.handleModalAcc();
        }
    }

    componentDidMount() {
        // this.props.onTop(); 
        this.props.loadBdgUsh();
    }

    render() {
        const { handleChange } = this.props
        const kdall = [this.props.flayer.kd_produk, this.props.flayer.kd_channel, this.props.flayer.kd_subchannel, this.props.flayer.kd_program,
        this.props.flayer.kd_subprogram, this.props.flayer.kd_region, this.props.flayer.refcode, this.props.flayer.kd_pendamping].filter((v) => v).length !== 8;
        const isDisabled = (parseInt(this.props.flayer.appid.replace(/[^\w,]/gi, "").length) === 0 || parseInt(this.props.flayer.appid.replace(/[^\w,]/gi, "").length) > 11);
        const titlprod = (this.props.flayer.kd_produk === this.props.flayer.etypeproduk ? "Al-Hajj" : "Bank Muamalat")
        const imageprod = (this.props.flayer.kd_produk === this.props.flayer.etypeproduk ? "./assets/Logo-ALIF-Ver-2.png" : "./assets/bmimurni.png")
        localStorage.clear();
        return (
            <div>
                <Header prod={this.props.flayer.kd_produk} eprod={this.props.flayer.etypeproduk} />
                <Container >
                    <ModalACCconfirm
                        animationType={'slide'}
                        handleModalAcc={this.props.handleModalAcc}
                        handleChange={this.props.handleChange}
                        radioChange={this.props.radioChange}
                        nextStep={this.props.nextStep}
                        accn={this.props.flayer.accnumktp}
                        flgac={this.props.flayer.custflg}
                        macc={this.props.flayer.moacc} />
                    <Card className="shadow p-3 mb-5 bg-white rounded">
                        <Card.Body>
                            <div className="p-2 col-example text-justify"
                                dangerouslySetInnerHTML={{
                                    __html:
                                        "<b>Assalamu'alaikum Warahmatullahi Wabarakatuh</b>"
                                        + "<p/><br/>Anda ingin daftar porsi Haji sekeluarga tapi belum memiliki dana?"
                                        + "<br/>Segera daftarkan diri dan keluarga anda di Pembiayaan Multiguna " + titlprod
                                }} />
                            <Row>
                                <Col md>
                                    <img src={imageprod} className="rounded float-center" width="80%" label='Prohajj flayer' alt='flayer' />
                                </Col>
                            </Row>
                            <Row hidden={kdall}>
                                <Col md></Col>
                                <Col md='5'>
                                    <Form.Group>
                                        <NumberFormat customInput={TextField}
                                            format="#############"
                                            hinttext=""
                                            placeholder=""
                                            label="e-Form ID"
                                            //type="number"
                                            //required
                                            inputProps={{ style: { textAlign: 'center' } }}
                                            fullWidth
                                            size="small"
                                            value={this.props.flayer.appid}
                                            name="appid"
                                            onChange={handleChange("appid")}
                                            variant="standard"
                                            helperText={"Cantumkan e-Form ID jika pernah mengisi e-Form sebelumnya.\nSilahkan klik lanjut jika belum pernah mengisi e-Form."}
                                        />
                                    </Form.Group>
                                </Col>
                                <Col md></Col>
                            </Row>
                            <Row hidden={!kdall}>
                                <Col md>
                                    <div className="p-2 col-example text-danger text-center"
                                        dangerouslySetInnerHTML={{
                                            __html:
                                                "<b>URL/Link Tidak Valid</b>"
                                        }} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md></Col>
                                <Col md='5'>
                                    <div className="d-flex left-content-start">
                                        <table style={{ width: '100%' }}>
                                            <tbody>
                                                <tr>
                                                    <td style={{ textAlign: 'center' }}>
                                                        {this.props.flayer.loading && (
                                                            <span className="spinner-grow spinner-grow-sm" style={{ color: "green" }} />
                                                        )}
                                                        {this.props.flayer.loading && (
                                                            <span className="spinner-grow spinner-grow-sm" style={{ color: "orange" }} />
                                                        )}
                                                        {this.props.flayer.loading && (
                                                            <span className="spinner-grow spinner-grow-sm" style={{ color: "purple" }} />
                                                        )}
                                                        {!this.props.flayer.loading && (
                                                            <button className="btn btn-outline-success btn-block"
                                                                disabled={!isDisabled}
                                                                hidden={kdall}
                                                                type='button'
                                                                id='continue'
                                                                onClick={this.lanjut}
                                                                title='Lanjut'>Lanjut</button>
                                                        )}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </Col>
                                <Col md></Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    }
}

export default EformFlayer
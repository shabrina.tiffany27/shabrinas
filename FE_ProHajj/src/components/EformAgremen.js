import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {FormControl, FormGroup, FormControlLabel ,FormHelperText, Checkbox} from '@material-ui/core/';
import ProHajjPernyataan from './ProHajjPernyataan';
import ProhajjAgremen from './ProhajjAgremen';
import ProhajjAgremenAlHajj from './ProhajjAgremenAlHajj'
import ProhajjKetTabungan from './ProhajjKetTabungan';
import ModalConfirm from './ModalConfirm';
import {TextField,Box} from '@material-ui/core/';
//import { ToastContainer } from 'react-toastify';

export class EformAgremen extends Component {
    continue = e => {
        e.preventDefault();
        //this.props.handleSaveTemp();
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.onTop();  
    }
    msgResult = () => {
        let valrespctr = (this.props.mgree.validresp !== null?this.props.mgree.validresp.tokenctr:0)
        let valrespcd = (this.props.mgree.validresp !== null?this.props.mgree.validresp.respcode:'' )
        let rspctr = (this.props.mgree.mssge !== ''?valrespctr:0);
        let getknctr = (this.props.mgree.tokenctr !== ''?this.props.mgree.tokenctr:'');
        var msg = null;
        //var msg2 = 'M-Pass Code akan dikrim ke alamat email : '+this.props.mgree.pemohon.email;
        if (getknctr === 0 && this.props.mgree.mssge ===''){
            msg = 'M-Pass Code berhasil terkirim';
        }else if (rspctr > 0 && rspctr < 3 && this.props.mgree.mssge !=='' && valrespcd !== '11'){
            msg = this.props.mgree.mssge+' ( Kesempatan '+ (3 - rspctr) + ' kali lagi ) '
        }else if (getknctr > 0 && getknctr < 3) {
            msg = 'M-Pass Code berhasil terkirim, berlaku selama 5 menit. Kesempatan kirim ulang M-Pass Code '+(3 - getknctr)+ ' kali lagi';
        }else if (getknctr === 3){
            msg = 'Kirim Ulang M-Pass Code telah mencapai limit 3 (tiga) kali, anda tidak bisa melanjutkan proses / wajib input ulang'
        }else if (valrespcd === '11'){
            msg = this.props.mgree.validresp.respdesc+' Silahkan Request/KIRIM ulang M-Pass Code';
        }else if (valrespcd === '06'){
            msg = this.props.mgree.validresp.respdesc;
        }
        return msg
    }
    render() {
        //const numOnly = /^[0-9\b]+$/;
        const { handleChangeCkBox, validToken, getSenToken, handleChangeN, handleSaveTemp } = this.props
        const prytModal = this.props.mgree.custsign;
        const agreModal = this.props.mgree.syaratpby;
        const stabModal = this.props.mgree.syarattab;
        const error = [prytModal, agreModal, stabModal].filter((v) => v).length !== 3;
        const vRespcd =(this.props.mgree.validresp !== null?this.props.mgree.validresp.respcode:'');
        const isDisabled =(this.props.mgree.tokensub.length === 6 && !error ) ;
        const titlprod = (this.props.mgree.kd_produk === this.props.mgree.etypeproduk ?"Al-Hajj":"Multiguna")
        return (
            <div>
                {console.log('bidang ktrnya?', this.props.mjob.pekerjaan.bidangktr)}
                <Header prod = {this.props.mgree.kd_produk} eprod = {this.props.mgree.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.mgree.batal}/>
                <ProHajjPernyataan
                    onTop ={this.props.onTop}
                    handleModalpnyata = {this.props.handleModalpnyata}
                    handleChange = {this.props.handleChange}
                    dateNow = {this.props.dateNow}
                    msign = {this.props.mgree.mcsig}
                    prytModal = {prytModal}
                    custname = { this.props.mgree.pemohon.namalkp}
                    prod = {this.props.mgree.kd_produk} 
                    eprod = {this.props.mgree.etypeproduk}
                    />
                <ProhajjAgremen 
                    handleModalagree = {this.props.handleModalagree}
                    mbpb = {this.props.mgree.mspby}/>
                <ProhajjAgremenAlHajj 
                    handleModalalhajj = {this.props.handleModalalhajj}
                    mbpjj = {this.props.mgree.mspblhajj}/>
                <ProhajjKetTabungan 
                    handleModalsytab = {this.props.handleModalsytab}
                    mktab = {this.props.mgree.mstab}/>
                    
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'80%',textAlign:'left'}}><h5>Persetujuan Nasabah</h5></td>
                            <td style={{width:'20%',textAlign:'right'}}>Page 7</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Box component="fieldset" mb={3} borderColor="transparent">
                        <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: "Dengan ini saya telah menyetujui Pernyataan, "
                                +"Syarat Umum Pembiayaan "+titlprod+", dan Syarat Ketentuan Tabungan:"
                                }}/>
                        <Row>
                            <Col md>
                            <FormControl required error={error} component="fieldset" >
                                <FormGroup>
                                <FormControlLabel
                                    control={<Checkbox 
                                        required 
                                        value={'YA'} 
                                        onChange={handleChangeCkBox("custsign")}
                                        checked={(this.props.mgree.custsign==='YA')}
                                        name="custsign" 
                                        color="primary"
                                        />}
                                    label="Akad Pembukaan Rekening dan Pernyataan Nasabah"
                                />
                                <FormControlLabel
                                    control={<Checkbox  
                                        required 
                                        value={'YA'}
                                        onChange={handleChangeCkBox("syaratpby")}
                                        checked={(this.props.mgree.syaratpby==='YA')}
                                        name="syaratpby" 
                                        color="primary"
                                         />}
                                    label={"Syarat Umum Pembiayaan "+titlprod}
                                />
                                <FormControlLabel
                                    control={<Checkbox 
                                        required 
                                        value={'YA'} 
                                        onChange={handleChangeCkBox("syarattab")}
                                        checked={(this.props.mgree.syarattab==='YA')}
                                        name="syarattab" 
                                        color="primary" />}
                                    label="Syarat Ketentuan Tabungan"
                                />
                                </FormGroup>
                                <FormHelperText></FormHelperText>
                            </FormControl>
                            </Col>
                        </Row>
                        </Box>
                        <div className="p-2 col-example text-center" 
                                dangerouslySetInnerHTML={{__html: "<font size='2'>M-Pass Code akan dikrim ke alamat email : <b><i>"+this.props.mgree.pemohon.email+"</i></b>"
                                }}/>
                        <Row >
                            <Col md>
                            <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{width:'50%',textAlign:'right'}}>
                                        <TextField
                                            disabled={error}
                                            label="M-Pass Code"
                                            required
                                            name = "tokensub"
                                            size="small"
                                            value={this.props.mgree.tokensub}
                                            onChange={handleChangeN("tokensub")}
                                            inputProps={{style: { textAlign: 'center'},maxLength :6}}//, fontSize: '16px'
                                            variant="outlined"
                                        />
                                    </td>
                                    <td style={{width:'50%',textAlign:'left'}}>
                                        <button className="btn btn-success my-1 my-sm-0" 
                                            type='button' 
                                            id='gettoken'
                                            disabled={(this.props.mgree.loading || this.props.mgree.tokensub || error) && vRespcd !== '11' }
                                            onClick={getSenToken}
                                            //onChange={this.validSubmit()}
                                            title='Kirim M-Pass Code'>KIRIM M-Pass Code
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                            </div>
                            </Col>
                        </Row>
                            <Form.Text className='text-danger text-center'>{this.props.mgree.loading && (
                                            <span className="spinner-border spinner-border-sm"></span>
                                        )}{this.msgResult()}</Form.Text>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='button' 
                                            id='continue'
                                            disabled={!isDisabled || this.props.mgree.loading}
                                            onClick={validToken}
                                            title='Lanjut'>Setuju</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            disabled={prytModal}
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Container>
            </div>
        )
    }
}

export default EformAgremen

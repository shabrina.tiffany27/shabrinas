import React, { Component } from 'react'
import {Modal, Row, Col} from 'react-bootstrap'
//import TextField from '@material-ui/core/TextField';

export class ModalConfirm extends Component {
    refreshPage() { window.location.reload(false); }
    render() {
        return (
                <Modal 
                    show={this.props.mbtl}
                    onHide={()=>this.props.handleModalbtl}
                    backdrop="static"
                    keyboard={false}>
                    <Modal.Header><b>Konfirmasi</b></Modal.Header>
                    <Modal.Body>
                    <Row>
                        <Col md>
                        <div className="p-2 col-example text-justify"
                            dangerouslySetInnerHTML={{__html: "<b>Batal isi e-Form Multiguna?</b>"
                            }}/>
                        </Col>
                    </Row>
                    
                    </Modal.Body>
                    <Modal.Footer>
                    <button className="btn btn-outline-success btn-sm" 
                            //disabled={!isDisabled}
                            type='submit' 
                            id='ksubmit'
                            onClick={this.refreshPage}
                            title='Ya'>YA</button>
                    <button className="btn btn-outline-warning btn-sm" 
                            type='button' 
                            id='closemodal'
                            onClick={this.props.handleModalbtl}
                            title='Tidak'>TIDAK</button>
                    </Modal.Footer>
                </Modal>
        )
    }
}

export default ModalConfirm
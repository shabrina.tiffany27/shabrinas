import React, { Component } from 'react'
import {Navbar} from 'react-bootstrap'
import './Prohajj.css';

export class Header extends Component {
    render() {
        const bgNav = {backgroundColor: 'white'}
        const logo = (this.props.prod === this.props.eprod ?"./assets/alif.png":"./assets/bmilogotr.png")
        const wisiz = (this.props.prod === this.props.eprod ?"90px":"140px")
        const titlprod = (this.props.prod === this.props.eprod ?"Al-Hajj":"Multiguna")
        //<img src="./assets/bmi12.png" className="rounded float-left" alt="BMI Logo" />
        return (
            <div>
                <header className='header'>
                <Navbar  
                    style={bgNav} 
                    expand="md" scrolling ="true"
                    fixed="top" >
                    <Navbar.Brand>
                        <img src={logo} width={wisiz} height="100%" className="rounded float-left" alt="Logo" />
                    </Navbar.Brand>
                    <Navbar.Brand>
                    {titlprod} e-Form
                    </Navbar.Brand>
                </Navbar>
                </header>
            </div>
        )
    }
}

export default Header

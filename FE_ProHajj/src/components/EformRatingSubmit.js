import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {TextField,Typography,Box} from '@material-ui/core/';
import Rating from "@material-ui/lab/Rating";
//import { ToastContainer } from 'react-toastify';

export class EformRatingSubmit extends Component {
    continue = e => {
        e.preventDefault();
        this.props.handleSaveTemp();
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.onTop();  
    }
    
    render() {
        //const numOnly = /^[0-9\b]+$/;
        const { handleChange , handleSubmit } = this.props
        //const [value, setValue] = React.useState(1);
        return (
            <div>
                {console.log('bidang ktrnya???', this.props.mjob.pekerjaan.bidangktr)}
                <Header prod = {this.props.rsubm.kd_produk} eprod = {this.props.rsubm.etypeproduk}/>
                <Container >
                <Form onSubmit={handleSubmit} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'80%',textAlign:'center'}}><h5>TERIMA KASIH</h5></td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md></Col>
                            <Col md='6'>
                            <Box component="fieldset" mb={3} >
                                <Typography component="legend">Berikan penilaian atas proses pembiayaan ini:</Typography>
                                <Rating
                                    name="rating"
                                    //defaultValue={1}
                                    //precision={0.5}
                                    onChange={handleChange("rating")}
                                    size="large"
                                />
                            </Box>
                            </Col>
                            <Col md></Col>
                        </Row>
                        <Row>
                            <Col md></Col>
                            <Col md='5'>
                            <Form.Group>
                                <TextField
                                    label=""
                                    //required
                                    multiline 
                                    rows={4}
                                    rowsMax={4}
                                    fullWidth
                                    value={this.props.rsubm.ratingcomment}
                                    name="ratingcomment"
                                    onChange={handleChange("ratingcomment")}
                                    variant="outlined"
                                    //placeholder='Minimal 10 Karakter'
                                    //helperText ="Minimal 10 Karakter"
                                />
                            </Form.Group>
                            </Col>
                            <Col md></Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='submit' 
                                            id='save'
                                            //onClick={this.continue}
                                            title='Submit Eform'>SUBMIT</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformRatingSubmit

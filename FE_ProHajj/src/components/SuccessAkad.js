import React, { Component } from 'react'
import {Container, Card} from 'react-bootstrap'
import Header from './Header'
//import Footer from './Footer'
import { toast } from 'react-toastify';
import { jsPDF } from "jspdf";

export class SuccessAkad extends Component {
    saGood = () => {
        var data = [
            [0, 11, "Pagi"],
            [11, 15, "Siang"],
            [15, 18, "Sore"],
            [18, 24, "Malam"]
        ],
            hr = new Date().getHours(),
            slm ='';
        for(var i=0; i<data.length;i++){
            if(hr >= data[i][0] && hr <= data[i][1]){
                slm=(data[i][2]);
                //break;
            }
        }
        return slm;
    }
    componentDidMount() {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
          })
       toast.success("Alhamdulillah pengiriman Sukses..");
       this.jsPdfGenerator();
      }

      jsPdfGenerator =() =>{
        var doc = new jsPDF('p', 'pt');
        var salam = (this.props.sccesfield.kd_produk === this.props.sccesfield.etypeproduk?"SalamAlhajj : 021-80668066":"SalaMuamalat : 1500016")
        var prodown = (this.props.sccesfield.kd_produk === this.props.sccesfield.etypeproduk?"Al-Hajj":"Multiguna")

        if(this.props.sccesfield.kd_produk === this.props.sccesfield.etypeproduk){
            doc.addImage('./assets/alif.png','PNG',260,20,100,60);
        }else{
            doc.addImage('./assets/bmilogo.png','PNG',230,0,150,120);
        }
        
        //doc.addImage('./assets/bmilogo.png','PNG',230,0,150,120);
        //doc.setFontSize(20);
        //doc.text(210,90, 'Bank Muamalat');
        
        doc.setFontSize(9);
        doc.setFont("normal");
        doc.text(50,110, "Alhamdulillahirabbil`alamin");
        doc.text(50,120, "Selamat, anda telah mengirimkan pengajuan pembiayaan "+prodown+" melalui e-Form.");
        
        doc.setFontSize(11);
        doc.setFont("normal", "bold");
        doc.text(50,150, 'E-FORM ID     : '+this.props.sccesfield.appid);
        doc.text(50,165, 'Nama Nasabah : '+this.props.sccesfield.pemohon.namalkp);

        doc.setFontSize(9);
        doc.setFont("normal","normal");
        doc.text(50,190, "Nantikan konfirmasi dari kami untuk pengajuan pembiayaan "+prodown+" ini.");
        //doc.text(50,200, "e-Form yang telah anda kirim dan setujui akan langsung terkirim ke email anda.");

        doc.setFontSize(8);
        doc.setFont("normal", "italic");
        doc.text(50,220, 'Waspada penipuan yang mengatasnamakan Bank Muamalat !!!');
        doc.text(50,230, 'Info resmi '+prodown+' hanya melalui :');
        doc.text(50,240, '1. Akad elektronik : WhatsApp Resmi');
        doc.text(50,250, '2. Akad by phone : 021-806680xx');
        doc.text(50,260, '3. '+salam);
        doc.text(50,270, 'Kami tidak pernah meminta nasabah untuk mengirimkan dana apapun ke rekening lain selain rekening atas nama nasabah sendiri.');
    
        doc.save('BMI'+this.props.sccesfield.appid+'.pdf')
        toast.success('BMI'+this.props.sccesfield.appid+'.pdf, Download Sukses..');
    }
    render() {
        let salam = (this.props.sccesfield.kd_produk === this.props.sccesfield.etypeproduk?"SalamAlhajj : 021-80668066":"SalaMuamalat : 1500016")
        let prodown = (this.props.sccesfield.kd_produk === this.props.sccesfield.etypeproduk?"Al-Hajj":"Multiguna")
        return (
            <div>
                <Header prod = {this.props.sccesfield.kd_produk} eprod = {this.props.sccesfield.etypeproduk}/>
                <Container >
                <div className="shadow p-3 mb-5 bg-white rounded">
                <Card>
                    <Card.Header>
                    <h5><i>Alhamdulillahirabbil'alamin</i></h5>
                    </Card.Header>
                    <Card.Body>
                        <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: ""
                                +"Selamat, anda telah mengirimkan pengajuan pembiayaan "+prodown+" melalui e-Form.<br/><p>"         
                                +"<table><tr><td><b>E-FORM ID</b></td><td>:</td><td><b>"+this.props.sccesfield.appid+"</b></td></tr>"
                                +"<tr><td><b>Nama Nasabah</b></td><td>:</td><td><b>"+this.props.sccesfield.pemohon.namalkp+"</b></td></tr></table>"
                                +"<br/>Nantikan konfirmasi dari kami untuk pengajuan pembiayaan "+prodown+" ini."
                                +"<br/><br/><font size='2' face=''><i>Waspada penipuan yang mengatasnamakan Bank Muamalat !!!"
                                +"<br/>Info resmi "+prodown+" hanya melalui:"
                                +"<br/>1. Akad elektronik : WhatsApp Resmi"
                                +"<br/>2. Akad by phone : 021-806680xx"
                                +"<br/>3. "+salam
                                +"<br/>Kami tidak pernah meminta nasabah untuk mengirimkan dana apapun ke rekening lain selain rekening atas nama nasabah sendiri.</i>"}}/>
                    </Card.Body>
                    <Card.Footer>
                    <button className="btn btn-outline-success btn-sm" onClick={()=>this.jsPdfGenerator()}>Download</button>
                    </Card.Footer>
                </Card>
                </div>
                </Container>
            </div>
        )
    }
}

export default SuccessAkad
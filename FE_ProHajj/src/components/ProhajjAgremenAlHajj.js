import React, { Component } from 'react'
import {Container, Card, Modal} from 'react-bootstrap'

export class ProhajjAgremenAlHajj extends Component {
    render() {  
          return (
            <Modal 
                size='xl'
                show={this.props.mbpjj}
                onHide={()=>this.props.handleModalalhajj}
                backdrop="static"
                keyboard={false}>

            <Modal.Body>
                <Container >
                    <div className="p-2 col-example text-center">
                        <h5>SYARAT KETENTUAN PEMBIAYAAN AL-HAJJ</h5>
                    </div>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <Card.Body>
                    <div className="p-2 col-example text-justify" dangerouslySetInnerHTML={{__html: 
                        "<font size='2' face=''><table style='width:100%;'> "
                        +"<tr><td><b>I. </b></td><td colspan='3'><b>Berlakunya Syarat-Syarat Umum</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 1</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Syarat-syarat Umum ini berlaku bagi Pembiayaan Al-Hajj dan merupakan bagian terpenting dan integral yang tidak dapat dipisahkan dari Akad Pembiayaan Al-Hajj</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Jika dalam Akad tidak dimuat ketentuan khusus, maka ketentuan dalam Syarat-syarat Umum ini berlaku atas pemberian Pembiayaan Al-Hajj berdasarkan Akad tersebut.</td></tr>"
                        +"<tr><td></td><td valign='top'>3. </td><td colspan='2'>Jika ketentuan khusus dalam Akad mengatur hal yang sama atau bertentangan dengan Syarat-syarat Umum ini, maka yang diberlakukan adalah ketentuan khusus Akad tersebut.</td></tr>"
                        +"<tr><td></td><td valign='top'>4. </td><td colspan='2'>Dengan disetujuinya Akad, maka seluruh ketentuan dalam Syarat-syarat Umum berlaku dan mengikat untuk fasilitas Pembiayaan Al-Hajj.</td></tr>"
                        +"<tr><td><b>II. </b></td><td colspan='3'><b>Definisi</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 2</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Kata-kata yang dimulai dengan huruf besar yang digunakan dalam Syarat-syarat Umum dan Akad harus diartikan sebagai berikut:</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>a. </td><td><b>Akad</b> berarti perjanjian/kesepakatan secara lisan (melalui percakapan telepon yang direkam) dan tertulis antara Bank dan Nasabah yang memuat hak dan kewajiban bagi masing-masing pihak sesuai dengan Prinsip Syariah yang mengatur lebih lanjut tentang fasilitas Pembiayaan, berikut semua pengubahan, tambahan, perpanjangan, penegasan dan pembaharuannya, yang merupakan bagian yang tak terpisahkan dari Syarat-syarat Umum beserta segala dokumen yang dibuat sehubungan dengan itu.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>b. </td><td><b>Angsuran</b> adalah Jumlah Kewajiban yang harus dibayarkan oleh Nasabah yang terdiri dari hutang Baki Debet Pokok dan atau Imbalan sesuai dengan jadwal angsuran berdasarkan Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>c. </td><td><b>Baki Debet Pokok</b> berarti jumlah Pembiayaan yang ditarik oleh Nasabah setelah dikurangi Angsuran Pokok (jika ada) berdasarkan Akad dan atau dokumen lainnya yang disepakati bersama antara Bank dan Nasabah. Untuk menghindari keragu-raguan, yang dimaksud Baki Debet Pokok adalah jumlah dana talangan yang diberikan Bank kepada Nasabah untuk Pembiayaan berdasarkan Prinsip Al-Qardh.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>d. </td><td><b>Bank</b> berarti perseroan terbatas PT Bank Muamalat Indonesia Tbk, berkedudukan di Jakarta Selatan dan beralamat di Jalan Prof. Dr. Satrio Kav.18 Jakarta, termasuk tetapi tidak terbatas pada kantor-kantor cabangnya, para penerima dan atau pengganti haknya.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>e. </td><td><b>Biaya Administrasi</b> berarti biaya baik langsung maupun tidak langsung, yang timbul dari dan atau berhubungan dengan pelaksanaan Akad termasuk biaya yang dirinci dalam pasal 4 Syarat-syarat Umum ini.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>f. </td><td><b>Cedera Janji</b> adalah hal-hal yang disebutkan pada Pasal 10 Syarat-syarat Umum ini.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>g. </td><td><b>Ganti Rugi atau Ta’widh</b> berarti kewajiban pembayaran atas biaya-biaya rill yang dikeluarkan Bank sebagai akibat Cedera Janji Nasabah.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>h. </td><td><b>Hari Kerja</b> berarti hari-hari di mana Bank beroperasi untuk menjalankan usahanya.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>i. </td><td><b>Imbalan</b> berarti Ujrah untuk transaksi berdasarkan prinsip Wakalah bil Ujrah.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>j. </td><td><b>Jangka Waktu Akad</b> berarti jangka waktu fasilitas Pembayaan yang ditarik/digunakan oleh Nasabah sebagaimana dituangkan di dalam Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>k. </td><td><b>Jumlah Kewajiban</b> berarti jumlah Baki Debet Pokok, Imbalan, Denda, Biaya serta semua jumlah uang lain yang karena apapun juga terhutang dan wajib dibayar oleh Nasabah kepada Bank berdasarkan dan sesuai dengan Akad, baik yang disepakati dalam Akad maupun yang timbul di kemudian hari dalam rangka pelaksanaan Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>l. </td><td><b>Nasabah</b> berarti pihak yang menandatangani Syarat-syarat Umum ini sebagai penerima Pembiayaan dari Bank berdasarkan Akad <b>Obyek Akad</b> berarti manfaat yang detailnya akan diuraikan lebih lanjut di dalam Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>m. </td><td><b>Para Pihak</b> berarti Bank dan Nasabah secara bersama-sama.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>n. </td><td><b>Pembiayaan</b> adalah penyediaan dana atau tagihan yang dipersamakan dengan itu berupa:<br/>i. Transaksi pinjam meminjam dalam bentuk piutang al-Qardh;<br/>ii. Transaksi pembiayaan berdasarkan Prinsip Syariah lainnya.<br/>berdasarkan persetujuan atau kesepakatan antara Bank dan Nasabah yang mewajibkan Nasabah yang dibiayai dan/atau diberi fasilitas Pembiayaan untuk mengembalikan Pembiayaan tersebut setelah jangka waktu tertentu dengan Imbalan.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>o. </td><td><b>Prinsip Syariah</b> berarti prinsip-prinsip hukum yang bersumber dari <b>Al-Qur’an</b> dan <b>sunnah Nabi Muhammad SAW</b>, yang pelaksanaannya diatur dalam fatwa-fatwa yang dikeluarkan oleh <b>Dewan Syariah Nasional Majelis Ulama Indonesia</b> serta peraturan-peraturan/ surat-surat edaran yang dikeluarkan oleh pihak yang berwenang di bidang keuangan, termasuk Bank Indonesia dan Otoritas Jasa Keuangan, sepanjang menyangkut transaksitransaksi berdasarkan Prinsip Syariah.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>p. </td><td><b>Al-Qardh</b> berarti akad pinjaman dana dari Bank kepada kepada Nasabah dengan ketentuan bahwa Nasabah wajib mengembalikan dana yang diterimanya kepada Bank pada waktu yang telah disepakati oleh Bank dan Nasabah,</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>q. </td><td><b>Rekening Pembiayaan</b> berarti suatu rekening pada pembukuan Bank yang mencatat Jumlah Kewajiban Nasabah dan segala transaksi keuangan berkenaan dengan Pembiayaan antara Bank dengan Nasabah.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>r. </td><td><b>Syarat-syarat Umum</b> berarti semua ketentuan dan syarat yang tercantum dalam Syarat-syarat Umum ini, sebagaimana sewaktu-waktu diubah, ditambah atau diperbaharui, yang merupakan bagian dan menjadi satu kesatuan yang tidak terpisahkan dari Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>s. </td><td><b>Tunggakan</b> berarti tunggakan atas Jumlah Kewajiban, termasuk namun tidak terbatas pada tunggakan Baki Debet Pokok, tunggakan Imbalan, Denda, Biaya atau setiap jumlah lainnya yang wajib dibayar oleh Nasabah yang pada saat jatuh tempo belum/tidak dibayar sebagaimana mestinya kepada Bank.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>t. </td><td><b>Wakalah</b> berarti kuasa yang diberikan Bank kepada Nasabah untuk mencari, membayar harga dan memeriksa serta menerima Obyek Akad dari Pemasok atau kuasa dari Nasabah kepada Bank untuk pengurusan dokumen dan/atau tagihan Nasabah, tergantung dari konteks Pembiayaan yang diterima Nasabah dari Bank.</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Judul-judul pada pasal-pasal dalam Syarat-syarat Umum dan atau Akad dibuat hanya untuk kemudahan pencarian kembali saja dan tidak boleh dipakai dalam menafsirkan ketentuan Syarat-syarat Umum dan atau Akad.</td></tr>"
                        +"<tr style='text-align: right;'><td colspan='4'>Page <b>1</b> of <b>4</b></td></tr></table>"}}/>
                </Card.Body>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <Card.Body>
                    <div className="p-2 col-example text-justify" dangerouslySetInnerHTML={{__html: 
                        "<font size='2' face=''><table style='width:100%;'>"
                        +"<tr><td><b>III. </b></td><td colspan='3'><b>Tujuan, Jenis, Limit, dan Jangka Waktu Pembiayaan</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 3</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Tujuan Pembiayaan Al-Hajj adalah pemberian layanan pengurusan pendaftaran porsi haji dan pemberian dana talangan pendaftaran porsi haji Nasabah dan</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Jenis skema Pembiayaan Al-Hajj menggunakan skema Wakalah bil Ujroh dan Qardh.</td></tr>"
                        +"<tr><td></td><td valign='top'>3. </td><td colspan='2'>Jangka Waktu Pembiayaan sebagaimana disepakati di dalam Akad.</td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pelaksanaan Prinsip Syariah Dalam Pembiayaan</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 4</b></td></tr>"
                        +"<tr><td></td><td colspan='3'>Pelaksanaan Pembiayaan Al-Hajj akan dilaksanakan berdasarkan prinsip-prinsip sebagai berikut:</td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'><b>Pelaksanaan Prinsip Wakalah bil Ujrah</b></td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>a. </td><td>Bahwa Nasabah dalam melakukan pengurusan pendaftaran porsi haji, membutuhkan dukungan Bank berupa fasilitas layanan pengurusan pendaftaran porsi haji.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>b. </td><td>Dukungan fasilitas layanan Bank dilakukan atas dasar pemberian kuasa (Wakalah) oleh Nasabah kepada Bank untuk pengurusan pendaftaran porsi haji dan penyimpanan dokumen terkait pendaftaran porsi haji tersebut dengan prinsip Wakalah.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>c. </td><td>Terhadap pemberian kuasa (Wakalah) tersebut, Nasabah diwajibkan membayar Imbalan / Ujrah kepada Bank, yang akan disepakati di dalam Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>d. </td><td>Bahwa pada prinsipnya Nasabah telah menerima manfaat atas jasa Bank ketika porsi haji berhasil di daftarkan, namun pembayaran atas Imbalan / Ujrah tersebut dapat diangsur sesuai dengan jangka waktu yang disepakati pada saat Akad.</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'><b>Pelaksanaan Prinsip Al-Qardh</b></td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>a. </td><td>Bahwa dalam memenuhi kebutuhannya untuk mendaftar porsi haji, Nasabah membutuhkan pinjaman dana (AlQardh) dari Bank.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>b. </td><td>Berdasarkan hasil penilaian Bank terhadap Nasabah, Bank bersedia memberikan pinjaman dana tersebut yang akan disepakati di dalam Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>c. </td><td>Nasabah wajib mengembalikan pinjaman dana (Al-Qardh) tersebut kepada Bank dalam Jangka Waktu yang disepakati di dalam Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>d. </td><td>Terhadap pemberian pinjaman dana (Al-Qardh) tersebut, Nasabah sepakat untuk membayar Biaya Administrasi yang akan disepakati di dalam Akad.</td></tr>"
                        +"<tr><td><b>IV. </b></td><td colspan='3'><b>Pencairan/Penarikan Pembiayaan</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 5</b></td></tr>"
                        +"<tr><td></td><td colspan='3'>Pembiayaan Al-Hajj hanya dapat ditarik oleh Nasabah jika menurut pendapat Bank persyaratan di bawah ini telah dipenuhi secara baik dan benar:</td></tr>"
                        +"<tr><td></td><td valign='top'>a. </td><td colspan='2'>Nasabah telah membuka rekening tabungan jemaah haji pada Bank.</td></tr>"
                        +"<tr><td></td><td valign='top'>b. </td><td colspan='2'>Nasabah telah memenuhi semua ketentuan dan persyaratan yang ditetapkan oleh Bank termasuk namun tidak terbatas pada pelunasan Biaya serta penutupan asuransi sesuai ketentuan Akad.</td></tr>"
                        +"<tr><td></td><td valign='top'>c. </td><td colspan='2'>Bank telah menerima semua Dokumen sebagaimana yang disyaratkan dalam Akad dan atau dokumen lainnya.</td></tr>"
                        +"<tr><td></td><td valign='top'>d. </td><td colspan='2'>Nasabah telah menandatangani Syarat-syarat Umum, dan telah tercapai kesepakatan berdasarkan Akad.</td></tr>"
                        +"<tr><td></td><td valign='top'>e. </td><td colspan='2'>Pernyataan dan jaminan Nasabah yang tercantum dalam Syarat-syarat Umum adalah benar, masih berlaku pada tanggal penarikan.</td></tr>"
                        +"<tr><td></td><td valign='top'>f. </td><td colspan='2'>Pada saat realisasi atau penarikan fasilitas Pembiayaan, hasil Sistem Informasi Debitur/Sistem Layanan Informasi Keuangan menunjukkan kolektibilitas Nasabah digolongkan lancar sesuai ketentuan Otoritas Jasa Keuangan.</td></tr>"
                        +"<tr><td><b>V. </b></td><td colspan='3'><b>Pengakuan Hutang dan Pembayaran Jumlah Kewajiban</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 6</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Dengan diterimanya Pembiayaan Al-Hajj oleh Nasabah, Nasabah mengaku berhutang kepada Bank, sebesar Jumlah Kewajiban yang belum dilunasi, dengan ketentuan:</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>a. </td><td>Untuk Transaksi Wakalah bil Ujroh. Nasabah dengan ini mengaku berutang pada Bank atas Jumlah Kewajiban Nasabah yang belum dilunasi kepada Bank sebagaimana disepakati dalam Akad. Oleh karenanya Nasabah dengan ini sekarang untuk nanti pada waktunya mengaku benar-benar dan secara sah telah berutang kepada Bank disebabkan karena kewajiban Nasabah yang timbul berdasarkan Akad, uang sejumlah Imbalan/Ujroah yang belum dibayar oleh Nasabah.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>b. </td><td>Untuk Pembiayaan berdasarkan Prinsip Al-Qardh. Nasabah dengan ini mengaku berutang pada Bank atas Jumlah Kewajiban Nasabah yang belum dilunasi kepada Bank sebagaimana diatur dalam Akad. Oleh karenanya Nasabah dengan ini sekarang untuk nanti pada waktunya mengaku benar-benar dan secara sah telah berutang kepada Bank disebabkan karena kewajiban Nasabah yang timbul berdasarkan Akad, uang sejumlah dana Al-Qardh, demikian berikut dengan Denda dan Biaya yang belum dibayar oleh Nasabah.</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Nasabah wajib membayar Jumlah Kewajiban sesuai dengan jadwal pembayaran sebagaimana disepakati dalam Akad.</td></tr>"
                        +"<tr><td></td><td valign='top'>3. </td><td colspan='2'>Untuk ketertiban pembayaran Jumlah Kewajiban oleh Nasabah kepada Bank, maka Nasabah memberi kuasa kepada Bank dan Bank menerima pemberian kuasa tersebut untuk membebani, mendebet rekening (-rekening) Nasabah di Bank sebesar Jumlah Kewajiban yang timbul pada tanggal jatuh tempo.</td></tr>"
                        +"<tr><td></td><td valign='top'>4. </td><td colspan='2'>Kuasa yang diberikan Nasabah tersebut dalam ayat 3 di atas adalah kuasa yang tidak dapat ditarik kembali dan tidak akan berakhir karena sebab-sebab yang disebutkan pada Pasal 1813, 1814 dan 1816 Kitab Undang-Undang Hukum Perdata.</td></tr>"
                        +"<tr style='text-align: right;'><td colspan='4'>Page <b>2</b> of <b>4</b></td></tr></table>"}}/>
                </Card.Body>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <Card.Body>
                    <div className="p-2 col-example text-justify" dangerouslySetInnerHTML={{__html: 
                        "<font size='2' face=''><table style='width:100%;'>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 7</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Jumlah Kewajiban dihitung tiap tanggal yang ditentukan oleh Bank di dalam Akad, dan Nasabah wajib membayar Jumlah Kewajiban tersebut atau bagiannya sesuai dengan jadwal yang disebutkan dalam Akad terkait, kecuali disetujui lain oleh Bank.</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Besarnya Imbalan ditentukan secara tersendiri dan disepakati Para Pihak di dalam Akad.</td></tr>"
                        +"<tr><td></td><td valign='top'>3. </td><td colspan='2'>Nasabah dengan ini berjanji untuk membayar Imbalan sebagaimana disepakati Para Pihak di dalam Akad.</td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 8</b></td></tr>"
                        +"<tr><td></td><td colspan='3'>Biaya yang berkenaan dengan Akad dibebankan kepada Nasabah. Biaya-biaya dan pengeluaran-pengeluaran demikian meliputi di antaranya, namun tidak terbatas pada:</td></tr>"
                        +"<tr><td></td><td valign='top'>a. </td><td colspan='2'>Bea meterai, biaya pendaftaran, pajak dan pungutan-pungutan lainnya yang dikenakan oleh pemerintah.</td></tr>"
                        +"<tr><td></td><td valign='top'>b. </td><td colspan='2'>Biaya-biaya dan pengeluaran-pengeluaran yang timbul berkenaan dengan penagihan Pembiayaan dan penyelesaian hutang piutang oleh Bank atau pihak ketiga atau instansi-instansi yang ditunjuk oleh Bank;</td></tr>"
                        +"<tr><td></td><td valign='top'>c. </td><td colspan='2'>Biaya-biaya dan pengeluaran-pengeluaran yang dibuat oleh Nasabah dalam melaksanakan pembayaran sesuatu kewajiban kepada Bank.</td></tr>"
                        +"<tr><td><b>VI. </b></td><td colspan='3'><b>Pembayaran Yang Dipercepat Dan Penggunaan Pembayaran</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 9</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Nasabah dapat mengajukan permohonan pembayaran yang dipercepat sebelum tanggal jatuh tempo yang telah ditentukan dalam Akad , sepanjang Nasabah telah memberitahukan secara tertulis terlebih dahulu kepada Bank selambat-lambatnya 14 (empat belas) Hari Kerja sebelum tanggal pelunasan dipercepat tersebut dengan ketentuan dan syarat yang ditentukan oleh Bank. Jika Bank menyetujui secara tertulis bahwa Nasabah melunasi Pembiayaan kepada Bank sebelum tanggal jatuh tempo, maka Nasabah harus membayar Jumlah Kewajiban yang belum jatuh tempo dimaksud.</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Pembayaran yang dipercepat sebagaimana dimaksud dalam ayat (1) Pasal ini dapat dilakukan untuk pelunasan dipercepat yang dilakukan dengan maksud untuk melunasi Jumlah Kewajiban sebelum jatuh tempo.</td></tr>"
                        +"<tr><td></td><td valign='top'>3. </td><td colspan='2'>Bank berhak namun tidak wajib memberikan potongan atau discount atas pembayaran Jumlah Kewajiban yang dipercepat sebagaimana dimaksud dalam ayat (1) Pasal ini.</td></tr>"
                        +"<tr><td><b>VII.</b></td><td colspan='3'><b>Kejadian-Kejadian Kelalaian/Cedera Janji</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 10</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Nasabah dianggap telah Cedera Janji, jika terjadi satu atau lebih kejadian-kejadian yang disebutkan di bawah ini:</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>a. </td><td>Nasabah tidak memenuhi pembayaran Jumlah Kewajiban (baik sebagiannya atau keseluruhannya) sebagaimana ditentukan berdasarkan jadwal pembayaran sebagaimana disepakat dalam Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>b. </td><td>Nasabah tidak dapat memenuhi dan/atau melanggar sebagian atau seluruh syarat dan ketentuan yang tercantum dalam Syarat-syarat Umum dan Akad.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>c. </td><td>Pernyataan dan jaminan yang diberikan Nasabah ternyata tidak benar atau palsu.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>d. </td><td>Nasabah tidak mungkin lagi atau tidak mempunyai dasar hukum untuk memenuhi sesuatu ketentuan kewajiban berdasarkan Syarat-syarat Umum atau Akad;</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Jika terjadi salah satu atau lebih peristiwa Cedera Janji sebagaimana diatur pada Pasal 10 (1), maka :</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>a. </td><td>Bank berhak menyatakan bahwa waktu pembayaran Jumlah Kewajiban telah jatuh tempo dan Jumlah Kewajiban harus dibayar sekaligus lunas dan segera atas tagihan pertama Bank, sekalipun jangka waktu Akad belum berakhir, tanpa diperlukan adanya surat pemberitahuan surat teguran atau surat lainnya.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>b. </td><td>Apabila Nasabah tidak melaksanakan pembayaran seketika dan sekaligus sebagaimana butir 2 a di atas, Bank berhak untuk membatalkan porsi haji atas nama Nasabah ataupun porsi haji atas nama lainnya yang diperolah dari pembiayaan Qardh dari Bank. Uang dari pembatalan porsi haji tersebut diperhitungkan sebagai pembayaran/pelunasan sisa Jumlah Kewajiban Nasabah kepada Bank setelah dikurangi biaya-biaya.</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>c. </td><td>Jika hasil pembatalan porsi haji tidak mencukupi untuk membayar Jumlah Kewajiban Nasabah kepada Bank, maka Nasabah tetap bertanggung jawab untuk melunasi sisa Jumlah Kewajiban yang belum dibayar sampai dengan lunas.</td></tr>"
                        +"<tr><td><b>VIII.</b></td><td colspan='3'><b>Kesanggupan Nasabah</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 11</b></td></tr>"
                        +"<tr><td></td><td colspan='3'>Nasabah dengan ini berjanji, bahwa selama Nasabah masih berhutang kepada Bank, Nasabah menyanggupi untuk melakukan hal-hal tersebut di bawah ini:</td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>segera memberitahukan Bank secara tertulis tentang terjadinya suatu peristiwa yang disebutkan pada Pasal 10 di atas, atau hal yang dengan pemberitahuan atau dengan lewatnya waktu atau kedua-duanya akan merupakan suatu pelanggaran terhadap Pasal 10 di atas;</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>tidak akan mengajukan permohonan kepada pengadilan atau pihak yang berwenang lainnya untuk dinyatakan pailit atau agar diangkat pengampu atas suatu bagian atau semua aset Nasabah;</td></tr>"
                        +"<tr><td></td><td valign='top'>3. </td><td colspan='2'>membayar semua Biaya yang dijatuhkan oleh Bank, termasuk biaya yang dipersyaratkan sebelum proses pencairan pembiayaan dilakukan.</td></tr>"
                        +"<tr><td></td><td valign='top'>4. </td><td colspan='2'>memberikan dan telah menandatangani Surat Kuasa kepada Bank untuk melakukan pembatalan porsi haji atas nama Nasabah dan/atau atas nama pihak pihak lain yang pendaftaran porsinya menggunakan dana pencairan pembiayaan dari Bank kepada Nasabah.</td></tr>"
                        +"<tr style='text-align: right;'><td colspan='4'>Page <b>3</b> of <b>4</b></td></tr></table>"}}/>
                </Card.Body>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <Card.Body>
                    <div className="p-2 col-example text-justify" dangerouslySetInnerHTML={{__html: 
                        "<font size='2' face=''><table style='width:100%;'>"
                        +"<tr><td><b>IX. </b></td><td colspan='3'><b>Pernyataan Dan Jaminan Nasabah</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 12</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Nasabah dengan ini menyatakan dan menjamin, bahwa pada tanggal Akad ditandatangani:</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>a. </td><td>Nasabah berhak dan berwenang sepenuhnya secara sah untuk membuat Akad serta melaksanakan semua kewajibannya berdasarkan Akad sehingga Akad mengikat Nasabah dengan sah sesuai dengan ketentuan dalam Syarat-syarat Umum, Akad;</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>b. </td><td>semua dokumen termasuk tetapi tidak terbatas pada dokumen yang diserahkan oleh Nasabah kepada Bank sehubungan dengan:<br/>1) Pemberian Pembiayaan oleh Bank kepada Nasabah, dan<br/>2) Surat Kuasa Pembatalan Porsi Haji</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>c. </td><td>Nasabah telah memperoleh penjelasan dari Bank atas ketentuan dan syarat yang tercantum pada Syarat-syarat Umum, Akad , sehingga Nasabah sepenuhnya mengetahui dan mengerti serta menyetujui semua ketentuan dalam Syarat-syarat Umum, Akad;</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>d. </td><td>Tiada hal atau peristiwa yang merupakan suatu cedera janji atas suatu perjanjian;</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Jika di kemudian hari ternyata ada Pernyataan dan Jaminan Nasabah yang diuraikan dalam Pasal 12 ayat 1 tersebut di atas tidak benar dan menimbulkan kerugian pada Bank, maka Nasabah wajib bertanggung jawab dan mengganti seluruh kerugian yang diderita oleh Bank sekaligus lunas atas permintaan pertama Bank. Pertanggungjawaban Nasabah tersebut termasuk juga terhadap adanya gugatan dari Pihak Ketiga.</td></tr>"
                        +"<tr><td><b>X. </b></td><td colspan='3'><b>Hal-hal Lain</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 13</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Kecuali disepakati lain, maka Syarat-syarat Umum ini dan Akad hanya dapat diubah dengan suatu dokumen tertulis oleh Para Pihak.</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Akad (termasuk Syarat-syarat Umum ini mengikat Para Pihak pada Akad dan para pengganti hak atau penerima hak dari para pihak masing-masing dengan ketentuan bahwa Nasabah tidak berhak menyerahkan atau mengalihkan suatu atau semua hak Nasabah berdasarkan Akad tanpa mendapatkan persetujuan tertulis terlebih dahulu dari Bank.</td></tr>"
                        +"<tr><td></td><td valign='top'>3. </td><td colspan='2'>Syarat-syarat Umum dan Akad berlaku sejak tanggal ditandatangani dan/atau disepakati oleh Bank dan Nasabah sampai seluruh Jumlah Kewajiban dinyatakan lunas secara tertulis oleh Bank.</td></tr>"
                        +"<tr><td></td><td valign='top'>4. </td><td colspan='2'>Akad dan pelaksanaan Akad tunduk kepada dan diatur berdasarkan hukum Republik Indonesia serta Prinsip Syariah.</td></tr>"
                        +"<tr><td></td><td valign='top'>5. </td><td colspan='2'>Syarat-syarat Umum ini telah disesuaikan dengan ketentuan peraturan perundang-undangan termasuk ketentuan peraturan Otoritas Jasa Keuangan.</td></tr>"
                        +"<tr><td></td><td valign='top'>6. </td><td colspan='2'>Dalam rangka Good Corporate Governance, Nasabah tidak akan memberikan suatu pemberian atau imbalan dalam bentuk apapun kepada pejabat atau karyawan Bank atau pihak yang mewakili kepentingan Bank berkenaan dengan pemberian Pembiayaan ini.</td></tr>"
                        +"<tr><td><b>XI. </b></td><td colspan='3'><b>Komunikasi dan Pemberitahuan</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 14</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Alamat Pemberitahuan</td></tr>"
                        +"<tr><td></td><td></td><td colspan='2'>Semua surat menyurat atau pemberitahuan yang dikirim oleh masing-masing pihak kepada pihak yang lain harus dilakukan dengan surat tercatat, melalui kurir (ekspedisi), pesan singkat, surat elektronik, media elektronik lain, atau faksimili ke alamat-alamat sebagaimana disepakati di dalam Akad atau Aplikasi Permohonan Pembiayaan.</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Pemberitahuan dari salah satu pihak kepada pihak lainnya dianggap diterima:</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>a. </td><td>Jika dikirim melalui kurir (ekspedisi) pada tanggal penerimaan;</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>b. </td><td>Jika dikirim melalui pos tercatat 7 (tujuh) hari setelah tanggal pengirimannya;</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>c. </td><td>Jika dikirim melalui pesan singkat, surat elektronik (e-mail), ataupun media elektronik lainnya milik nasabah, dan/atau;</td></tr>"
                        +"<tr><td></td><td></td><td valign='top'>d. </td><td>Jika dikirim melalui faksimili, pada hari pengirimannya, dan/atau;</td></tr>"
                        +"<tr><td></td><td valign='top'>3. </td><td colspan='2'>Nasabah dapat mengganti alamat, nomor telepon, ataupun alamat surat elektronik dengan memberitahukan secara tertulis kepada Bank. Perubahan alamat tersebut dianggap diterima oleh Bank sesuai dengan ketentuan ayat 2 di atas.</td></tr>"
                        +"<tr><td></td><td valign='top'>4. </td><td colspan='2'>Dalam hal terjadi perubahan alamat Bank, pemberitahuan perubahan alamat Bank melalui media massa (cetak) berskala nasional atau lokal merupakan pemberitahuan resmi kepada Nasabah.</td></tr>"
                        +"<tr><td><b>XII.</b></td><td colspan='3'><b>Hukum yang Berlaku dan Penyelesaian Sengketa</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 15</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>Syarat-syarat Umum ini tunduk pada Hukum Negara Republik Indonesia.</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Dalam hal terjadi perselisihan mengenai pelaksanaan dan penafsiran terhadap Syarat-Syarat Umum ini maupun Akad ini, Para Pihak terlebih dahulu akan menyelesaikan perselisihan tersebut dengan cara musyawarah untuk mencapai mufakat dalam waktu paling lama 30 (tiga puluh) hari kalendar sejak tanggal perselisihan diajukan oleh salah satu Pihak. Musyawarah ini dapat dilakukan secara bilateral, maupun melalui Lembaga Alternatif Penyelesaian Sengketa Perbankan Indonesia.</td></tr>"
                        +"<tr><td></td><td valign='top'>3. </td><td colspan='2'>Apabila penyelesaian secara musyawarah mufakat sebagaimana dimaksud dalam ayat 2 Pasal ini tidak tercapai, maka Para Pihak sepakat menyerahkan penyelesaian perselisihan ini di Pengadilan Agama dengan memilih tempat kedudukan yang umum dan tetap pada Kantor Kepaniteraan Pengadilan Agama yang wilayahnya meliputi keberadaan cabang Bank yang memberikan Pembiayaan ini.</td></tr>"
                        +"<tr><td><b>XIII.</b></td><td colspan='3'><b>Penutup</b></td></tr>"
                        +"<tr><td></td><td colspan='3'><b>Pasal 16</b></td></tr>"
                        +"<tr><td></td><td valign='top'>1. </td><td colspan='2'>NASABAH telah membaca, memahami, dan menyetuju iisi dari Syarat-Syarat Umum ini</td></tr>"
                        +"<tr><td></td><td valign='top'>2. </td><td colspan='2'>Nasabah telah mendapatkan penjelasan yang cukup dari petugas Bank terkait dengan Syarat-Syarat Umum ini dan/atau Akad terkait.</td></tr>"
                        +"<tr style='text-align: right;'><td colspan='4'>Page <b>4</b> of <b>4</b></td></tr></table>"}}/>
                </Card.Body>
                </Card>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                    <button className="btn btn-outline-success btn-sm" 
                            type='button' 
                            id="clsmodal"
                            onClick={this.props.handleModalalhajj}
                            title='Kembali'>KEMBALI</button>
            </Modal.Footer>
            </Modal>
        )
    }
}

export default ProhajjAgremenAlHajj
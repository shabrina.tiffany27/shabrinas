import React, { Component } from 'react'
import {Container, Row, Col, Card, Form, Button } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import ModalConfirm from './ModalConfirm';
import ProhajjReview from './ProhajjReview';
//import { ToastContainer } from 'react-toastify';

export class EformReview extends Component {
    continue = e => {
        e.preventDefault();
        this.props.handleSaveTemp();
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.onTop();  
        this.props.loadBdgUsh();
    }
    
    render() {
        const { handleSaveTemp } = this.props
        return (
            <div>
                <Header prod = {this.props.rview.kd_produk} eprod = {this.props.rview.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.rview.batal}/>
                <ProhajjReview 
                    handleModalrview = {this.props.handleModalrview}
                    toCurrency={this.props.toCurrency}
                    dateNow={this.props.dateNow}
                    mview = {this.props.rview.mrview}
                    mdata = {this.props.rview}
                    loadBdgUsh = {this.props.loadBdgUsh}
                    mjob={this.props.mjob}
                />
                <Form onSubmit={this.continue} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'80%',textAlign:'left'}}><h5>Submit Data {'&'} Dokumen</h5></td>
                            <td style={{width:'20%',textAlign:'right'}}>Page 6</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                            <div className="p-2 col-example text-left"
                                dangerouslySetInnerHTML={{__html: "<b>Terima kasih, anda telah selesai mengisi seluruh data dan dokumen pada e-Form ini.<br/>"
                                        +"<br/>Pastikan kebenaran seluruh data dan dokumen yang sudah anda isi."
                                        +"<br/><br/>Klik Preview Data untuk mengecek data dan dokumen anda pada e-Form ini.</b>"
                                }}/>
                            </Col>
                        </Row>
                        <Button className="text-center" variant='success' id='review' size='sm' onClick={this.props.handleModalrview}>Review Data</Button>
                        <Row>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='submit' 
                                            id='continue'
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformReview

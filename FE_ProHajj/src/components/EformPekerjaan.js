import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {TextField, MenuItem} from '@material-ui/core/';
import ModalConfirm from './ModalConfirm';
import NumberFormat from 'react-number-format';
import Select from 'react-select';
import dataBankLain from '../csvjson.json';
//import { ToastContainer } from 'react-toastify';

export class EformPekerjaan extends Component {
    continue = e => {
        e.preventDefault();
        this.props.handleSaveTemp();
        this.props.nextStep(1);
        console.log(this.props.mjob.pekerjaan.bidangktr.length)
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.onTop(); 
        this.props.loadBdgUsh();
    }
    
    LamaBln= (e) => {
        var max = 11;
        var msg = '';
        if(e > max) {
            msg = 'Maksimal 11 bulan ...';
        }
        return msg;
    }
    render() {
        const customStyles = {
            control: (provided, state) => ({
             ...provided,
             boxShadow: '0 0 0 0',
             borderRadius: '0px',
             borderStyle: 'none',
             borderWidth: '0px',
             borderBottom: '1px solid rgba(0,0,0,0.4)',
             marginTop: '6px',
             ':hover' : {
                 borderStyle: 'none',
                 borderBottom: '2px solid'
             },
             ':focus' : {
                 borderStyle: 'none'
             }
                
            }),
            indicatorSeparator: (provided) => ({
                ...provided,
                width: '0px'
            }),
            input: (provided) => ({
                ...provided,
                fontFamily: 'Roboto',
                marginLeft: '0px',
                paddingBottom: '0px',
                paddingTop: '8px'
            }),
            placeholder: (provided) => ({
                ...provided,
                fontFamily: 'Roboto',
            }),
            valueContainer: (provided) => ({
                ...provided,
                padding: "0px 0px"
            }),
            singleValue: (provided) => ({
                ...provided,
                fontFamily: 'Roboto',
            }),
            option: (provided) => ({
                ...provided,
                fontFamily: 'Roboto',
                textAlign: 'left'
            })
        }
        //const numOnly = /^[0-9\b]+$/;
        const { handleChangeK, handleSaveTemp, loadBdgUsh } = this.props
        const jnspkrjn = 'Pemilik Usaha'
        const jnspkrjanEnable = (this.props.mjob.pekerjaan.jnspkjfos === '2' || this.props.mjob.pekerjaan.jnspkjfos === '4')
        const istsiLainShow=(this.props.mjob.pekerjaan.ktgrinst === '99');
        const phtLainShow=(this.props.mjob.pekerjaan.phasiltambah === '0');
        const stsEnable=(this.props.mjob.pekerjaan.jnspkjfos === '1' || this.props.mjob.pekerjaan.jnspkjfos === '3');
        const isRequired=(this.props.mjob.pekerjaan.jnspkjfos === '2' || this.props.mjob.pekerjaan.jnspkjfos === '4');
        const bdgUsha = this.props.mjob.bdgush;
        let dtbdgush = (this.props.mjob.pekerjaan.bidangktr === '' ? '' : bdgUsha.find(o => o.bidangcd === this.props.mjob.pekerjaan.bidangktr).bidangnama);
        console.log(this.props.mjob.pekerjaan.pbygaji)
        console.log('ada gk? ',bdgUsha.find(o => o.bidangcd === this.props.mjob.pekerjaan.bidangktr), this.props.mjob.pekerjaan.bidangktr)
        // console.log('status kerja: ', this.props.mjob.pekerjaan.stspkj)
        const pbygajiLainShow=(this.props.mjob.pekerjaan.pbygaji !== '' && this.props.mjob.pekerjaan.pbygaji !== 'Tunai' && this.props.mjob.pekerjaan.pbygaji !== 'Bank Muamalat' );
        let dtbanklain = (this.props.mjob.pekerjaan.pbygajikode === '' ? '' : dataBankLain.find(o => o.BANK_CODE === this.props.mjob.pekerjaan.pbygajikode).BANK_DESC);
        const checkTlpKtr = () => {
            if(this.props.mjob.pekerjaan.tlpktr.length > 0) {
                if(this.props.mjob.pekerjaan.tlpktr.length < 7) {
                    return "Nomor HP tidak valid, digit terlalu sedikit"
                }
            }
        }
        const checkTlpHrd = () => {
            if(this.props.mjob.pekerjaan.tlphrdktr.length > 0) {
                if(this.props.mjob.pekerjaan.tlphrdktr.length < 7) {
                    return "Nomor HP tidak valid, digit terlalu sedikit"
                }
            }
        }
        const checkBdg = () => {
            if(this.props.mjob.pekerjaan.bidangktr.length === 0) {
                return "Bidang usaha tidak boleh kosong"
                console.log(this.props.mjob.pekerjaan.bidangktr.length)
            }
        }
        const checkBnkLain = () => {
            if(pbygajiLainShow){
                if(this.props.mjob.pekerjaan.pbygajikode.length === 0) {
                    return "Field ini tidak boleh kosong"
                }
            }
        }

        return (
            <div>
                <Header prod = {this.props.mjob.kd_produk} eprod = {this.props.mjob.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.mjob.batal}/>
                <Form onSubmit={this.continue} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'50%',textAlign:'left'}}><h5>Data Pekerjaan Pemohon</h5></td><td style={{width:'50%',textAlign:'right'}}>Page 2-1</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                                <Form.Group >
                                    <TextField
                                        label="Jenis Pekerjaan"
                                        required
                                        fullWidth
                                        value={this.props.mjob.pekerjaan.jnspkjfos}
                                        name="jnspkjfos"
                                        onChange={handleChangeK}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'1'}>Karyawan</MenuItem>
                                            <MenuItem value={'2'}>Self Employee</MenuItem>
                                            <MenuItem value={'4'}>Profesional (Self Employee)</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField disabled={!stsEnable}
                                        label="Status Pekerjaan"
                                        required={stsEnable}
                                        fullWidth
                                        value={(jnspkrjanEnable ? jnspkrjn : this.props.mjob.pekerjaan.stspkj) && (this.props.mjob.pekerjaan.stspkj === 'Pemilik Usaha' ? !jnspkrjanEnable ? '' : this.props.mjob.pekerjaan.stspkj : jnspkrjanEnable ? jnspkrjn : this.props.mjob.pekerjaan.stspkj )}
                                        // value={this.props.mjob.pekerjaan.stspkj === ''? this.props.mjob.pekerjaan.jnspkjfos === '' ? '' : this.props.mjob.pekerjaan.jnspkjfos === '1' ? this.props.mjob.pekerjaan.stspkj :}
                                        name="stspkj"
                                        onChange={handleChangeK}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'CON'}>Karyawan Tetap</MenuItem>
                                            <MenuItem value={'FREE'}>Karyawan Kontrak</MenuItem>
                                            <MenuItem style={{display:'none'}} value={'Pemilik Usaha'}>Pemilik Usaha</MenuItem>
                                    </TextField>
                                    {console.log('status kerja: ', this.props.mjob.pekerjaan.stspkj)}
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Nama Perusahaan"
                                        required
                                        fullWidth
                                        size="small"
                                        value={this.props.mjob.pekerjaan.namaprsh}
                                        name="namaprsh"
                                        onChange={handleChangeK}
                                        variant="standard"
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <table style={{width:'100%'}}>
                                    <tbody>
                                        <tr></tr>
                                        <tr>
                                            <td style={{width:'30%',textAlign:'center'}}>
                                            <Form.Group>
                                                <TextField
                                                    label="Kategori Instansi"
                                                    required
                                                    fullWidth
                                                    value={this.props.mjob.pekerjaan.ktgrinst}
                                                    name="ktgrinst"
                                                    onChange={handleChangeK}
                                                    variant="standard"
                                                    size="small"
                                                    select>
                                                        <MenuItem value={'1'}>Pemerintah</MenuItem>
                                                        <MenuItem value={'2'}>BUMN</MenuItem>
                                                        <MenuItem value={'3'}>Swasta Asing</MenuItem>
                                                        <MenuItem value={'4'}>Swasta Nasional</MenuItem>
                                                        <MenuItem value={'5'}>TNI/POLRI</MenuItem>
                                                        <MenuItem value={'99'}>Lainnya</MenuItem>
                                                </TextField>
                                            </Form.Group>
                                            </td>
                                            <td style={{width:'70%',textAlign:'center'}} hidden={!istsiLainShow}>
                                                <Form.Group>
                                                   <TextField
                                                       label="Lainnya"
                                                       required
                                                       disabled={!istsiLainShow}
                                                       fullWidth
                                                       size="small"
                                                       value={this.props.mjob.pekerjaan.ktgrinstlain}
                                                       name="ktgrinstlain"
                                                       onChange={handleChangeK}
                                                       variant="standard"
                                                       //helperText=""
                                                   />
                                                </Form.Group>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Col>        
                            <Col md>
                                <Form.Group>
                                    <Select
                                        required
                                        defaultInputValue={dtbdgush}
                                        isClearable
                                        styles={customStyles}
                                        placeholder={'Pilih Bidang Usaha'}
                                        name="bidangktr"
                                        options={bdgUsha.map(opt => ({label:opt.bidangnama, value:opt.bidangcd}))}
                                        onChange={(e) => {
                                            const objData = {
                                                target:{
                                                    name:"bidangktr",
                                                    value:e === null ? '':e.value
                                                }
                                            }
                                            handleChangeK(objData)
                                        }}
                                        helperText={checkBdg}
                                    />
                                    <label style={{
                                       fontSize: '12px',
                                       fontFamily: 'Roboto',
                                       color: 'rgba(0, 0, 0, 0.54)',
                                       position: 'absolute',
                                       top: '0px',
                                       left: '15px'
                                    }}>Bidang Usaha *</label>

                                    {/* <TextField
                                        label="Bidang Usaha"
                                        required
                                        fullWidth
                                        size="small"
                                        value={this.props.mjob.pekerjaan.bidangktr}
                                        name="bidangktr"
                                        onChange={handleChangeK}
                                        variant="standard"
                                        //helperText="Tinggal"
                                        select>
                                            {
                                            bdgUsha.map((h) => 
                                            (<MenuItem key={h.bidangcd} value={h.bidangcd}>{h.bidangnama}</MenuItem>))
                                            }
                                    </TextField> */}
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Jabatan"
                                        required
                                        fullWidth
                                        size="small"
                                        value={this.props.mjob.pekerjaan.jabatan}
                                        name="jabatan"
                                        onChange={handleChangeK}
                                        variant="standard"
                                        //helperText="Tinggal"
                                        select>
                                            <MenuItem value={'Staff'}>Staff</MenuItem>
                                            <MenuItem value={'Supervisor'}>Supervisor</MenuItem>
                                            <MenuItem value={'Manager'}>Manager</MenuItem>
                                            <MenuItem value={'Head/General Manager'}>Head/General Manager</MenuItem>
                                            <MenuItem value={'Direktur'}>Direktur</MenuItem>
                                            <MenuItem value={'Komisaris'}>Komisaris</MenuItem>
                                            <MenuItem value={'Dosen'}>Dosen</MenuItem>
                                            <MenuItem value={'Others'}>Others</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={4}>
                                <table style={{width:'100%'}}>
                                    <tbody>
                                        <tr></tr>
                                        <tr>
                                            <td style={{width:'50%',textAlign:'center'}}>
                                            <Form.Group>
                                                <TextField 
                                                    label="Tahun"
                                                    required
                                                    fullWidth
                                                    size="small"
                                                    value={this.props.mjob.pekerjaan.lmkrjathn}
                                                    name="lmkrjathn"
                                                    onChange={handleChangeK}
                                                    variant="standard"
                                                    helperText="Lama Bekerja"
                                                />
                                            </Form.Group>
                                            </td>
                                            <td style={{width:'50%',textAlign:'center'}}>
                                            <Form.Group>
                                                <TextField
                                                    label="Bulan"
                                                    required
                                                    fullWidth
                                                    size="small"
                                                    value={this.props.mjob.pekerjaan.lmkrjabln}
                                                    name="lmkrjabln"
                                                    onChange={handleChangeK}
                                                    variant="standard"
                                                    helperText={this.LamaBln(this.props.mjob.pekerjaan.lmkrjabln) || 'Lama Bekerja'}
                                                    error={Boolean(this.LamaBln(this.props.mjob.pekerjaan.lmkrjabln))}
                                                />
                                            </Form.Group>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Col>
                            <Col md={3}>
                                <Form.Group >
                                    <NumberFormat customInput={TextField}
                                    //label="Income"
                                    label="Pendapatan Per Bulan"
                                    required={true}
                                    fullWidth
                                    size="small"
                                    value={this.props.mjob.pekerjaan.pendptn}
                                    name="pendptn"
                                    onChange={handleChangeK}
                                    variant="standard"
                                    thousandSeparator={'.'} 
                                    decimalSeparator={','}
                                    prefix={'Rp'} 
                                />
                                </Form.Group>
                            </Col>
                            <Col md>
                            <table style={{width:'100%'}}>
                                    <tbody>
                                        <tr></tr>
                                        <tr>
                                            <td style={{width:'46%',textAlign:'start'}}>
                                            <Form.Group>
                                                <TextField
                                                    label="Pembayaran Gaji/Usaha"
                                                    required
                                                    fullWidth
                                                    size="small"
                                                    value={(this.props.mjob.pekerjaan.pbygaji === 'Bank Lain' || this.props.mjob.pekerjaan.pbygaji === 'Bank Muamalat' || this.props.mjob.pekerjaan.pbygaji === 'Tunai') ? this.props.mjob.pekerjaan.pbygaji : this.props.mjob.pekerjaan.pbygaji === '' ? '' : 'Bank Lain'}
                                                    name="pbygaji"
                                                    onChange={handleChangeK}
                                                    variant="standard"
                                                    //helperText="Tinggal"
                                                    select>
                                                        <MenuItem value={'Bank Lain'}>Bank Lain</MenuItem>
                                                        <MenuItem value={'Bank Muamalat'}>Bank Muamalat</MenuItem>
                                                        <MenuItem value={'Tunai'}>Tunai</MenuItem>
                                                </TextField>
                                            </Form.Group>
                                            </td>
                                            <td style={{width:'70%',textAlign:'center'}} hidden={!pbygajiLainShow}>
                                                <Form.Group>
                                                <Select
                                                    required={pbygajiLainShow}
                                                    defaultInputValue={dtbanklain}
                                                    isClearable
                                                    styles={customStyles}
                                                    name="pbygajikode"
                                                    options={dataBankLain.map(opt => ({label:opt.BANK_DESC, value:opt.BANK_CODE}))}
                                                    onChange={(e) => {
                                                        const objData = {
                                                            target:{
                                                                name:"pbygajikode",
                                                                value:e === null ? '':e.value
                                                            }
                                                        }
                                                        handleChangeK(objData)
                                                    }}
                                                    // onInputChange={(opt) => this.setState({banklaindisplay:opt.label})}
                                                    placeholder={'Bank Lainnya'}
                                                />
                                            </Form.Group>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={4}>
                                <Form.Group >
                                    <TextField
                                        //label="Non Fixed Income"
                                        label="Penghasilan Tidak Tetap Per Bulan"
                                        required
                                        fullWidth
                                        value={this.props.mjob.pekerjaan.phasiltdktetap}
                                        name="phasiltdktetap"
                                        onChange={handleChangeK}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'1'} >{'<Rp10 Juta'}</MenuItem>
                                            <MenuItem value={'2'} >{'>Rp10 Juta <Rp50 Juta'}</MenuItem>
                                            <MenuItem value={'3'} >{'>Rp50 Juta'}</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md={3}>
                                <Form.Group >
                                    <TextField
                                        //label="Pengeluaran Tetap"
                                        label="Pengeluaran Tetap Per Bulan"
                                        required
                                        fullWidth
                                        value={this.props.mjob.pekerjaan.pluartetap}
                                        name="pluartetap"
                                        onChange={handleChangeK}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'1'} >{'<Rp10 Juta'}</MenuItem>
                                            <MenuItem value={'2'} >{'>Rp10 Juta <Rp50 Juta'}</MenuItem>
                                            <MenuItem value={'3'} >{'>Rp50 Juta'}</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <table style={{width:'100%'}}>
                                    <tbody>
                                        <tr></tr>
                                        <tr>
                                            <td style={{width:'60%', textAlign:'start'}}>
                                            <Form.Group>
                                                <TextField
                                                    //label="Additional Income"
                                                    label="Informasi Penghasilan Tambahan"
                                                    required
                                                    fullWidth
                                                    value={this.props.mjob.pekerjaan.phasiltambah}
                                                    name="phasiltambah"
                                                    onChange={handleChangeK}
                                                    variant="standard"
                                                    size="small"
                                                    select>
                                                        <MenuItem value={'1'}>Kerja Paruh Waktu</MenuItem>
                                                        <MenuItem value={'2'}>Hasil Usaha</MenuItem>
                                                        <MenuItem value={'3'}>Hasil Sewa</MenuItem>
                                                        <MenuItem value={'4'}>Deviden</MenuItem>
                                                        <MenuItem value={'5'}>Investasi</MenuItem>
                                                        <MenuItem value={'6'}>Warisan</MenuItem>
                                                        <MenuItem value={'0'}>Lainnya</MenuItem>
                                                </TextField>
                                            </Form.Group>
                                            </td>
                                            <td style={{width:'50%',textAlign:'center'}} hidden={!phtLainShow}>
                                                <Form.Group>
                                                   <TextField
                                                       label="Lainnya"
                                                       required
                                                       disabled={!phtLainShow}
                                                       fullWidth
                                                       size="small"
                                                       value={this.props.mjob.pekerjaan.phasiltambahlain}
                                                       name="phasiltambahlain"
                                                       onChange={handleChangeK}
                                                       variant="standard"
                                                       //helperText=""
                                                   />
                                                </Form.Group>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Col> 
                        </Row>
                        <Row>
                        <Col md>
                                <table style={{width:'100%'}}>
                                        <tbody>
                                            <tr>
                                                <td style={{width:'25%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <NumberFormat customInput={TextField}
                                                            format="####"
                                                            hinttext=""
                                                            placeholder=""
                                                            label="Kd.Area"
                                                            //type="number"
                                                            required={isRequired}
                                                            fullWidth
                                                            size="small"
                                                            value={this.props.mjob.pekerjaan.tlpktrkd}
                                                            name="tlpktrkd"
                                                            onChange={handleChangeK}
                                                            variant="standard"
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td style={{width:'50%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <NumberFormat customInput={TextField}
                                                            // format="##########"
                                                            hinttext=""
                                                            placeholder=""
                                                            label="No.Telp. Kantor"
                                                            //type="number"
                                                            required={isRequired}
                                                            fullWidth
                                                            size="small"
                                                            value={this.props.mjob.pekerjaan.tlpktr}
                                                            name="tlpktr"
                                                            onChange={handleChangeK}
                                                            variant="standard"
                                                            error = {checkTlpKtr()}
                                                            helperText = {checkTlpKtr()}
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td style={{width:'25%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <TextField
                                                        label="Ext"
                                                        //required={isRequired}
                                                        fullWidth
                                                        value={this.props.mjob.pekerjaan.extktr}
                                                        name="extktr"
                                                        onChange={handleChangeK}
                                                        variant="standard"
                                                        size="small"
                                                        />
                                                    </Form.Group>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </Col>
                            <Col md>
                                <table style={{width:'100%'}}>
                                        <tbody>
                                            <tr>
                                                <td style={{width:'25%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <NumberFormat customInput={TextField}
                                                            format="####"
                                                            hinttext=""
                                                            placeholder=""
                                                            label="Kd.Area"
                                                            //type="number"
                                                            required={stsEnable}
                                                            disabled={!stsEnable}
                                                            fullWidth
                                                            size="small"
                                                            value={this.props.mjob.pekerjaan.tlphrdkd}
                                                            name="tlphrdkd"
                                                            onChange={handleChangeK}
                                                            variant="standard"
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td style={{width:'50%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <NumberFormat customInput={TextField}
                                                            // format="##########"
                                                            hinttext=""
                                                            placeholder=""
                                                            label="No.Telp. HRD"
                                                            //type="number"
                                                            required={stsEnable}
                                                            disabled={!stsEnable}
                                                            fullWidth
                                                            size="small"
                                                            value={this.props.mjob.pekerjaan.tlphrdktr}
                                                            name="tlphrdktr"
                                                            onChange={handleChangeK}
                                                            variant="standard"
                                                            error = {checkTlpHrd()}
                                                            helperText = {checkTlpHrd()}
                                                        />
                                                    </Form.Group>
                                                </td>
                                                <td style={{width:'25%',textAlign:'center'}}>
                                                    <Form.Group>
                                                        <TextField
                                                        label="Ext"
                                                        //required={isRequired}
                                                        disabled={!stsEnable}
                                                        fullWidth
                                                        value={this.props.mjob.pekerjaan.exthrd}
                                                        name="exthrd"
                                                        onChange={handleChangeK}
                                                        variant="standard"
                                                        size="small"
                                                        />
                                                    </Form.Group>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='submit' 
                                            id='continue'
                                            disabled={checkTlpKtr() || checkTlpHrd() || checkBdg() || checkBnkLain() }
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformPekerjaan
//<MenuItem value={'3'}>Profesional (Karyawan)</MenuItem>
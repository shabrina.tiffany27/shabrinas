import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {TextField, MenuItem} from '@material-ui/core/';
import ModalConfirm from './ModalConfirm';
//import { ToastContainer } from 'react-toastify';

export class EformPemohon3 extends Component {
    continue = e => {
        e.preventDefault();
        this.props.handleSaveTemp();
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        let i = 1
        if (this.props.pbypmhn.pemohon.almnowktp ==='sda'){ i = 2}
        this.props.prevStep(i);
    }
    componentDidMount() {
        this.props.onTop();  
    }
   
    render() {
        //const numOnly = /^[0-9\b]+$/;
        //console.log(['IBU','MAMA','IBUNDA','UMI','MAMI'].includes(this.props.pbypmhn.pemohon.ibukdg));
        const { handleChangeP, handleSaveTemp } = this.props
        const pgex = (this.props.pbypmhn.pemohon.almnowktp ==='sda'?'3':'4');
        const erroribu = (['IBU','MAMA','IBUNDA','UMI','MAMI'].includes(this.props.pbypmhn.pemohon.ibukdg))
        const containword= (erroribu?'Nama Gadis Ibu kandung harus sesuai dengan KTP':'')
        const checkNoTlp = () => {
            if(this.props.pbypmhn.pemohon.notlpnow.length > 0) {
                if(this.props.pbypmhn.pemohon.notlpnow.length < 10) {
                    return "Nomor Telepon tidak valid, digit terlalu sedikit"
                }
                else if (this.props.pbypmhn.pemohon.notlpnow.length > 13) {
                    return "Nomor Telepon tidak valid, digit terlalu banyak"
                }
            }
        }
        const notlpLenght = (this.props.pbypmhn.pemohon.notlpnow.length === 0 || this.props.pbypmhn.pemohon.notlpnow.length >= 10 && this.props.pbypmhn.pemohon.notlpnow.length < 14)

        return (
            <div>
                <Header prod = {this.props.pbypmhn.kd_produk} eprod = {this.props.pbypmhn.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.pbypmhn.batal}/>
                <Form onSubmit={this.continue} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'50%',textAlign:'left'}}><h5>Data Pemohon</h5></td><td style={{width:'50%',textAlign:'right'}}>Page 1-{pgex}</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                                <Form.Group >
                                    <TextField
                                    label="Alamat Surat Menyurat"
                                    required
                                    fullWidth
                                    value={this.props.pbypmhn.pemohon.almsurat}
                                    name="almsurat"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    select>
                                        <MenuItem value={'10'}>Alamat KTP</MenuItem>
                                        <MenuItem value={'20'}>Alamat Tinggal</MenuItem>
                                        <MenuItem value={'30'}>Alamat Kantor</MenuItem>
                                        <MenuItem value={'40'}>Alamat Email</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="No.Telp. Rumah"
                                        required
                                        fullWidth
                                        size="small"
                                        value={this.props.pbypmhn.pemohon.notlpnow}
                                        name="notlpnow"
                                        onChange={handleChangeP}
                                        variant="standard"
                                        //placeholder="(555) 5555-5555"
                                        error = {checkNoTlp()}
                                        helperText={notlpLenght? "Dapat diisi dengan No.HP Nasabah jika tidak ada No.Telp Rumah":checkNoTlp()}
                                        //color = 'secondary'
                                    />
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Pendidikan Terakhir"
                                        required
                                        fullWidth
                                        value={this.props.pbypmhn.pemohon.pendidikan}
                                        name="pendidikan"
                                        onChange={handleChangeP}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'00'}>SD</MenuItem>
                                            <MenuItem value={'01'}>SMP</MenuItem>
                                            <MenuItem value={'02'}>SMA</MenuItem>
                                            <MenuItem value={'03'}>Diploma</MenuItem>
                                            <MenuItem value={'04'}>S-1</MenuItem>
                                            <MenuItem value={'05'}>S-2</MenuItem>
                                            <MenuItem value={'06'}>S-3</MenuItem>
                                            <MenuItem value={'07'}>Lainnya</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Jenis Kelamin"
                                        required
                                        fullWidth
                                        value={this.props.pbypmhn.pemohon.jklm}
                                        name="jklm"
                                        onChange={handleChangeP}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'Laki-laki'}>Laki-laki</MenuItem>
                                            <MenuItem value={'Perempuan'}>Perempuan</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Status Perkawinan"
                                        required
                                        fullWidth
                                        value ={this.props.pbypmhn.pemohon.stskwn}
                                        name="stskwn"
                                        onChange={handleChangeP}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'20'}>Lajang</MenuItem>
                                            <MenuItem value={'10'}>Menikah</MenuItem>
                                            <MenuItem value={'50'}>Menikah (Pisah Harta)</MenuItem>
                                            <MenuItem value={'30'}>Cerai Hidup</MenuItem>
                                            <MenuItem value={'40'}>Cerai Mati</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField disabled
                                        label="Kewarganegaraan"
                                        //required
                                        fullWidth
                                        value ={this.props.pbypmhn.pemohon.wngr}
                                        name="wngr"
                                        onChange={handleChangeP}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'ID'}>WNI</MenuItem>
                                            <MenuItem value={'WNA'}>WNA</MenuItem>
                                    </TextField> 
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group>
                                    <TextField disabled
                                        label="Status Kependudukan"
                                        //required
                                        fullWidth
                                        value ={this.props.pbypmhn.pemohon.stspenduk}
                                        name="stspenduk"
                                        onChange={handleChangeP}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={'Penduduk'}>Penduduk</MenuItem>
                                    </TextField> 
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField disabled
                                        label="Agama"
                                        //required
                                        fullWidth
                                        value ={this.props.pbypmhn.pemohon.din}
                                        name="din"
                                        onChange={handleChangeP}
                                        variant="standard"
                                        size="small"
                                        select>
                                            <MenuItem value={1}>ISLAM</MenuItem>
                                    </TextField> 
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                    label="Nama Gadis Ibu Kandung"
                                    required
                                    fullWidth
                                    value ={this.props.pbypmhn.pemohon.ibukdg}
                                    name="ibukdg"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    helperText={containword}
                                    error={erroribu}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='submit' 
                                            id='continue'
                                            disabled={erroribu || checkNoTlp()}
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformPemohon3

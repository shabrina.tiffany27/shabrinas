import * as React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faExclamationTriangle, faTimes } from "@fortawesome/free-solid-svg-icons";
import { solid, regular, brands } from '@fortawesome/free-solid-svg-icons';


function stepIcon(status, stepNumber) {
  if (status == "incomplete") {
    return stepNumber
  } else if (status == "approve") {
    return <FontAwesomeIcon size="md" icon={faCheck} />
  } else if (status == "sendBack") {
    return <FontAwesomeIcon icon={faExclamationTriangle} />
  } else if (status == "reject" || status == "cancel") {
    return <FontAwesomeIcon icon={faTimes} />
  }
}

function stepActivity(step) {
  if (step.activity == "active") {
    if (step.status == "approve") {
      return wrapperStepperActiveApprove
    } else if (step.status == "sendBack") {
      return wrapperStepperActiveSendBack
    } else if (step.status == "reject" || step.status == "cancel") {
      return wrapperStepperActiveReject
    }
  } else {
    return wrapperStepperInactive
  }
}

function stepActivity1(step) {
  if (step.activity == "active") {
    if (step.status == "approve") {
      return wrapperStepperActiveApprove1
    } else if (step.status == "sendBack") {
      return wrapperStepperActiveSendBack1
    } else if (step.status == "reject" || step.status == "cancel") {
      return wrapperStepperActiveReject1
    }
  } else {
    return wrapperStepperInactive1
  }
}

function stepStatus(status) {
  if (status == "incomplete") {
    return stepperBasic
  } else if (status == "approve") {
    return stepperApprove
  } else if (status == "sendBack") {
    return stepperSendBack
  } else if (status == "reject" || status == "cancel") {
    return stepperReject
  }
}

function stepLabelStat(status) {
  if (status == "incomplete") {
    return labelBasic
  } else if (status == "approve") {
    return labelApprove
  } else if (status == "sendBack") {
    return labelSendBack
  } else if (status == "reject" || status == "cancel") {
    return labelReject
  }
}

function stepLabelStat1(status) {
  if (status == "incomplete") {
    return labelBasic1
  } else if (status == "approve") {
    return labelApprove1
  } else if (status == "sendBack") {
    return labelSendBack1
  } else if (status == "reject" || status == "cancel") {
    return labelReject1
  }
}

function connectorStatus(status) {
  if (status == "incomplete") {
    return connectorBasic
  } else if (status == "approve") {
    return connectorApprove
  } else if (status == "sendBack") {
    return connectorSendBack
  } else if (status == "reject" || status == "cancel") {
    return connectorReject
  }
}

function connectorStatus1(status) {
  if (status == "incomplete") {
    return connectorBasic1
  } else if (status == "approve") {
    return connectorApprove1
  } else if (status == "sendBack") {
    return connectorSendBack1
  } else if (status == "reject" || status == "cancel") {
    return connectorReject1
  }
}

let indeks = 0

function isShow(index) {
  if (index == 0) {
    return false;
  } else {
    return true;
  }
}

function showIndex(index) {
  return indeks = index
}

let connectorBasic = {
  width: "50px",
  margin: "24px -16px 0px -16px",
  backgroundColor: "#595862",
  height: "2px"
};

let connectorBasic1 = {
  width: "2px",
  margin: "0px 23px 0px 23px",
  backgroundColor: "#595862",
  height: "20px"
}

let connectorApprove = {
  width: "50px",
  margin: "24px -16px 0px -16px",
  backgroundColor: "#28A745",
  height: "2px"
}

let connectorApprove1 = {
  width: "2px",
  margin: "0px 23px 0px 23px",
  backgroundColor: "#28A745",
  height: "20px"
}

let connectorSendBack = {
  width: "50px",
  margin: "24px -16px 0px -16px",
  backgroundColor: "#df8020",
  height: "2px"
}

let connectorSendBack1 = {
  width: "2px",
  margin: "0px 23px 0px 23px",
  backgroundColor: "#df8020",
  height: "20px"
}

let connectorReject = {
  width: "50px",
  margin: "24px -16px 0px -16px",
  backgroundColor: "#b21919",
  height: "2px"
}

let connectorReject1 = {
  width: "2px",
  margin: "0px 23px 0px 23px",
  backgroundColor: "#b21919",
  height: "20px"
}

let stepperBasic = {
  backgroundColor: "#CAC8CE",
  borderRadius: "50%",
  width: "38px",
  height: "38px",
  color: "#444",
  alignItems: "center",
  justifyContent: "center",
  display: "flex",
  margin: "0px 0px 0px 0px"
};

let stepperApprove = {
  backgroundColor: "#28A745",
  borderRadius: "50%",
  width: "38px",
  height: "38px",
  color: "#fff",
  alignItems: "center",
  justifyContent: "center",
  display: "flex",
  margin: "0px 0px 0px 0px"
}

let stepperSendBack = {
  backgroundColor: "#df8020",
  borderRadius: "50%",
  width: "38px",
  height: "38px",
  color: "#fff",
  alignItems: "center",
  justifyContent: "center",
  display: "flex",
  margin: "0px 0px 0px 0px"
}

let stepperReject = {
  backgroundColor: "#b21919",
  borderRadius: "50%",
  width: "38px",
  height: "38px",
  color: "#fff",
  alignItems: "center",
  justifyContent: "center",
  display: "flex",
  margin: "0px 0px 0px 0px"
}

let labelBasic = {
  fontSize: "14px",
  fontFamily: "Roboto"
}

let labelBasic1 = {
  fontSize: "16px",
  fontFamily: "Roboto",
  marginLeft: "8px",
  marginTop: '8px'
}

let labelApprove = {
  color: "#28A745",
  fontSize: "14px",
  fontFamily: "Roboto"
}

let labelApprove1 = {
  color: "#28A745",
  fontSize: "16px",
  fontFamily: "Roboto",
  marginLeft: '8px',
  marginTop: '8px'
}

let labelSendBack = {
  color: "#df8020",
  fontSize: "14px",
  fontFamily: "Roboto"
}

let labelSendBack1 = {
  color: "#df8020",
  fontSize: "16px",
  fontFamily: "Roboto",
  marginLeft: '8px',
  marginTop: '8px'
}

let labelReject = {
  color: "#b21919",
  fontSize: "14px",
  fontFamily: "Roboto"
}

let labelReject1 = {
  color: "#b21919",
  fontSize: "16px",
  fontFamily: "Roboto",
  marginLeft: '8px',
  marginTop: '8px'
}

let wrapperStepperBasic = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  width: "80px"
}

let wrapperStepperBasic1 = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "60vw"
}

let wrapperStepperActiveApprove = {
  borderRadius: "50%",
  width: "50px",
  height: "50px",
  border: "2px solid #28A745",
  padding: "4px",
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
  marginBottom: "10px"
}

let wrapperStepperActiveApprove1 = {
  borderRadius: "50%",
  width: "50px",
  height: "50px",
  border: "2px solid #28A745",
  padding: "4px",
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
  marginBottom: "5px",
  marginTop: "5px",
}

let wrapperStepperActiveSendBack = {
  borderRadius: "50%",
  width: "50px",
  height: "50px",
  border: "2px solid #df8020",
  padding: "4px",
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
  marginBottom: "10px"
}

let wrapperStepperActiveSendBack1 = {
  borderRadius: "50%",
  width: "50px",
  height: "50px",
  border: "2px solid #df8020",
  padding: "4px",
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
  marginBottom: "5px",
  marginTop: "5px"
}

let wrapperStepperActiveReject = {
  borderRadius: "50%",
  width: "50px",
  height: "50px",
  border: "2px solid #b21919",
  padding: "4px",
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
  marginBottom: "10px"
}

let wrapperStepperActiveReject1 = {
  borderRadius: "50%",
  width: "50px",
  height: "50px",
  border: "2px solid #b21919",
  padding: "4px",
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
  marginTop: "0px",
  marginBottom: '0px'
}

let wrapperStepperInactive = {
  borderRadius: "50%",
  width: "50px",
  height: "50px",
  border: "2px solid #ffffff00",
  padding: "4px",
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
  marginBottom: "10px"
}

let wrapperStepperInactive1 = {
  borderRadius: "50%",
  width: "50px",
  height: "50px",
  padding: "0px",
  justifyContent: "center",
  alignItems: "center",
  display: "flex",
  marginBottom: "0px"
}

export { indeks, stepIcon, stepActivity, stepActivity1, stepStatus, stepLabelStat, stepLabelStat1, connectorStatus, connectorStatus1, isShow, showIndex, wrapperStepperBasic, wrapperStepperBasic1 }
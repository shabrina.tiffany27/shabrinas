import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {TextField} from '@material-ui/core/';
import ModalConfirm from './ModalConfirm';
//import { ToastContainer } from 'react-toastify';

export class EformDaftarPorsi extends Component {
    continue = e => {
        e.preventDefault();
        //if (this.props.mfac.appid === ''){
            this.props.handleSaveTemp();
        //}
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.onTop();  
    }
    
    render() {
        //const numOnly = /^[0-9\b]+$/;
        const { handleChangeFC, handleSaveTemp } = this.props

        return (
            <div>
                <Header prod = {this.props.mfac.kd_produk} eprod = {this.props.mfac.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.mfac.batal}/>
                <Form onSubmit={this.continue} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'80%',textAlign:'left'}}><h5>Pembukaan Rekening Pembiayaan {'&'} Pendaftaran Porsi Haji</h5></td>
                            <td style={{width:'20%',textAlign:'right'}}>Page 4</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                                <Form.Group>
                                <TextField
                                    label="Jenis Produk"
                                    disabled
                                    fullWidth
                                    value={this.props.mfac.funding.jnsproduk}
                                    name="jnsproduk"
                                    onChange={handleChangeFC}
                                    variant="standard"
                                    size="small"
                                    //helperText={}
                                />
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                <TextField
                                    label="Type Rekening"
                                    disabled
                                    fullWidth
                                    value={this.props.mfac.funding.typeacc}
                                    name="typeacc"
                                    onChange={handleChangeFC}
                                    variant="standard"
                                    size="small"
                                    //helperText={}
                                />
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                <TextField
                                    label="Mata Uang"
                                    disabled
                                    fullWidth
                                    value={this.props.mfac.funding.currency}
                                    name="currency"
                                    onChange={handleChangeFC}
                                    variant="standard"
                                    size="small"
                                    //helperText={}
                                />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group>
                                <TextField
                                    label="Sumber Dana"
                                    disabled
                                    fullWidth
                                    value={this.props.mfac.funding.sumberdana}
                                    name="sumberdana"
                                    onChange={handleChangeFC}
                                    variant="standard"
                                    size="small"
                                    //helperText={}
                                />
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                <TextField
                                    label="Tujuan Pembukaan Rekening"
                                    disabled
                                    fullWidth
                                    value={this.props.mfac.funding.tujuanacc}
                                    name="tujuanacc"
                                    onChange={handleChangeFC}
                                    variant="standard"
                                    size="small"
                                    //helperText={}
                                />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='submit' 
                                            id='continue'
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformDaftarPorsi

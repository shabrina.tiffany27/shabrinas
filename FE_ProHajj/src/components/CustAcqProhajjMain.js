import React, { Component } from 'react'
import { toast } from 'react-toastify';
import Loader from './Loader'
import SuccessAkad from './SuccessAkad';
//import SaveSuccess from './SaveSuccess';
import ProHajjServices from '../services/ProHajjServices';
import EformFlayer from './EformFlayer';
import EformPembiayaan from './EformPembiayaan';
import EformPemohon1 from './EformPemohon1';
import EformPemohon2 from './EformPemohon2';
import EformPemohon3 from './EformPemohon3';
import EformPekerjaan from './EformPekerjaan';
import EformKerabat from './EformKerabat';
import EformCrsFatca from './EformCrsFatca';
import EformDaftarPorsi from './EformDaftarPorsi';
import EformUploadDoc from './EformUploadDoc';
import EformReview from './EformReview';
import EformAgremen from './EformAgremen';
import EformRatingSubmit from './EformRatingSubmit';
import MessageProhajj from './MessageProhajj';
import LastPage from './LastPage';
import axios from 'axios';
import StepperPengajuan from './StepperPengajuan';
import dataBankLain from '../csvjson.json'

export class CustAcqProhajjMain extends Component {
  constructor(props) {
    // this.cekStatusPenganjuan = false
    super(props);
    this.selectFile = this.selectFile.bind(this);
    this.uploadFileDoc = this.uploadFileDoc.bind(this);
    //this.emptypemohon = this.emptypemohon.bind(this);
    this.state = {
      dataStatusPengajuan: [],
      urlf: [], status: '', tglexpirerev: '',
      step: 0, stssend: '', pagedrf: null, error: '', initfile: null, batal: false, mrview: false, moacc: false, tokensub: '', tokenctr: '', validresp: null, mssge: '',
      loadspin: false, loading: false, respcode: '', respmsg: '', custflg: '', rating: '', ratingcomment: '', tglnasabah: '', kd_region: '', kd_produk: '', etypeproduk: 'D308',
      appid: '', accnumktp: '', plafond: '', tenor: '', jmlpor: '', refcode: '', refname: '', kd_channel: '', kd_program: '', salacqcode: '', salprocode: '',
      selectedFiles: undefined, currentFile: undefined, progress1: 0, progress2: 0, progress3: 0, progress4: 0, progress5: 0, message: '', kd_pendamping: '',
      ktpfile: [], urlktp: '', npwpfile: [], urlnpwp: '', gajifile: [], urlgaji: '', legalfile: [], urllegal: '', mutasifile: [], urlmutasi: '',
      custsign: '', mcsig: false, syaratpby: '', mspby: false, mspblhajj: false, syarattab: '', mstab: false, kd_subchannel: '', kd_subprogram: '',
      pemohon: {
        namalkp: '', namanoglr: '', jklm: '', tmplhr: '', tgllhr: '', nikktp: '',
        npwp: '', ibukdg: '', stskwn: '', pendidikan: '', almktp: '', ktprt: '', ktprw: '', klrhktp: '',
        kcmtktp: '', dt2ktp: '', dt1ktp: '', kdposktp: '', almnowktp: '', almnow: '', nowrt: '', nowrw: '',
        klrhnow: '', kcmtnow: '', dt2now: '', dt1now: '', kdposnow: '', din: '1', wngr: 'ID', stspenduk: 'Penduduk',
        lamathn: '', lamabln: '', stsalmnow: '', almsurat: '', notlpnowkd: '', notlpnow: '', email: '', nohp1: '', nohp2: ''
      },
      pekerjaan: {
        jnspkjfos: '', stspkj: '', namaprsh: '', ktgrinst: '', ktgrinstlain: '', bidangktr: '', jabatan: '', lmkrjathn: '', lmkrjabln: '',
        pendptn: '', pbygaji: '', pbygajikode: '', phasiltdktetap: '', pluartetap: '', phasiltambah: '', phasiltambahlain: '', almktr: '',
        klrhktr: '', kcmtnktr: '', dt2ktr: '', dt1ktr: '', kdposktr: '', tlpktrkd: '', tlpktr: '', extktr: '', tlphrdkd: '', tlphrdktr: '', exthrd: ''
      },
      kerabat: { namaktpf: '', nohpf: '', hubnsbf: '', notlpf: '' },
      funding: {
        fcrgcard: '', fcrgcardcountry: '', fctax: '', fctaxcountry: '', fctaxid: '', fctrsletter: '',
        jnsproduk: 'Tabungan RTJU', typeacc: 'Single', currency: 'IDR', tujuanacc: 'Pendaftaran Porsi Haji',
        sumberdana: ''
      },
      prov: [], kab: [], kec: [], kel: [], nkab: [], nkec: [], nkel: [], kabk: [], keck: [], kelk: [], bdgush: [],
      urlimgdoc: ''
      //urlimgdoc : 'https://apigw-ext.bankmuamalat.co.id/eformntb' 
      //urlimgdoc : 'http://localhost:19130/eformntb'
    };
  }

  nextStep = (e) => {
    const { step } = this.state;
    this.setState({
      step: step + e
    });
  }
  prevStep = (e) => {
    const { step } = this.state;
    this.setState({
      step: step - e
      //custsign: ''
    });
  }
  onTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  }
  upperNoNumbr(e) {
    var sRet = ''
    if (e !== null) {
      sRet = e.replace(/[^\w\s.,']/gi, "").replace(/[0-9]/gi, '').toUpperCase();
    }
    return sRet;
  }
  radioChange = () => {
    //var ckdReky = document.getElementById("yarekno");
    var ckdRekt = document.getElementById("norekno");
    /*  if (ckdReky.checked){
         document.getElementById("norekktp").disabled = false; 
     }else{
         document.getElementById("norekktp").disabled = true;
         document.getElementById("norekktp").value = '';        
     }*/
    if (ckdRekt.checked) {
      //document.getElementById("appid").value = '';
      this.setState({ accnumktp: '' });
    }
  };
  cekIbukandung(e) {
    let string = e.toUpperCase();
    let special = ['IBU', 'MAMA', 'IBUNDA', 'UMI', 'MAMI'];
    let string_array = string.split(" ");
    //let fault = false;

    for (let i = 0; i < string_array.length; i++) {
      if (special.indexOf(string_array[i]) >= 0) {
        if (special.indexOf(string_array[0]) >= 0) {
          string = string.replace(string_array[i] + " ", string_array[i] + '.');
        } else {
          string = string.replace(" " + string_array[i], '.' + string_array[i]);
        }
      }
    }
    //console.log('string_array.length '+string_array.length);

    return string;
  }
  handleChangeP = (e) => {
    // /[^0-9\.,]/g ----- /^[0-9\b]+$/
    const re = /^[0-9\b]+$/;
    const { pemohon } = { ...this.state };
    const currentState = pemohon;
    const { name, value } = e.target;
    if (name === 'namalkp' || name === 'namanoglr') {
      if (value === '' || e.target.value.length < 101) {
        currentState[name] = this.upperNoNumbr(value);
      }
    } else if (name === 'tmplhr') {
      if (value === '' || e.target.value.length < 51) {
        currentState[name] = this.upperNoNumbr(value);
      }
    }
    else if (name === 'ibukdg') {
      currentState[name] = this.cekIbukandung(this.upperNoNumbr(value));
    } else if (name === 'nohp1' || name === 'nohp2' || name === 'notlpnow'
      || name === 'notlpnow') {
      if ((value === '' || re.test(value)) && e.target.value.length < 15) {
        currentState[name] = value;
      }
    } else if (name === 'kdposktp' || name === 'kdposnow') {
      if ((value === '' || re.test(value)) && e.target.value.length < 10) {
        currentState[name] = value;
      }
    } else if (name === 'ktprt' || name === 'ktprw' || name === 'nowrt' || name === 'nowrw') {
      if ((value === '' || re.test(value)) && e.target.value.length < 4) {
        currentState[name] = value;
      }
    } else if (name === 'lamathn' || name === 'lamabln') {
      if ((value === '' || re.test(value)) && e.target.value.length < 3) {
        currentState[name] = value;
      }
    } else if (name === 'nikktp') {
      if ((value === '' || re.test(value)) && e.target.value.length < 17) {
        currentState[name] = value;
      }
      //  if (e.target.value.length === 16) {this.checkEktp();} //Check KTP di aplicant_person
    } else if (name === 'npwp') {
      if ((value === '' || re.test(value)) && e.target.value.length < 16) {
        currentState[name] = value;
      }
    } else if (name === 'tgllhr') {
      currentState[name] = value;
    } else if (name === 'email') {
      currentState[name] = value.replace(/[^\w\s.@-_]/gi, "");
    } else if (name === 'almktp' || name === 'almnow') {
      currentState[name] = value.replace(/[^\w\s.,-]/gi, "").toUpperCase();
    } else if (name === 'din' || name === 'stsalmnow' || name === 'stspenduk' || name === 'almsurat' || name === "stskwn" || name === 'pendidikan') {
      currentState[name] = value.replace(/[^\w\s.,-]/gi, "")
    } else {
      currentState[name] = value;//.replace(/[^\w\s.)/(,-/]/gi, "");
      /*if (name === 'almnowktp'){
         if (e.target.value === 'sda') {this.emptypemohon();}
      }*/

      if (name === 'dt1ktp') {
        this.getWilayah('dati2', 'pp');
      } else if (name === 'dt2ktp') {
        this.getWilayah('kec', 'pp');
      } else if (name === 'kcmtktp') {
        this.getWilayah('kel', 'pp');
      } else if (name === 'klrhktp') {
        currentState[name] = value;
        this.loadKodepos('kdposktp', this.state.pemohon.dt2ktp + "--" + this.state.pemohon.kcmtktp + "--" + value);
        pemohon.kdposktp = this.state.pemohon.kdposktp;
      } else

        if (name === 'dt1now') {
          this.getWilayah('dati2', 'pn');
        } else if (name === 'dt2now') {
          this.getWilayah('kec', 'pn');
        } else if (name === 'kcmtnow') {
          this.getWilayah('kel', 'pn');
        } else if (name === 'klrhnow') {
          currentState[name] = value;
          this.loadKodepos('kdposnow', this.state.pemohon.dt2now + "--" + this.state.pemohon.kcmtnow + "--" + value);
          pemohon.kdposnow = this.state.pemohon.kdposnow;
          /* if (value.lastIndexOf('-') > 0 ){
             currentState[name] = value.substr(0,value.lastIndexOf('-'));
             pemohon.kdposktp = value.substr(value.lastIndexOf('-')+1,value.length);
             
             currentState[name] = value;
             this.loadKodepos("kdposktp",this.state.pemohon.kcmtktp + "--"+ value);
             pemohon.kdposktp = this.state.pemohon.kdposktp;
             */
        } else { currentState[name] = value }
    }
    this.setState({ pemohon: currentState });
  }
  fsumberdana = (e) => {
    let sRet = '';
    if (e === '1' || e === '3') {
      sRet = 'Gaji/Tabungan';
    } else if (e === '2' || e === '4') {
      sRet = 'Hasil Usaha';
    }

    //this.setState({pekerjaan : {...this.state.pekerjaan, stspkj: ""}});
    this.setState({
      funding: { ...this.state.funding, sumberdana: sRet }
    });
    //return sRet;
  }

  // pbygajilain = (e) => {
    // let tempDataBank = dataBankLain.find(o => o.BANK_CODE === this.state.pekerjaan.pbygajikode).BANK_DESC;
  //   let pbylain = ''
  //   if (e === 'Bank Lain') {
  //     pbylain = dataBankLain.find(o => o.BANK_CODE === this.state.pekerjaan.pbygajikode).BANK_DESC;
  //   } else if (e === 'Bank Muamalat') {
  //     pbylain = 'Bank Muamalat';
  //   } else if (e === 'Tunai') {
  //     pbylain = 'Tunai';
  //   }

    //this.setState({pekerjaan : {...this.state.pekerjaan, stspkj: ""}});
    // this.setState({
    //   pekerjaan: { ...this.state.pekerjaan, pbygaji: pbylain }
    // });
  //   return pbylain;
  // }
  emptypemohon = () => {
    this.setState({ pemohon: { ...this.state.pemohon, almnow: '', dt1now: '', dt2now: '', kcmtnow: '', klrhnow: '', nowrt: '', nowrw: '', kdposnow: '' } });
  }

  handleChangeK = (e) => {
    const re = /^[0-9\b]+$/;
    const { pekerjaan } = { ...this.state };
    const currentState = pekerjaan;
    const { name, value } = e.target;
    if (name === 'namaprsh') {
      //currentState[name] = this.upperNoNumbr(value);
      currentState[name] = value.replace(/[^\w\s.,']/gi, "").toUpperCase();

    } else if (name === 'rtktr' || name === 'rwktr') {
      if ((value === '' || re.test(value)) && e.target.value.length < 4) {
        currentState[name] = value;
      }
    } else if (name === 'lmkrjathn' || name === 'lmkrjabln') {
      if ((value === '' || re.test(value)) && e.target.value.length < 3) {
        currentState[name] = value;
      }
    } else if (name === 'extktr' || name === 'exthrd') {
      if ((value === '' || re.test(value)) && e.target.value.length < 10) {
        currentState[name] = value;
      }
    } else if (name === 'kdposktr') {
      if ((value === '' || re.test(value)) && e.target.value.length < 10) {
        currentState[name] = value;
      }
    } else if (name === 'almktr') {
      currentState[name] = value.replace(/[^\w\s.,]/gi, "").toUpperCase();
    } else if (name === 'jnspkjfos' || name === 'stspkj' || name === 'ktgrinst' || name === 'ktgrinstlain'
      || name === 'bidangktr' || name === 'phasiltambah' || name === 'pbygaji' || name === 'pbygajikode') {
      currentState[name] = value.replace(/[^\w\s.,]/gi, "");
      if (name === 'jnspkjfos') {
        this.fsumberdana(value);
      }
    }
    else if (name === 'pendptn' || name === 'pluartetap') {
      currentState[name] = value.replace(/[^\w,]/gi, "").replace(/[a-z]/gi, '');
    } else if (name === 'phasiltambahlain') {
      currentState[name] = value.replace(/[^\w\s']/gi, "").replace(/[0-9]/gi, '').toUpperCase();
    } else if ( name === 'tlpktr' || name === 'tlphrdktr'){
      if ((value === '' || re.test(value)) && e.target.value.length < 11) {
        currentState[name] = value;
      }
    } else {
      currentState[name] = value;//.replace(/[^\w\s.)/(,-/]/gi, "")
      if (name === 'dt1ktr') {
        this.getWilayah('dati2', 'kt');
      } else if (name === 'dt2ktr') {
        this.getWilayah('kec', 'kt');
      } else if (name === 'kcmtnktr') {
        this.getWilayah('kel', 'kt');
      } else if (name === 'klrhktr') {
        currentState[name] = value;
        this.loadKodepos('kdposktr', this.state.pekerjaan.dt2ktr + "--" + this.state.pekerjaan.kcmtnktr + "--" + value);
        pekerjaan.kdposktr = this.state.pekerjaan.kdposktr;
      } else { currentState[name] = value }
    }
    this.setState({ pekerjaan: currentState });

  }
  checkEktp() {
    this.setState({ loading: true });
    const eNikKtp = this.state.pemohon.nikktp;
    //console.log('token',token);
    ProHajjServices.getDataExistKTP(eNikKtp)
      .then(result => {
        //console.log('Response : '+JSON.stringify(result));
        this.setState({
          data: result.data,
          appid: (result.data.rsBody === null ? '' : result.data.rsBody.app_id),
          respmsg: result.data.rsMsg,
          respcode: result.data.msgId,
          //refcode: (result.data.rsBody === null?'':result.data.rsBody.refcode),
          //refname: (result.data.rsBody === null?'':result.data.rsBody.refname),
          loading: false,
          error: false,
          pemohon: { ...this.state.pemohon, nikktp: (result.data.rsBody === null ? this.state.pemohon.nikktp : '') },
          message: (result.data.rsBody === null ? '' : 'Nik KTP ' + result.data.msgId + ' already exists')
        });
        if (result.data.rsBody !== null) { toast.error('Nik KTP ' + this.state.respcode + ' sudah pengajuan') }
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          respmsg: `${error}`,
          loading: false,
        });
        toast.error(`${error}`)
      });
  }

  componentDidMount() {
    this.handleParsing();
    this.handleViewDoc();
  }
  handleViewDoc() {
    this.setState({
      urlimgdoc: ProHajjServices.getUrlview()
    });
  }
  handleParsing() {
    this.setState({ loading: true });
    let pth = window.location.search;
    let urlp = pth.substring(pth.lastIndexOf("?") + 1)
    //const urlp = this.props.match.params.id;
    // let pth = window.location.pathname;
    //let urlp = pth.substring(pth.lastIndexOf("/")+1)
    //console.log('token',token);
    if (urlp.indexOf('_') <= 0) { this.loadDataRevisi(urlp) }
    else {
      ProHajjServices.getEformParsing(urlp)
        .then(result => {
          //console.log('Response : '+JSON.stringify(result));
          if ((result.data[0] !== 'D306') && (result.data[0] !== 'D307') && (result.data[0] !== 'D308')) {
            this.setState({
              loading: false,
              error: false,
            });
            //toast.warning(`Incorrect URL(Link)...!!`)
          } else if (result.data[5].length !== 4) {
            this.setState({
              loading: false,
              error: false,
            });
          } else {
            this.setState({
              kd_produk: result.data[0],
              kd_channel: result.data[1],
              kd_subchannel: result.data[2],
              kd_program: result.data[3],
              kd_subprogram: result.data[4],
              kd_region: result.data[5],
              refcode: result.data[6],
              salacqcode: result.data[7],
              kd_pendamping: result.data[8],

              //refcode: (result.data.rsBody === null?'':result.data.rsBody.refcode),
              //refname: (result.data.rsBody === null?'':result.data.rsBody.refname),
              loading: false,
              error: false,
            });
          }
        })
        .catch(error => {
          //console.error("error: ", error);
          this.setState({
            respmsg: `${error}`,
            loading: false,
          });
          toast.warning(`Incorrect URL(Link)...!!`)
        });
    }
  }

  handleChangeF = (e) => {
    const re = /^[0-9\b]+$/;
    const { kerabat } = { ...this.state };
    const currentState = kerabat;
    const { name, value } = e.target;
    if (name === 'namaktpf') {
      currentState[name] = this.upperNoNumbr(value);
    } else if (name === 'notlpf') {
      if ((value === '' || re.test(value)) && e.target.value.length < 19) {
        currentState[name] = value;
      }
    } else if (name === 'nohpf') {
      if ((value === '' || re.test(value)) && e.target.value.length < 17) {
        currentState[name] = value;
      }
    } else if (name === 'hubnsbf') {
      currentState[name] = value.replace(/[^\w\s.,]/gi, "")
    } else {
      currentState[name] = value.replace(/[^\w\s.,]/gi, "").replace(/[0-9]/gi, '');
    }
    this.setState({ kerabat: currentState });
  }

  handleChangeFC = (e) => {
    const re = /^[0-9\b]+$/;
    const { funding } = { ...this.state };
    const currentState = funding;
    const { name, value } = e.target;
    //currentState[name] = value ;
    if (name === 'fcrgcardcountry') {
      currentState[name] = value.replace(/[^\w\s]/gi, "").replace(/[0-9]/gi, '').toUpperCase();
    } else if (name === 'fctaxid') {
      if ((value === '' || re.test(value)) && e.target.value.length < 21) {
        currentState[name] = value;
      }
    } else {
      currentState[name] = value
    }
    this.setState({ funding: currentState });
  }

  handleChange = input => e => {
    if (e.target.name === "ratingcomment") {
      this.setState({ [input]: e.target.value.replace(/[^\w\s.,]/gi, "") });
    } else {
      if (e.target.name === "appid") {
        this.setState({ [input]: e.target.value })
        if (parseInt(e.target.value.replace(/[^\w,]/gi, "").length) === 13) {
          this.loadDataTemp(e.target.value);
          //this.loadDataRevisi(e.target.value);
        }
      }
      this.setState({ [input]: e.target.value })
    }
  };

  handleModalAcc = () => {
    this.setState({ moacc: !this.state.moacc });
  };
  handleModalbtl = () => {
    this.setState({ batal: !this.state.batal/*, custsign: ''*/ });
  };
  handleModalrview = () => {
    this.setState({ mrview: !this.state.mrview });
  };
  handleModalpnyata = () => {
    this.setState({ mcsig: !this.state.mcsig });
  };
  handleModalagree = () => {
    this.setState({ mspby: !this.state.mspby });
  };
  handleModalalhajj = () => {
    this.setState({ mspblhajj: !this.state.mspblhajj });
  };
  handleModalsytab = () => {
    this.setState({ mstab: !this.state.mstab });
  };
  handleChangeCkBox = input => e => {
    let val = ''
    if (e.target.checked) {
      val = e.target.value
      if (e.target.name === 'custsign') { this.handleModalpnyata() }
      if (e.target.name === 'syaratpby') {
        if (this.state.kd_produk === this.state.etypeproduk) {
          this.handleModalalhajj()
        } else {
          this.handleModalagree()
        }
      }
      if (e.target.name === 'syarattab') { this.handleModalsytab() }
    }
    this.setState({ [input]: val })
  }

  emailValidation = email => {
    if (
      /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        email,
      )
    ) {
      return null;
    }
    // if (email.trim() === '') {
    //   return 'Email is required';
    // }
    return 'Harap masukkan email yang benar';
  };

  handleNumber = (e) => {
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
      this.setState({ [e.target.name]: e.target.value })
    }
  }

  handleChangeN = input => e => {
    const re = /^[0-9\b]+$/;
    const rep = /^[1-4\b]+$/;
    const val = e.target.value.replace(/[^\w,]/gi, "").replace(/[a-z]/gi, '')
    if (e.target.name !== "jmlpor") {
      if (val === '' || re.test(val)) {
        this.setState({ [input]: val });
      }
      //if (e.target.name !== "plafond"){

      //}
    } else if ((e.target.value === '' || rep.test(e.target.value)) && e.target.value.length < 2) {
      let max = e.target.value * 25000000

      this.setState({
        [input]: e.target.value,
        plafond: '' + max,
        pemohon: { ...this.state.pemohon, npwp: (max <= 50000000 ? '' : this.state.pemohon.npwp) }
      });
    }
  }

  handleChangeUN = input => e => {
    //const { name, value } = e.target;
    if (e.target.value.length < 10) {
      this.setState({ [input]: e.target.value.replace(/[^\w\s]/gi, "").toUpperCase() });
    }
    if (e.target.value.length === 8 || e.target.value.length === 9) {
      //console.log("referal 2 : "+e.target.value)
      this.srcRefCode(e.target.value);
    }
  }

  handleChangeUktp = (e) => {
    //var files = e.target.files
    if (this.checkMimeType(e) && this.checkFileSize(e)) {
      this.setState({
        ktpfile: e.target.files[0],
        selectedFiles: e.target.files,
        loaded: 0,
        initfile: 1,
        urlktp: (e.target.files[0] ? URL.createObjectURL(e.target.files[0]) : '')
      })
    }
  }

  handleChangeUnpwp = (e) => {
    //var files = e.target.files
    if (this.checkMimeType(e) && this.checkFileSize(e)) {
      this.setState({
        npwpfile: e.target.files[0],
        selectedFiles: e.target.files,
        loaded: 0,
        initfile: 2,
        urlnpwp: (e.target.files[0] ? URL.createObjectURL(e.target.files[0]) : '')
      })
    }
  }

  handleChangeGaji = (e) => {
    //var files = e.target.files
    if (this.checkMimeTypePDF(e) && this.checkFileSize(e)) {
      this.setState({
        gajifile: e.target.files[0],
        selectedFiles: e.target.files,
        loaded: 0,
        initfile: 3,
        urlgaji: (e.target.files[0] ? e.target.files[0].name : '')
      })
    }
  }

  handleChangeLegal = (e) => {
    //var files = e.target.files
    if (this.checkMimeTypePDF(e) && this.checkFileSize(e)) {
      this.setState({
        legalfile: e.target.files[0],
        selectedFiles: e.target.files,
        loaded: 0,
        initfile: 4,
        urllegal: (e.target.files[0] ? e.target.files[0].name : '')
      })
    }
  }

  handleChangeMutasi = (e) => {
    //var files = e.target.files
    if (this.checkMimeTypePDF(e) && this.checkFileSize(e)) {
      this.setState({
        mutasifile: e.target.files[0],
        selectedFiles: e.target.files,
        loaded: 0,
        initfile: 5,
        urlmutasi: (e.target.files[0] ? e.target.files[0].name : '')
      })
    }
  }
  selectFile(e) {
    this.setState({
      selectedFiles: e.target.files,
    });
  }

  uploadFileDoc() {
    let currentFile = this.state.selectedFiles[0];
    let appid = this.state.initfile + '-' + this.state.appid;
    let file = appid.substring(0, appid.indexOf('-'));
    //console.log('file : '+file);
    //console.log("REQUEST : "+JSON.stringify({currentFile})+", Appid : "+appid);
    this.setState({
      progress1: 0,
      progress2: 0,
      progress3: 0,
      progress4: 0,
      progress5: 0,
      currentFile: currentFile
    });

    ProHajjServices.upload(currentFile, appid, (event) => {
      let prsProg = Math.round((100 * event.loaded) / event.total)
      this.setState({
        progress1: file === '1' ? prsProg : 0,
        progress2: file === '2' ? prsProg : 0,
        progress3: file === '3' ? prsProg : 0,
        progress4: file === '4' ? prsProg : 0,
        progress5: file === '5' ? prsProg : 0
      });
    })
      .then((result) => {
        //console.log("RESPONSE : "+JSON.stringify({result}));
        this.setState({
          message: result.data.rsMsg,
        });
        toast.success(this.state.message)
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          progress1: 0,
          progress2: 0,
          progress3: 0,
          progress4: 0,
          progress5: 0,
          message: "Tidak dapat mengupload file!",
          currentFile: undefined,
        });
        toast.error(this.state.message)
      });

    this.setState({
      selectedFiles: undefined,
    });
  }

  handleChangeRsltP(input) {
    let sRet = '';
    if (this.state.data !== null) {
      sRet = input;
    }

    return sRet;
  }

  checkFileSize = (e) => {
    let files = e.target.files;
    // 20 Megabytes = 20971520 Bytes // 10 Megabytes = 10485760 Bytes // 5 Megabytes = 5242880 Bytes
    let size = 5242880;
    let err = [];
    for (var x = 0; x < files.length; x++) {
      if (files[x].size > size) {
        err[x] = files[x].type + ' Maximum 5Mb, harap pilih file yang lebih kecil\n';
      }
    };
    for (var z = 0; z < err.length; z++) {
      toast.error(err[z])
      e.target.value = null
      console.log(err)
      return false;
    }
    /*if (err !== '') {
      e.target.value = null
      console.log(err)
      alert(' ' + err);
      return false
    }*/
    return true;

    /* let files = e.target.files
     let size = 1500000 
     let err = ""; 
     for(var x = 0; x<files.length; x++) {
     if (files[x].size > size) {
     err += files[x].type+'is too large, please pick a smaller file\n';
   }
     };
     if (err !== '') {
         e.target.value = null
         console.log(err)
         alert(' ' + err);
         return false
       }
       
       return true;*/
  }

  checkMimeType = (e) => {
    let files = e.target.files
    let err = [] // create empty array
    const types = ['image/png', 'image/jpeg', 'image/gif']
    for (var x = 0; x < files.length; x++) {
      if (types.every(type => files[x].type !== type)) {
        err[x] = files[x].type + ' is not a supported format\n';
        // assign message to array
      }
    };
    for (var z = 0; z < err.length; z++) { // loop create toast massage
      e.target.value = null
      toast.error(err[z])
      console.log(err)
      return false;
    }
    /*if (err !== '') { // if message not same old that mean has error 
        e.target.value = null // discard selected file
        console.log(err)
        alert(' ' + err);
         return false; 
    }*/
    return true;

    /* //getting file object
      let files = e.target.files 
      //define message container
      let err = ''
      // list allow mime type
     const types = ['image/png', 'image/jpeg', 'image/gif']
      // loop access array
      for(var x = 0; x<files.length; x++) {
       // compare file type find doesn't matach
           if (types.every(type => files[x].type !== type)) {
           // create error message and assign to container   
           err += files[x].type+' is not a supported format\n';
         }
       };
    
     if (err !== '') { // if message not same old that mean has error 
          e.target.value = null // discard selected file
          console.log(err)
          alert(' ' + err);
           return false; 
      }
     return true;
      */

  }
  checkMimeTypePDF = (e) => {
    let files = e.target.files
    let err = [] // create empty array
    const types = ['application/pdf']
    for (var x = 0; x < files.length; x++) {
      if (types.every(type => files[x].type !== type)) {
        err[x] = files[x].type + ' is not a supported format\n';
        // assign message to array
      }
    };
    for (var z = 0; z < err.length; z++) { // loop create toast massage
      e.target.value = null
      toast.error(err[z])
      console.log(err)
      return false;
    }

    return true;
  }

  toCurrency(number) {
    const formatter = new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR"
    });

    return formatter.format(number);
  }

  telpInput = (value, previousValue) => {
    if (!value) return value;
    const currentValue = value.replace(/[^\d]/g, '');
    const cvLength = currentValue.length;

    if (!previousValue || value.length > previousValue.length) {
      if (cvLength < 4) return currentValue;
      if (cvLength < 7) return `(${currentValue.slice(0, 3)}) ${currentValue.slice(3)}`;
      return `(${currentValue.slice(0, 3)}) ${currentValue.slice(3, 6)}-${currentValue.slice(6, 10)} Ext: ${currentValue.slice(10, 15)}`;
    }
  };

  validateTelp = value => {
    let error = ""

    if (!value) error = "Required!"
    else if (value.length !== 15) error = "Invalid phone format. ex: (5555) 555-5555";

    return error;
  };
  requestDate = () => {
    var today = new Date();
    //var sRet = today.getFullYear() + '-' + this.insertnol(today.getMonth()+1)+ '-' + this.insertnol(today.getDate());
    var sRet = today.getFullYear() + '-' + this.insertnol(today.getMonth() + 1) + '-' + this.insertnol(today.getDate()) + ' ' + this.insertnol(today.getHours()) + ':' + this.insertnol(today.getMinutes()) + ':' + this.insertnol(today.getSeconds());
    //var sRet = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()+' '+today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    //return  new Date(sRet);
    return sRet;
  };
  calculate_age = (dob1) => {
    var today = new Date();
    var birthDate = new Date(dob1);
    var age_now = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age_now--;
    }
    //console.log(age_now);
    return age_now;
  }
  dateNow = (e) => {
    var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var sdate = new Date();
    var sdate1 = new Date(e);
    //var sign = this.state.custsign;
    var sRet = null
    if (e === '') {
      sRet = this.insertnol(sdate.getDate()) + " " + monthNames[sdate.getMonth()] + " " + sdate.getFullYear();
    } else {
      sRet = this.insertnol(sdate1.getDate()) + " " + monthNames[sdate1.getMonth()] + " " + sdate1.getFullYear();
    }
    return sRet;
  }
  insertnol = (e) => {
    var snol = '';
    if (e < 10) {
      snol = '0' + e;
    } else {
      snol = e;
    }
    return snol;
  }

  handleSaveTemp = (e) => {
    let tempDataBank = dataBankLain.find(o => o.BANK_CODE === this.state.pekerjaan.pbygajikode).BANK_DESC;
    const tempstatus = 'Pemilik Usaha'
    console.log('yg ini harusnya', dataBankLain.find(o => o.BANK_CODE === this.state.pekerjaan.pbygajikode),this.state.pekerjaan.pbygajikode)
    console.log('status pekerjaan: ', this.state.pekerjaan.stspkj)
    // console.log('isinya ini', this.state.pekerjaan.pbygaji)
    this.setState({ loadspin: true });
    let dataEform = { /* stssend: 'P' */
      app_id: this.state.appid, mailflag: 1, stssend: (this.state.appid === '' ? 'P' : this.state.stssend === '' ? 'S' : this.state.stssend), page: (this.state.pagedrf > this.state.step ? this.state.pagedrf : this.state.step), custflg: this.state.custflg, accnumktp: this.state.accnumktp, kd_subchannel: this.state.kd_subchannel, kd_pendamping: this.state.kd_pendamping,
      refcode: this.state.refcode, refname: this.state.refname, hprefferal: this.state.hprefferal, kd_channel: this.state.kd_channel, kd_program: this.state.kd_program, salacqcode: this.state.salacqcode, salprocode: this.state.salprocode, kd_region: this.state.kd_region, kd_produk: this.state.kd_produk,
      plafond: this.state.plafond, tenor: this.state.tenor, jmlpor: this.state.jmlpor, rating: this.state.rating, ratingcomment: this.state.ratingcomment, custsign: this.state.custsign, syaratpby: this.state.syaratpby, syarattab: this.state.syarattab, kd_subprogram: this.state.kd_subprogram,
      pemohon: {
        namalkp: this.state.pemohon.namalkp, namanoglr: this.state.pemohon.namanoglr, jklm: this.state.pemohon.jklm, tmplhr: this.state.pemohon.tmplhr, tgllhr: this.formatDate(this.state.pemohon.tgllhr), nikktp: this.state.pemohon.nikktp,
        npwp: this.state.pemohon.npwp, ibukdg: this.state.pemohon.ibukdg, stskwn: this.state.pemohon.stskwn, pendidikan: this.state.pemohon.pendidikan, almktp: this.state.pemohon.almktp, ktprt: this.state.pemohon.ktprt, ktprw: this.state.pemohon.ktprw, klrhktp: this.state.pemohon.klrhktp,
        kcmtktp: this.state.pemohon.kcmtktp, dt2ktp: this.state.pemohon.dt2ktp, dt1ktp: this.state.pemohon.dt1ktp, kdposktp: this.state.pemohon.kdposktp, almnowktp: this.state.pemohon.almnowktp, almnow: this.state.pemohon.almnow, nowrt: this.state.pemohon.nowrt, nowrw: this.state.pemohon.nowrw,
        klrhnow: this.state.pemohon.klrhnow, kcmtnow: this.state.pemohon.kcmtnow, dt2now: this.state.pemohon.dt2now, dt1now: this.state.pemohon.dt1now, kdposnow: this.state.pemohon.kdposnow, din: this.state.pemohon.din, wngr: this.state.pemohon.wngr, stspenduk: this.state.pemohon.stspenduk,
        lamathn: this.state.pemohon.lamathn, lamabln: this.state.pemohon.lamabln, stsalmnow: this.state.pemohon.stsalmnow, almsurat: this.state.pemohon.almsurat, notlpnow: this.state.pemohon.notlpnow, email: this.state.pemohon.email, nohp1: this.state.pemohon.nohp1, nohp2: this.state.pemohon.nohp2
      },
      kerabat: { namaktpf: this.state.kerabat.namaktpf, almktpf: this.state.kerabat.almktpf, notlpf: this.state.kerabat.notlpf, nohpf: this.state.kerabat.nohpf, hubnsbf: this.state.kerabat.hubnsbf },
      pekerjaan: {
        jnspkjfos: this.state.pekerjaan.jnspkjfos, stspkj: this.state.pekerjaan.jnspkjfos === '1' ? this.state.pekerjaan.stspkj : this.state.pekerjaan.jnspkjfos === '2' ? tempstatus : this.state.pekerjaan.jnspkjfos === '4' ? tempstatus : '', bidangktr: this.state.pekerjaan.bidangktr, namaprsh: this.state.pekerjaan.namaprsh, jabatan: this.state.pekerjaan.jabatan, lmkrjathn: this.state.pekerjaan.lmkrjathn, lmkrjabln: this.state.pekerjaan.lmkrjabln,
        ktgrinst: this.state.pekerjaan.ktgrinst, ktgrinstlain: this.state.pekerjaan.ktgrinstlain, pendptn: this.state.pekerjaan.pendptn, pbygaji: this.state.pekerjaan.pbygaji === 'Bank Lain' ? tempDataBank : this.state.pekerjaan.pbygaji, pbygajikode: this.state.pekerjaan.pbygajikode, almktr: this.state.pekerjaan.almktr,
        klrhktr: this.state.pekerjaan.klrhktr, kcmtnktr: this.state.pekerjaan.kcmtnktr, dt2ktr: this.state.pekerjaan.dt2ktr, dt1ktr: this.state.pekerjaan.dt1ktr, kdposktr: this.state.pekerjaan.kdposktr,
        tlpktr: this.state.pekerjaan.tlpktrkd + '-' + this.state.pekerjaan.tlpktr, extktr: this.state.pekerjaan.extktr, tlphrdktr: this.state.pekerjaan.tlphrdkd + '-' + this.state.pekerjaan.tlphrdktr, exthrd: this.state.pekerjaan.exthrd,
        phasiltdktetap: this.state.pekerjaan.phasiltdktetap, pluartetap: this.state.pekerjaan.pluartetap, phasiltambah: this.state.pekerjaan.phasiltambah, phasiltambahlain: this.state.pekerjaan.phasiltambahlain
      },
      funding: {
        fcrgcard: this.state.funding.fcrgcard, fcrgcardcountry: this.state.funding.fcrgcardcountry, fctax: this.state.funding.fctax, fctaxcountry: this.state.funding.fctaxcountry, fctaxid: this.state.funding.fctaxid, fctrsletter: this.state.funding.fctrsletter,
        jnsproduk: this.state.funding.jnsproduk, typeacc: this.state.funding.typeacc, currency: this.state.funding.currency, sumberdana: this.state.funding.sumberdana, tujuanacc: this.state.funding.tujuanacc
      },
    }
    console.log(dataEform);
    console.log('isinya ini', this.state.pekerjaan.pbygaji)
    ProHajjServices.postTempEform(dataEform)
      .then(result => {
        // console.log(result);
        let appIdUser = result.data.rsBody.app_id

        JSON.stringify({ result });
        //console.log("RESPONSE : "+JSON.stringify({result}));
        this.setState({
          //data: result.data,
          appid: (result.data.rsBody === null ? '' : result.data.rsBody.app_id),
          respmsg: result.data.rsMsg,
          respcode: result.data.msgId,
          loadspin: false,
          error: false
        });

        if (result.data.msgId !== '0') {
          //if (this.state.step === 2) {
          toast.success(this.state.respmsg)
          //this.nextStep(1);
          //}
        } else { toast.error(this.state.respmsg) }
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          respmsg: `${error}`,
          loadspin: false,
        });
        toast.error(`${error}`)
      });
  };

  handleSubmit = (e) => {
    let fixDataBank = dataBankLain.find(o => o.BANK_CODE === this.state.pekerjaan.pbygajikode).BANK_DESC;
    console.log('bidang ktr submit:',this.state.pekerjaan.pbygaji)
    this.setState({ loadspin: true });
    let dataEform = {
      app_id: this.state.appid, mailflag: 3, stssend: 'P', page: (this.state.pagedrf > this.state.step ? this.state.pagedrf : this.state.step), custflg: this.state.custflg, accnumktp: this.state.accnumktp, tglnasabah: this.requestDate(), kd_pendamping: this.state.kd_pendamping,
      refcode: this.state.refcode, refname: this.state.refname, hprefferal: this.state.hprefferal, kd_channel: this.state.kd_channel, kd_program: this.state.kd_program, salacqcode: this.state.salacqcode, salprocode: this.state.salprocode, kd_subchannel: this.state.kd_subchannel, kd_subprogram: this.state.kd_subprogram,
      plafond: this.state.plafond, tenor: this.state.tenor, jmlpor: this.state.jmlpor, rating: this.state.rating, ratingcomment: this.state.ratingcomment, custsign: this.state.custsign, syaratpby: this.state.syaratpby, syarattab: this.state.syarattab, kd_produk: this.state.kd_produk, kd_region: this.state.kd_region,
      pemohon: {
        namalkp: this.state.pemohon.namalkp, namanoglr: this.state.pemohon.namanoglr, jklm: this.state.pemohon.jklm, tmplhr: this.state.pemohon.tmplhr, tgllhr: this.formatDate(this.state.pemohon.tgllhr), nikktp: this.state.pemohon.nikktp,
        npwp: this.state.pemohon.npwp, ibukdg: this.state.pemohon.ibukdg, stskwn: this.state.pemohon.stskwn, pendidikan: this.state.pemohon.pendidikan, almktp: this.state.pemohon.almktp, ktprt: this.state.pemohon.ktprt, ktprw: this.state.pemohon.ktprw, klrhktp: this.state.pemohon.klrhktp,
        kcmtktp: this.state.pemohon.kcmtktp, dt2ktp: this.state.pemohon.dt2ktp, dt1ktp: this.state.pemohon.dt1ktp, kdposktp: this.state.pemohon.kdposktp, almnowktp: this.state.pemohon.almnowktp, almnow: this.state.pemohon.almnow, nowrt: this.state.pemohon.nowrt, nowrw: this.state.pemohon.nowrw,
        klrhnow: this.state.pemohon.klrhnow, kcmtnow: this.state.pemohon.kcmtnow, dt2now: this.state.pemohon.dt2now, dt1now: this.state.pemohon.dt1now, kdposnow: this.state.pemohon.kdposnow, din: this.state.pemohon.din, wngr: this.state.pemohon.wngr, stspenduk: this.state.pemohon.stspenduk,
        lamathn: this.state.pemohon.lamathn, lamabln: this.state.pemohon.lamabln, stsalmnow: this.state.pemohon.stsalmnow, almsurat: this.state.pemohon.almsurat, notlpnow: this.state.pemohon.notlpnow, email: this.state.pemohon.email, nohp1: this.state.pemohon.nohp1, nohp2: this.state.pemohon.nohp2
      },
      kerabat: { namaktpf: this.state.kerabat.namaktpf, almktpf: this.state.kerabat.almktpf, notlpf: this.state.kerabat.notlpf, nohpf: this.state.kerabat.nohpf, hubnsbf: this.state.kerabat.hubnsbf },
      pekerjaan: {
        jnspkjfos: this.state.pekerjaan.jnspkjfos, stspkj: (this.state.pekerjaan.jnspkjfos === '1' || this.state.pekerjaan.jnspkjfos === '') ? this.state.pekerjaan.stspkj : 'Pemilik Usaha', bidangktr: this.state.pekerjaan.bidangktr, namaprsh: this.state.pekerjaan.namaprsh, jabatan: this.state.pekerjaan.jabatan, lmkrjathn: this.state.pekerjaan.lmkrjathn, lmkrjabln: this.state.pekerjaan.lmkrjabln,
        ktgrinst: this.state.pekerjaan.ktgrinst, ktgrinstlain: this.state.pekerjaan.ktgrinstlain, pendptn: this.state.pekerjaan.pendptn, pbygaji: this.state.pekerjaan.pbygaji === 'Bank Lain' ? fixDataBank : this.state.pekerjaan.pbygaji, pbygajikode: this.state.pekerjaan.pbygajikode, almktr: this.state.pekerjaan.almktr,
        klrhktr: this.state.pekerjaan.klrhktr, kcmtnktr: this.state.pekerjaan.kcmtnktr, dt2ktr: this.state.pekerjaan.dt2ktr, dt1ktr: this.state.pekerjaan.dt1ktr, kdposktr: this.state.pekerjaan.kdposktr,
        tlpktr: this.state.pekerjaan.tlpktrkd + '-' + this.state.pekerjaan.tlpktr, extktr: this.state.pekerjaan.extktr, tlphrdktr: this.state.pekerjaan.tlphrdkd + '-' + this.state.pekerjaan.tlphrdktr, exthrd: this.state.pekerjaan.exthrd,
        phasiltdktetap: this.state.pekerjaan.phasiltdktetap, pluartetap: this.state.pekerjaan.pluartetap, phasiltambah: this.state.pekerjaan.phasiltambah, phasiltambahlain: this.state.pekerjaan.phasiltambahlain
      },
      funding: {
        fcrgcard: this.state.funding.fcrgcard, fcrgcardcountry: this.state.funding.fcrgcardcountry, fctax: this.state.funding.fctax, fctaxcountry: this.state.funding.fctaxcountry, fctaxid: this.state.funding.fctaxid, fctrsletter: this.state.funding.fctrsletter,
        jnsproduk: this.state.funding.jnsproduk, typeacc: this.state.funding.typeacc, currency: this.state.funding.currency, sumberdana: this.state.funding.sumberdana, tujuanacc: this.state.funding.tujuanacc
      },
    }

    console.log('data final: ',dataEform);
    ProHajjServices.postDataEform(dataEform)
      .then(result => {
        // console.log('RESULT DATA: ', result.data);
        // console.log('RESULT DATA: ', JSON.stringify(result.data));
        JSON.stringify({ result });
        //console.log("RESPONSE : "+JSON.stringify({result}));
        this.setState({
          //data: result.data,
          appid: (result.data.rsBody === null ? '' : result.data.rsBody.app_id),
          respmsg: result.data.rsMsg,
          respcode: result.data.msgId,
          loadspin: false,
          error: false
        });

        
        if (result.data.msgId !== '0') {
          //toast.success(this.state.respmsg)
          this.loadDataTemp2(result.data.rsBody.app_id);
          this.nextStep(1);
        } else { toast.error(this.state.respmsg) }
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          respmsg: `${error}`,
          loadspin: false,
        });
        toast.error(`${error}`)
      });
  };

  getWilayah = (e, val) => {
    /*let nmwily = {};
    if (e === "prov") {
      nmwily = {wil : e, nama : "1" };
    }else if (e === "dati2"){
      nmwily = {wil : e, nama : this.state.pemohon.dt1now };
    }else if (e === "kec"){
      nmwily = {wil : e, nama : this.state.pemohon.dt2now };
    }else if (e === "kel"){
      nmwily = {wil : e, nama : this.state.pemohon.kcmtnow };
    }*/
    let nmwily = '';
    if (e === "prov") {
      nmwily = "1";
    } else if (e === "dati2") {
      if (val === 'pp') {
        nmwily = this.state.pemohon.dt1ktp;
      } else if (val === 'pn') {
        nmwily = this.state.pemohon.dt1now;
      } else if (val === 'kt') {
        nmwily = this.state.pekerjaan.dt1ktr;
      }
    } else if (e === "kec") {
      if (val === 'pp') {
        nmwily = this.state.pemohon.dt2ktp;
      } else if (val === 'pn') {
        nmwily = this.state.pemohon.dt2now;
      } else if (val === 'kt') {
        nmwily = this.state.pekerjaan.dt2ktr;
      }
    } else if (e === "kel") {
      if (val === 'pp') {
        nmwily = this.state.pemohon.kcmtktp + "--" + this.state.pemohon.dt2ktp;
      } else if (val === 'pn') {
        nmwily = this.state.pemohon.kcmtnow + "--" + this.state.pemohon.dt2now;
      } else if (val === 'kt') {
        nmwily = this.state.pekerjaan.kcmtnktr + "--" + this.state.pekerjaan.dt2ktr;
      }
    }

    //console.log('Get : '+JSON.stringify(nmwily));
    ProHajjServices.getMasterProvKel(e, nmwily)
      .then(result => {
        result.data.sort();
        //console.log("Array Provinsi : "+JSON.stringify(result));
        if (e === "prov") {
          this.setState({
            prov: result.data,
          });
        } else if (e === "dati2") {
          if (val === 'pp') {
            this.setState({
              kab: result.data
            });
          } else if (val === 'pn') {
            this.setState({
              nkab: result.data,
            });
          } else if (val === 'kt') {
            this.setState({
              kabk: result.data,
            });
          }

        } else if (e === "kec") {
          if (val === 'pp') {
            this.setState({
              kec: result.data
            });
          } else if (val === 'pn') {
            this.setState({
              nkec: result.data,
            });
          } else if (val === 'kt') {
            this.setState({
              keck: result.data,
            });
          }
        } else if (e === "kel") {
          if (val === 'pp') {
            this.setState({
              kel: result.data
            });
          } else if (val === 'pn') {
            this.setState({
              nkel: result.data,
            });
          } else if (val === 'kt') {
            this.setState({
              kelk: result.data,
            });
          }
        }
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          error: `${error}`
          //loading: false
        });
        //toast.error(`${error}`)
      });
  };

  /*loadKodepos=(e) => {
  let sRet ='';
  if (e.lastIndexOf('-')!==-1){
    sRet =e.substr(e.lastIndexOf('-')+1)
  }
   
  return sRet;
  } */
  loadKodepos = (a, e) => {
    this.setState({ loading: true });
    ProHajjServices.getMasterProvKel("kodepos", e)
      .then(response => response.data)
      .then((data) => {
        if (a === "kdposktp") {
          this.setState({ pemohon: { ...this.state.pemohon, kdposktp: data[0] }, loading: false, error: false });
        } else if (a === "kdposnow") {
          this.setState({ pemohon: { ...this.state.pemohon, kdposnow: data[0] }, loading: false, error: false });
        } else if (a === "kdposktpf") {
          this.setState({ kerabat: { ...this.state.kerabat, kdposktpf: data[0] }, loading: false, error: false });
        } else if (a === "kdposktr") {
          this.setState({ pekerjaan: { ...this.state.pekerjaan, kdposktr: data[0] }, loading: false, error: false });
        }
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          error: `${error}`,
          loading: false
        });
      });
  }
  loadBdgUsh = () => {
    ProHajjServices.getBdgUsh()
      .then(result => {
        // console.log('result' + JSON.stringify(result));
        this.setState({ bdgush: result.data })
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          error: `${error}`
        });
      });
  }
  validToken = () => {
    // this.setState({ loadSpin: true });
    this.setState({ loading: true });
    let request = {
      appId: this.state.appid,
      token: this.state.tokensub
    }
    //console.log('token',token);
    ProHajjServices.validateToken(request)
      .then(result => {
        //console.log(result);
        this.setState({
          validresp: result.data,
          mssge: result.data.respdesc,
          loading: false,
          error: false
        });
        //toast.error(result.data.respdesc)
        if (this.state.validresp.respcode === "03" && this.state.validresp.tokenctr < 3) {
          toast.error(result.data.respdesc)
        }
        if (result.data.tokenctr > 0 && result.data.tokenctr < 3 && result.data.respcode !== '00') {
          toast.warning('Kesempatan ' + (3 - result.data.tokenctr) + ' kali lagi')
        }
        if (this.state.validresp !== null) {
          const rspcode = this.state.validresp.respcode;
          const tokenctr = this.state.validresp.tokenctr;
          if (rspcode === "00") {
            //this.handleSubmit();
            this.nextStep(1);
          } else if (rspcode === "06" && tokenctr === 3) {

            this.setState({
              mssge: '(' + tokenctr + 'x) ' + result.data.respdesc,
            });
            toast.error(result.data.respdesc)
            //this.nextStep(1);

          }
        }
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          error: `${error}`,
          loading: false
        });
      });
  };

  getSenToken = () => {
    this.setState({ loading: true });
    let appno = this.state.appid;
    let emailapp = { "eml": this.state.pemohon.email };
    //console.log('token',token);
    ProHajjServices.getTokenSenById("EM", appno, emailapp)
      .then(result => {
        //console.log("RESPONSE :"+JSON.stringify(result));
        this.setState({
          tokenctr: result.data,
          mssge: '',
          loading: false,
          error: false
        });
        if (result.data === 0) {
          toast.success('M-Pass Code berhasil terkirim')
        }
        if (result.data !== '' && result.data > 0 && result.data < 3) {
          toast.warning('Kesempatan kirim ulang M-Pass Code ' + (3 - result.data) + ' kali lagi')
        }
        if (result.data === 3) {
          toast.error('Kirim Ulang M-Pass Code telah mencapai limit 3 (tiga) kali')
        }
      })
      .catch(error => {
        console.error("error: ", error);
        this.setState({
          error: `${error}`,
          loading: false
        });
      });
  };
  formatDate = (e) => {
    var sdate = new Date(e);
    var sRet = sdate.getFullYear() + '-' + this.insertnol(sdate.getMonth() + 1) + '-' + this.insertnol(sdate.getDate());
    return sRet;
  };
  formatDateTime = (e) => {
    var sdate = new Date(e);
    //var sRet = d.toLocaleString("en-US")
    var sRet = sdate.getFullYear() + '-' + this.insertnol(sdate.getMonth() + 1) + '-' + this.insertnol(sdate.getDate()) + ' ' + this.insertnol(sdate.getHours()) + ':' + this.insertnol(sdate.getMinutes()) + ':' + this.insertnol(sdate.getSeconds());
    return sRet;
  };

  loadDataTemp = (e) => {
    this.setState({ loading: true });
    //const appidn = this.props.match.params.id;
    const appidn = e;//this.state.appid;
    ////let pth = window.location.pathname;
    //let appidn = pth.substring(pth.lastIndexOf("/")+1)
    //console.log('User :' +currentUser.username)

    ProHajjServices.getEformTempApid(appidn) //axios
      .then(result => {
        //console.log('Response : '+JSON.stringify(result));
        if (result.data) {
          this.setState({
            data: result.data,
            respmsg: result.data.app_id,
            respcode: '00',
            loading: false, token: result.data.token, stssend: result.data.stssend,
            error: false, tglnasabah: result.data.tglnasabah, accnum: result.data.accnum, kd_region: result.data.kd_region, kd_pendamping: result.data.kd_pendamping,
            appid: result.data.app_id, step: result.data.page, pagedrf: result.data.page, custflg: result.data.custflg, accnumktp: result.data.accnumktp, kd_produk: result.data.kd_produk, kd_subchannel: result.data.kd_subchannel, kd_subprogram: result.data.kd_subprogram,
            refcode: result.data.refcode, refname: result.data.refname, hprefferal: result.data.hprefferal, kd_channel: result.data.kd_channel, kd_program: result.data.kd_program, salacqcode: result.data.salacqcode, salprocode: result.data.salprocode,
            plafond: result.data.plafond, tenor: result.data.tenor, jmlpor: result.data.jmlpor, rating: result.data.rating, ratingcomment: result.data.ratingcomment, custsign: result.data.custsign, syaratpby: result.data.syaratpby, syarattab: result.data.syarattab,
            pemohon: {
              namalkp: result.data.pemohon.namalkp, namanoglr: result.data.pemohon.namanoglr, jklm: result.data.pemohon.jklm, tmplhr: result.data.pemohon.tmplhr, tgllhr: this.formatDate(result.data.pemohon.tgllhr), nikktp: result.data.pemohon.nikktp,
              npwp: result.data.pemohon.npwp, ibukdg: result.data.pemohon.ibukdg, stskwn: result.data.pemohon.stskwn, pendidikan: result.data.pemohon.pendidikan, almktp: result.data.pemohon.almktp, ktprt: result.data.pemohon.ktprt, ktprw: result.data.pemohon.ktprw, klrhktp: result.data.pemohon.klrhktp,
              kcmtktp: result.data.pemohon.kcmtktp, dt2ktp: result.data.pemohon.dt2ktp, dt1ktp: result.data.pemohon.dt1ktp, kdposktp: (result.data.pemohon.kdposktp ? this.loadKodepos('kdposktp', result.data.pemohon.dt2ktp + '--' + result.data.pemohon.kcmtktp + '--' + result.data.pemohon.klrhktp) : result.data.pemohon.kdposktp), almnowktp: result.data.pemohon.almnowktp, almnow: result.data.pemohon.almnow, nowrt: result.data.pemohon.nowrt, nowrw: result.data.pemohon.nowrw,
              klrhnow: result.data.pemohon.klrhnow, kcmtnow: result.data.pemohon.kcmtnow, dt2now: result.data.pemohon.dt2now, dt1now: result.data.pemohon.dt1now, kdposnow: (result.data.pemohon.kdposnow ? this.loadKodepos('kdposnow', result.data.pemohon.dt2now + '--' + result.data.pemohon.kcmtnow + '--' + result.data.pemohon.klrhnow) : result.data.pemohon.kdposnow), din: result.data.pemohon.din, wngr: result.data.pemohon.wngr, stspenduk: result.data.pemohon.stspenduk,
              lamathn: result.data.pemohon.lamathn, lamabln: result.data.pemohon.lamabln, stsalmnow: result.data.pemohon.stsalmnow, almsurat: result.data.pemohon.almsurat, notlpnow: result.data.pemohon.notlpnow, email: result.data.pemohon.email, nohp1: result.data.pemohon.nohp1, nohp2: result.data.pemohon.nohp2
            },
            kerabat: { namaktpf: result.data.kerabat.namaktpf, almktpf: result.data.kerabat.almktpf, notlpf: result.data.kerabat.notlpf, nohpf: result.data.kerabat.nohpf, hubnsbf: result.data.kerabat.hubnsbf },
            pekerjaan: {
              jnspkjfos: result.data.pekerjaan.jnspkjfos, stspkj: result.data.pekerjaan.stspkj, bidangktr: result.data.pekerjaan.bidangktr, namaprsh: result.data.pekerjaan.namaprsh, jabatan: result.data.pekerjaan.jabatan, lmkrjathn: result.data.pekerjaan.lmkrjathn, lmkrjabln: result.data.pekerjaan.lmkrjabln,
              ktgrinst: result.data.pekerjaan.ktgrinst, ktgrinstlain: result.data.pekerjaan.ktgrinstlain, pendptn: result.data.pekerjaan.pendptn, pbygaji: result.data.pekerjaan.pbygaji, pbygajikode: result.data.pekerjaan.pbygajikode, almktr: result.data.pekerjaan.almktr,
              klrhktr: result.data.pekerjaan.klrhktr, kcmtnktr: result.data.pekerjaan.kcmtnktr, dt2ktr: result.data.pekerjaan.dt2ktr, dt1ktr: result.data.pekerjaan.dt1ktr, kdposktr: (result.data.pekerjaan.kdposktr ? this.loadKodepos('kdposktr', result.data.pekerjaan.dt2ktr + '--' + result.data.pekerjaan.kcmtnktr + '--' + result.data.pekerjaan.klrhktr) : result.data.pekerjaan.kdposktr),
              tlpktrkd: this.parseTelpArea(result.data.pekerjaan.tlpktr), tlpktr: this.parseTelp(result.data.pekerjaan.tlpktr), extktr: result.data.pekerjaan.extktr, tlphrdkd: this.parseTelpArea(result.data.pekerjaan.tlphrdktr), tlphrdktr: this.parseTelp(result.data.pekerjaan.tlphrdktr), exthrd: result.data.pekerjaan.exthrd,
              phasiltdktetap: result.data.pekerjaan.phasiltdktetap, pluartetap: result.data.pekerjaan.pluartetap, phasiltambah: result.data.pekerjaan.phasiltambah, phasiltambahlain: result.data.pekerjaan.phasiltambahlain
            },
            funding: {
              fcrgcard: result.data.funding.fcrgcard, fcrgcardcountry: result.data.funding.fcrgcardcountry, fctax: result.data.funding.fctax, fctaxcountry: result.data.funding.fctaxcountry, fctaxid: result.data.funding.fctaxid, fctrsletter: result.data.funding.fctrsletter,
              jnsproduk: result.data.funding.jnsproduk, typeacc: result.data.funding.typeacc, currency: result.data.funding.currency, sumberdana: result.data.funding.sumberdana, tujuanacc: result.data.funding.tujuanacc
            }
          });
        } else {
          this.loadDataTemp2(appidn)
        }
      })
      .catch(error => {
        this.setState({
          error: `${error}`,
          loading: false,
          appid: '',
          //step: 0,
        });
        // toast.error(`${error}`)
        toast.error(`error koneksi ke API server`)
      });
  }

  loadDataTemp2 = (e) => {
    this.setState({ loading: true });
    const appidn = e;//this.state.appid;

    ProHajjServices.getStatusPengajuan(appidn) //axios
      .then((result) => {

        var arrayDB = [
          {
            'last_stage': result.data[0].last_stage,
            'stage_name': result.data[0].stage_name,
            'activity': result.data[0].activity,
            'status_pengajuan': result.data[0].status_pengajuan,
            'reason': result.data[0].reason,
          },

          {
            'last_stage': result.data[1].last_stage,
            'stage_name': result.data[1].stage_name,
            'activity': result.data[1].activity,
            'status_pengajuan': result.data[1].status_pengajuan,
            'reason': result.data[0].reason,
          },
          {
            'last_stage': result.data[2].last_stage,
            'stage_name': result.data[2].stage_name,
            'activity': result.data[2].activity,
            'status_pengajuan': result.data[2].status_pengajuan,
            'reason': result.data[0].reason,
          },
          {
            'last_stage': result.data[3].last_stage,
            'stage_name': result.data[3].stage_name,
            'activity': result.data[3].activity,
            'status_pengajuan': result.data[3].status_pengajuan,
            'reason': result.data[0].reason,
          },
          {
            'last_stage': result.data[4].last_stage,
            'stage_name': result.data[4].stage_name,
            'activity': result.data[4].activity,
            'status_pengajuan': result.data[4].status_pengajuan,
            'reason': result.data[0].reason,
          },
          {
            'last_stage': result.data[5].last_stage,
            'stage_name': result.data[5].stage_name,
            'activity': result.data[5].activity,
            'status_pengajuan': result.data[5].status_pengajuan,
            'reason': result.data[0].reason,
          }
        ]

        sessionStorage.setItem('arrayDB', JSON.stringify(arrayDB))

        this.setState({
          dataStatusPengajuan: result.data,
          step: 13,
        })

        // this.state.dataStatusPengajuan.map((dataPengajuanDB) => {
        //   sessionStorage.setItem('last_stage', dataPengajuanDB.last_stage)
        //   sessionStorage.setItem('stage_name', dataPengajuanDB.stage_name)
        //   sessionStorage.setItem('status_pengajuan', dataPengajuanDB.status_pengajuan)
        //   sessionStorage.setItem('activity', dataPengajuanDB.activity)

        //   // let idx = dataPengajuanDB.last_stage;
        //   // // console.log('nama: ' + index + ' ' + dataPengajuanDB.stage_name);
        // steps[idx].noSteps = dataPengajuanDB.last_stage;
        // steps[idx].label = dataPengajuanDB.stage_name;
        // steps[idx].status = dataPengajuanDB.status_pengajuan;
        // steps[idx].activity = dataPengajuanDB.activity;
        // })

        // this.state.dataStatusPengajuan.map((dataPengajuanDB, index) => {
        //   // let idx = index+1
        //   // console.log('nama: ' + index + ' ' + dataPengajuanDB.stage_name);
        //   steps[index].noSteps = dataPengajuanDB.last_stage;
        //   steps[index].label = dataPengajuanDB.stage_name;
        //   steps[index].status = dataPengajuanDB.status_pengajuan;
        //   steps[index].activity = dataPengajuanDB.activity;
        // })

      })
      .catch(error => {
        this.setState({
          error: `${error}`,
          loading: false,
          appid: '',
          //step: 0,
        });
        // toast.error(`${error}`)
        toast.error(`error koneksi ke API server`)
      });
  }

  loadDataRevisi = (e) => {
    this.setState({ loading: true });
    //const appidn = this.props.match.params.id;
    const appidn = e;//this.state.appid;
    //const tglskarang = this.formatDateTime(new Date());
    ////let pth = window.location.pathname;
    //let appidn = pth.substring(pth.lastIndexOf("/")+1)
    //console.log('Response : '+appidn)
    ProHajjServices.getEformApid(appidn)
      .then(result => {
        //console.log('Response : '+JSON.stringify(result));
        if (result.data) {
          var expir = this.formatDateTime(result.data.tglexpirerev);
          var sttrev = result.data.status;
          //if (typeof(expir) !== 'undefined'){console.log('expired undef')}
          if (typeof (expir) !== 'undefined') {
            //console.log(this.requestDate()+'expired :' +tglskarang+' < '+expir)
            //  if (tglskarang < expir ) {
            if (sttrev === 'Revision') {
              this.setState({
                data: result.data,
                respmsg: result.data.app_id,
                respcode: '00',
                loading: false,
                error: false,
                step: 9,
                status: result.data.status, tglexpirerev: result.data.tglexpirerev,
                kd_region: result.data.kd_region, kd_pendamping: result.data.kd_pendamping,
                appid: result.data.app_id, kd_produk: result.data.kd_produk, kd_subchannel: result.data.kd_subchannel, kd_subprogram: result.data.kd_subprogram,
                refcode: result.data.refcode, kd_channel: result.data.kd_channel, kd_program: result.data.kd_program, plafond: result.data.plafond,
                pekerjaan: { jnspkjfos: result.data.pekerjaan.jnspkjfos }
              });
            } else {
              this.setState({
                //respmsg: 'Url/Link Aplikasi Telah Expired', //Appid '+appidn+' Expired link',
                respmsg: 'Url/Link Aplikasi Incorrect', //Appid '+appidn+' Expired link',
                respcode: '05',
                loading: false,
                error: false,
                appid: '',
                status: result.data.status, tglexpirerev: result.data.tglexpirerev,
              })
              toast.warning(this.state.respmsg)
            }
          } else {
            this.setState({
              //respmsg: 'Appid '+appidn+' Not Found',
              respmsg: 'Incorrect Url/Link',
              respcode: '02',
              loading: false,
              error: false,
              appid: '',
              //step: 0,
            });
            toast.warning('Incorrect Url/Link')
          }
        } else {
          this.setState({
            //respmsg: 'Appid '+appidn+' Not Found',
            respmsg: 'Incorrect Url/Link',
            respcode: '03',
            loading: false,
            error: false,
            appid: '',
            //step: 0,
          });
          toast.warning('Incorrect Url/Link')
        }
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          error: `${error}`,
          loading: false,
          appid: '',
          //step: 0,
        });
        // toast.error(`${error}`)
        toast.error(`error koneksi ke API server`)
      });
  }

  handleRevUpdate = () => {
    this.setState({ loadspin: true });
    let dterev = { app_id: this.state.appid, status: 'RevisionUpdate', tglexpirerev: this.state.tglexpirerev }
    ProHajjServices.postEformRevisi(dterev)
      .then(result => {
        JSON.stringify({ result });
        //console.log(result);
        this.setState({
          //data: result.data,
          appid: (result.data.msgId === '200' ? '' : result.data.msgId),
          respmsg: result.data.rsMsg,
          respcode: result.data.rsCode,
          loadspin: false,
          error: false
        });
        if (result.data.rsCode === '01') {
          toast.success('Submit ' + this.state.respmsg)
        } else {
          toast.error(this.state.respmsg)
        }
      })
      .catch(error => {
        //console.error("error: ", error);
        this.setState({
          respmsg: `${error}`,
          loadspin: false,
        });
        toast.error(`${error}`)
      });
  }
  parseTelpArea(e) {
    var tlpk = null;
    if (e) {
      tlpk = e.substring(0, e.indexOf('-'))
    }

    return tlpk
  }
  parseTelp(e) {
    var tlp = null;
    if (e) {
      tlp = e.substring(e.indexOf('-') + 1)
    }

    return tlp
  }
  render() {
    const { step, appid, loadspin, loading, initfile, batal, mrview, moacc, accnumktp, custflg, plafond, tenor, jmlpor, ktpfile, urlktp,
      prov, kab, kec, kel, nkab, nkec, nkel, kabk, keck, kelk, npwpfile, urlnpwp, gajifile, urlgaji, legalfile, urllegal, mutasifile, urlmutasi,
      custsign, mcsig, syaratpby, mspby, mspblhajj, syarattab, mstab, selectedFiles, currentFile, progress1, progress2, progress3, progress4, progress5, message,
      tokensub, tokenctr, validresp, mssge, rating, ratingcomment, kd_produk, kd_channel, kd_subchannel, kd_program, etypeproduk,
      kd_subprogram, kd_region, refcode, kd_pendamping, urlimgdoc, status, bdgush,
      pemohon: { namalkp, namanoglr, jklm, tmplhr, tgllhr, wngr, din, nikktp,
        npwp, ibukdg, stskwn, pendidikan, almktp, ktprt, ktprw, klrhktp,
        kcmtktp, dt2ktp, dt1ktp, kdposktp, almnowktp, almnow, nowrt, nowrw, klrhnow,
        kcmtnow, dt2now, dt1now, kdposnow, stspenduk, lamabln, lamathn, stsalmnow, almsurat,
        notlpnowkd, notlpnow, nohp1, nohp2, email },
      pekerjaan: { jnspkjfos, stspkj, namaprsh, ktgrinst, ktgrinstlain, bidangktr, jabatan, lmkrjathn, lmkrjabln,
        pendptn, pbygaji, pbygajikode, phasiltdktetap, pluartetap, phasiltambah, phasiltambahlain, almktr,
        klrhktr, kcmtnktr, dt2ktr, dt1ktr, kdposktr, tlpktrkd, tlpktr, extktr, tlphrdkd, tlphrdktr, exthrd },
      kerabat: { namaktpf, nohpf, hubnsbf, notlpf },
      funding: { fcrgcard, fcrgcardcountry, fctax, fctaxcountry, fctaxid, fctrsletter,
        jnsproduk, typeacc, currency, sumberdana, tujuanacc } } = this.state;
    const flayer = {
      loading, appid, step, moacc, accnumktp, custflg, kd_produk, kd_channel, kd_subchannel, kd_program,
      kd_subprogram, kd_region, refcode, kd_pendamping, etypeproduk
    }
    const pbypmhn = {
      loading, batal, plafond, tenor, jmlpor, prov, kab, kec, kel, nkab, nkec, nkel, message, kd_produk, etypeproduk,
      pemohon: {
        namalkp, namanoglr, jklm, tmplhr, tgllhr, wngr, din, nikktp,
        npwp, ibukdg, stskwn, pendidikan, almktp, ktprt, ktprw, klrhktp,
        kcmtktp, dt2ktp, dt1ktp, kdposktp, almnowktp, almnow, nowrt, nowrw, klrhnow,
        kcmtnow, dt2now, dt1now, kdposnow, stspenduk, lamabln, lamathn, stsalmnow, almsurat,
        notlpnowkd, notlpnow, nohp1, nohp2, email
      }
    };
    const mjob = {
      prov, kabk, keck, kelk, loading, batal, kd_produk, etypeproduk, bdgush,
      pekerjaan: {
        jnspkjfos, stspkj, namaprsh, ktgrinst, ktgrinstlain, bidangktr, jabatan, lmkrjathn, lmkrjabln,
        pendptn, pbygaji, pbygajikode, phasiltdktetap, pluartetap, phasiltambah, phasiltambahlain, almktr,
        klrhktr, kcmtnktr, dt2ktr, dt1ktr, kdposktr, tlpktrkd, tlpktr, extktr, tlphrdkd, tlphrdktr, exthrd
      }
    };
    const mfam = { kerabat: { namaktpf, nohpf, hubnsbf, notlpf } };
    const mfac = {
      appid, batal, kd_produk, etypeproduk, funding: {
        fcrgcard, fcrgcardcountry, fctax, fctaxcountry, fctaxid, fctrsletter,
        jnsproduk, typeacc, currency, sumberdana, tujuanacc
      }
    };
    const muplo = {
      batal, appid, initfile, plafond, ktpfile, urlktp, npwpfile, urlnpwp, gajifile, urlgaji, legalfile, urllegal, mutasifile, urlmutasi,
      selectedFiles, currentFile, progress1, progress2, progress3, progress4, progress5, message, urlimgdoc, status, kd_produk, etypeproduk,
      pekerjaan: { jnspkjfos }
    };
    const mgree = {
      batal, kd_produk, etypeproduk, loading, custsign, mcsig, syaratpby, mspby, mspblhajj, syarattab, mstab, tokensub, tokenctr, validresp, mssge,
      pemohon: { namalkp, email }
    };
    const rsubm = { rating, ratingcomment, kd_produk, etypeproduk, }
    const sccesfield = { appid, kd_produk, etypeproduk, pemohon: { namalkp } }
    const rview = {
      appid, loading, batal, mrview, accnumktp, plafond, tenor, jmlpor, ktpfile, urlktp, npwpfile, urlnpwp, gajifile, urlgaji, legalfile, urllegal, mutasifile, urlmutasi, urlimgdoc, kd_produk, etypeproduk,
      pemohon: {
        namalkp, namanoglr, jklm, tmplhr, tgllhr, wngr, din, nikktp, npwp, ibukdg, stskwn, pendidikan, almktp, ktprt, ktprw, klrhktp,
        kcmtktp, dt2ktp, dt1ktp, kdposktp, almnowktp, almnow, nowrt, nowrw, klrhnow, kcmtnow, dt2now, dt1now, kdposnow, stspenduk, lamabln, lamathn, stsalmnow, almsurat,
        notlpnowkd, notlpnow, nohp1, nohp2, email
      },
      pekerjaan: {
        jnspkjfos, stspkj, namaprsh, ktgrinst, ktgrinstlain, bidangktr, jabatan, lmkrjathn, lmkrjabln, pendptn, pbygaji, pbygajikode, phasiltdktetap, pluartetap, phasiltambah, phasiltambahlain, almktr,
        klrhktr, kcmtnktr, dt2ktr, dt1ktr, kdposktr, tlpktrkd, tlpktr, extktr, tlphrdkd, tlphrdktr, exthrd
      },
      kerabat: { namaktpf, nohpf, hubnsbf, notlpf },
      funding: { fcrgcard, fcrgcardcountry, fctax, fctaxcountry, fctaxid, fctrsletter, jnsproduk, typeacc, currency, sumberdana, tujuanacc }
    }
    const lastpage = { loading, kd_produk, etypeproduk }

    if (loadspin) {
      return (
        <Loader prod={this.state.kd_produk}
          eprod={this.state.etypeproduk} />
      )
    }
    if (this.state.respcode === "01" || this.state.respcode === "02" || this.state.respcode === "05") {
      return (
        <MessageProhajj
          message={this.state.respcode + this.state.respmsg}
          prod={this.state.kd_produk}
          eprod={this.state.etypeproduk} />
      );
    }

    switch (step) {
      case 0:
        return (
          <EformFlayer
            onTop={this.onTop}
            nextStep={this.nextStep}
            handleModalAcc={this.handleModalAcc}
            radioChange={this.radioChange}
            handleChange={this.handleChange}
            flayer={flayer}
            loadBdgUsh={this.loadBdgUsh}
          />
        )

      case 1:
        return (
          <EformPembiayaan
            onTop={this.onTop}
            nextStep={this.nextStep}
            handleModalbtl={this.handleModalbtl}
            handleChangeN={this.handleChangeN}
            handleChangeP={this.handleChangeP}
            toCurrency={this.toCurrency}
            calculate_age={this.calculate_age}
            emailValidation={this.emailValidation}
            pbypmhn={pbypmhn}
            handleSaveTemp={this.handleSaveTemp}
            flayer={flayer}
          />
        );
      case 2:
        return (
          <EformPemohon1
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleModalbtl={this.handleModalbtl}
            handleChangeP={this.handleChangeP}
            getWilayah={this.getWilayah}
            pbypmhn={pbypmhn}
            handleSaveTemp={this.handleSaveTemp}
            emptypemohon={this.emptypemohon}
          />
        );
      case 3:
        return (
          <EformPemohon2
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleModalbtl={this.handleModalbtl}
            handleChangeP={this.handleChangeP}
            getWilayah={this.getWilayah}
            pbypmhn={pbypmhn}
            handleSaveTemp={this.handleSaveTemp}
          />
        );
      case 4:
        return (
          <EformPemohon3
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleModalbtl={this.handleModalbtl}
            handleChangeP={this.handleChangeP}
            pbypmhn={pbypmhn}
            handleSaveTemp={this.handleSaveTemp}
          />
        );
      case 5:
        return (
          <EformPekerjaan
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleModalbtl={this.handleModalbtl}
            handleChangeK={this.handleChangeK}
            mjob={mjob}
            handleSaveTemp={this.handleSaveTemp}
            loadBdgUsh={this.loadBdgUsh}
          />
        );
      case 6:
        return (
          <EformKerabat
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleModalbtl={this.handleModalbtl}
            handleChangeK={this.handleChangeK}
            handleChangeF={this.handleChangeF}
            getWilayah={this.getWilayah}
            mjob={mjob}
            mfam={mfam}
            handleSaveTemp={this.handleSaveTemp}
            loadBdgUsh={this.loadBdgUsh}
          />
        );
      case 7:
        return (
          <EformCrsFatca
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleModalbtl={this.handleModalbtl}
            handleChangeFC={this.handleChangeFC}
            mfac={mfac}
            handleSaveTemp={this.handleSaveTemp}
          />
        );
      case 8:
        return (
          <EformDaftarPorsi
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleModalbtl={this.handleModalbtl}
            handleChangeFC={this.handleChangeFC}
            mfac={mfac}
            handleSaveTemp={this.handleSaveTemp}
          />
        );
      case 9:
        return (
          <EformUploadDoc
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleModalbtl={this.handleModalbtl}
            handleChangeUktp={this.handleChangeUktp}
            handleChangeUnpwp={this.handleChangeUnpwp}
            handleChangeGaji={this.handleChangeGaji}
            handleChangeLegal={this.handleChangeLegal}
            handleChangeMutasi={this.handleChangeMutasi}
            uploadFileDoc={this.uploadFileDoc}
            handleRevUpdate={this.handleRevUpdate}
            muplo={muplo}
            handleSaveTemp={this.handleSaveTemp}
          />
        );
      case 10:
        return (
          <EformReview
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleModalbtl={this.handleModalbtl}
            handleModalrview={this.handleModalrview}
            toCurrency={this.toCurrency}
            dateNow={this.dateNow}
            rview={rview}
            handleSaveTemp={this.handleSaveTemp}
            loadBdgUsh={this.loadBdgUsh}
            mjob={mjob}
          />
        );
      case 11:
        return (
          <EformAgremen
            onTop={this.onTop}
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            dateNow={this.dateNow}
            handleModalbtl={this.handleModalbtl}
            handleChangeCkBox={this.handleChangeCkBox}
            handleModalpnyata={this.handleModalpnyata}
            handleModalagree={this.handleModalagree}
            handleModalalhajj={this.handleModalalhajj}
            handleModalsytab={this.handleModalsytab}
            validToken={this.validToken}
            getSenToken={this.getSenToken}
            handleChangeN={this.handleChangeN}
            handleChange={this.handleChange}
            mgree={mgree}
            handleSaveTemp={this.handleSaveTemp}
            mjob={mjob}
          />
        );
      case 12:
        return (
          <EformRatingSubmit
            onTop={this.onTop}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
            rsubm={rsubm}
            mjob={mjob}
          />
        );
      case 13:
        return (
          <LastPage
            lastpage={lastpage}
            handleSaveTemp={this.handleSaveTemp} />
        )
      default:
        return (<SuccessAkad sccesfield={sccesfield} />);
    }
  }
}

export default CustAcqProhajjMain

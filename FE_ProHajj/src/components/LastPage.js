import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {TextField, MenuItem, Box} from '@material-ui/core/';
import ModalConfirm from './ModalConfirm';
import NumberFormat from 'react-number-format';
//import { ToastContainer } from 'react-toastify';
import StepperPengajuan from './StepperPengajuan'

export class LastPage extends Component {
    constructor(props) {super(props)}
    render() {
        return (
            <div>
                <Header prod = {this.props.lastpage.kd_produk} eprod = {this.props.lastpage.etypeproduk}/>
                <Container >
                <ModalConfirm />
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header style={{borderBottom:'none', height:'87.75px'}}>
                        <div className="d-flex left-content-start">
                            <table style={{width:'100%'}}>
                            <tbody>
                            <tr>
                                <td style={{width:'100%',textAlign:'center'}}><h5 style={{fontSize:'24px', marginTop:'15px', marginBottom:'15px'}}>Status Pengajuan</h5></td>
                            </tr>
                            </tbody>
                            </table>
                        </div>
                    </Card.Header>
                    <Card.Body>
                        <h6 style={{color:'#363636',fontWeight:'500', fontSize:'20px',marginTop:'86.25px'}}>Berikut status pengajuan anda saat ini : </h6>
                        <StepperPengajuan />
                        {/* <div style={{background: '#28A745', width:'289px', height:'79px', left:'575px', marginTop:'41px', marginLeft:'auto', marginRight:'auto', marginBottom:'48px', borderRadius:'8px'}}>
                        <img src='./assets/shape.png' className="rounded float-left" alt="Icon" style={{marginTop:'18px', marginLeft:'18px', marginRight:'18px'}}/>
                            <div style={{textAlign:'left', marginLeft:'56px'}}>
                                <h5 style={{color:'#FFFFFF',fontWeight:'bold', fontSize:'16px',paddingTop:'16px'}}>Success notification</h5>
                                <h6 style={{color:'#FFFFFF',fontWeight:'normal', fontSize:'14px', margin: 0,paddingBottom:'16px'}}>Pengisian form berhasil</h6>
                            </div>
                        </div> */}
                    </Card.Body>
                    <Card.Footer className="d-flex left-content-start" style={{background:'#F7F7F7', height:'87.75px', borderTop:'none'}}></Card.Footer>
                </Card>
                </Container>
            </div>
        )
    }
}

export default LastPage
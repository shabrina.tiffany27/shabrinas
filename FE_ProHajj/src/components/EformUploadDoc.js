import React, { Component } from 'react'
import {Image, Button, Container, Row, Col, Card, Form } from 'react-bootstrap'
//import {TextField} from '@material-ui/core/';
import './Prohajj.css';
import Header from './Header';
import ModalConfirm from './ModalConfirm';
//import { ToastContainer } from 'react-toastify';

export class EformUploadDoc extends Component {
    continue = e => {
        e.preventDefault();
        this.props.handleSaveTemp();
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.onTop();  
    }

    isEnableSubmit() {
        let sRet = true;

        // let uplo1 = [this.props.muplo.urlgaji, this.props.muplo.urllegal, this.props.muplo.urlmutasi].filter((v) => v).length !== 3;
        // let uplo2 = [this.props.muplo.urlgaji, this.props.muplo.urlmutasi].filter((v) => v).length !== 2;
        if (this.props.muplo.plafond > 50000000 && (this.props.muplo.initfile === 2 && this.props.muplo.progress2 === 100 && this.props.muplo.progress1 === 0)
             || (this.props.muplo.initfile === 3 && this.props.muplo.progress3 === 100 )
             || (this.props.muplo.initfile === 4 && this.props.muplo.progress4 === 100 )
             || (this.props.muplo.initfile === 5 && this.props.muplo.progress5 === 100 )
            ) {
            sRet = false;
        }else if (this.props.muplo.plafond <= 50000000 && (this.props.muplo.initfile === 1 && this.props.muplo.progress1 === 100)
             || (this.props.muplo.initfile === 3 && this.props.muplo.progress3 === 100 )
             || (this.props.muplo.initfile === 4 && this.props.muplo.progress4 === 100 )
             || (this.props.muplo.initfile === 5 && this.props.muplo.progress5 === 100 )
            ) { 
            sRet = false;
        }

        return sRet;
    }
    render() {
        const {handleRevUpdate, handleSaveTemp, handleChangeUktp, handleChangeUnpwp, handleChangeGaji, handleChangeLegal, handleChangeMutasi, uploadFileDoc } = this.props;
        const jenisp = (this.props.muplo.pekerjaan.jnspkjfos === '2' || this.props.muplo.pekerjaan.jnspkjfos === '4')
        //const stsEnable=(this.props.muplo.pekerjaan.jnspkjfos === '1' || this.props.muplo.pekerjaan.jnspkjfos === '3');
        const isEnabled = this.props.muplo.plafond > 50000000;
        const isEnabnpwp = (this.props.muplo.plafond > 50000000 && this.props.muplo.progress1 === 100);
        const isEnabGaji = (this.props.muplo.plafond > 50000000?this.props.muplo.progress2 === 100:this.props.muplo.progress1 === 100) && !jenisp;
        const isEnabmMutasi = ( (this.props.muplo.plafond > 50000000?this.props.muplo.progress2 === 100:this.props.muplo.progress1 === 100) || this.props.muplo.progress4 === 100) && jenisp;
        const selectedFilesKtp = (this.props.muplo.appid !== '' && this.props.muplo.selectedFiles && this.props.muplo.urlktp && this.props.muplo.initfile === 1 )
        const selectedFilesNpwp = (this.props.muplo.appid !== '' && this.props.muplo.selectedFiles && this.props.muplo.urlnpwp && this.props.muplo.initfile === 2)
        const selectedFilesGaji = (this.props.muplo.appid !== '' && this.props.muplo.selectedFiles && this.props.muplo.urlgaji && this.props.muplo.initfile === 3 )
        const selectedFilesLegal = (this.props.muplo.appid !== '' && this.props.muplo.selectedFiles && this.props.muplo.urllegal && this.props.muplo.initfile === 4 )
        const selectedFilesMutasi = (this.props.muplo.appid !== '' && this.props.muplo.selectedFiles && this.props.muplo.urlmutasi && this.props.muplo.initfile === 5 )
        const warappid = (this.props.muplo.appid === '' )
        const isHideStatus = (this.props.muplo.status === 'Revision')
        //const uplo1 = [this.props.muplo.urlgaji, this.props.muplo.urllegal, this.props.muplo.urlmutasi].filter((v) => v).length !== 3;
        //const uplo2 = [this.props.muplo.urlgaji, this.props.muplo.urlmutasi].filter((v) => v).length !== 2;
        const uplo1 = [this.props.muplo.urlktp, this.props.muplo.urlgaji].filter((v) => v).length !== 2;
        const uplo2 = [this.props.muplo.urlktp, this.props.muplo.urlmutasi].filter((v) => v).length !== 2;
        const uplo3 = [this.props.muplo.urlktp, this.props.muplo.urlnpwp, this.props.muplo.urlgaji].filter((v) => v).length !== 3;
        const uplo4 = [this.props.muplo.urlktp, this.props.muplo.urlnpwp, this.props.muplo.urlmutasi].filter((v) => v).length !== 3;
        const disLjt = (jenisp?(isEnabled?uplo4:uplo2):(isEnabled?uplo3:uplo1))
        const URL_API = this.props.muplo.urlimgdoc;
        return (
            <div>
                <Header prod = {this.props.muplo.kd_produk} eprod = {this.props.muplo.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.muplo.batal}/>
                <Form onSubmit={this.continue} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'80%',textAlign:'left'}}><h5>Dokumen Nasabah</h5></td>
                            <td style={{width:'20%',textAlign:'right'}} hidden={isHideStatus}>Page 5</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                    <Row >
                            <Col md>
                            <div className="p-2 col-example text-center">
                            <Card>
                                <Card.Header>
                                    <input style={{display: 'none'}} type="file" name="ktpfile"  accept="image/jpeg, image/png, image/gif" onChange={handleChangeUktp} 
                                    ref={fileInputk => this.fileInputk = fileInputk} />
                                    <Button variant='primary' onClick={() => this.fileInputk.click()}  size='sm'>Upload KTP</Button>
                                </Card.Header>
                                <Card.Body>
                                    <Form.Text className='text-success text-center'><i>Jika dokumen belum sesuai, Pemohon dapat mengupload ulang dengan klik "Upload KTP"</i></Form.Text>
                                    <div className="Image">
                                        <Image src={(!this.props.muplo.urlktp?URL_API+'/files/'+this.props.muplo.appid:this.props.muplo.urlktp)} width="50%" />
                                    </div>
                                    <hr hidden={!selectedFilesKtp}/>
                                    {this.props.muplo.currentFile && this.props.muplo.urlktp && this.props.muplo.progress1 > 0 && (
                                    
                                    <div  className="progress" style={{ margin:'auto', height:'9px', width:'200px'}}>
                                        
                                        <div
                                        className="progress-bar progress-bar-info progress-bar-striped"
                                        role="progressbar"
                                        aria-valuenow={this.props.muplo.progress1}
                                        aria-valuemin="0"
                                        aria-valuemax="100"
                                        style={{ background:'limegreen', width: this.props.muplo.progress1 + "%" }}
                                        >
                                        <small>{this.props.muplo.progress1}%</small>
                                        </div>
                                    </div>
                                    )}
                                    <Form.Text className='text-danger text-center' hidden={!warappid}><i>App ID Failed</i></Form.Text><button hidden={!warappid} className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Kembali'>Klik Disini</button>
                                    <Button variant='success' id='ktp' size='sm' hidden={!selectedFilesKtp} onClick={uploadFileDoc}>Submit KTP</Button>
                                    <Form.Text   id='ktpu' className='text-danger text-center' hidden={!selectedFilesKtp}><i>Jika dokumen sudah sesuai, silahkan klik tombol Submit KTP</i></Form.Text>
                                    <Form.Text className='text-success text-center'><small><i>Wajib dalam format JPEG/PNG/GIF atau Ambil Foto<br/>Jika KTP buram harap foto KTP + SIM/Paspor bersamaan</i></small></Form.Text>
                                </Card.Body>
                            </Card>
                            </div>
                            </Col>
                            <Col md hidden={!isEnabled}>
                            <div className="p-2 col-example text-center">
                            <Card>
                                <Card.Header>
                                    <input style={{display: 'none'}} type="file" name="npwpfile" accept="image/jpeg, image/png, image/gif"  onChange={handleChangeUnpwp} 
                                    ref={fileInputn => this.fileInputn = fileInputn} />
                                    <Button variant='primary' onClick={() => this.fileInputn.click()} size='sm' disabled={!isEnabnpwp}>Upload NPWP</Button>
                                </Card.Header>
                                <Card.Body>
                                <Form.Text className='text-success text-center'><i>Jika dokumen belum sesuai, Pemohon dapat mengupload ulang dengan klik "Upload NPWP"</i></Form.Text>
                                    <div className="Image">
                                        <Image src={isEnabled?(!this.props.muplo.urlnpwp?URL_API+'/filenpwp/'+this.props.muplo.appid:this.props.muplo.urlnpwp):'./assets/idcardempty.png'} width="50%" />
                                        <Form.Text className='text-muted text-center' hidden={isEnabled}><i>Pengajuan Pembiayaan diatas 50jt</i></Form.Text>
                                    </div>
                                    <hr hidden={!selectedFilesNpwp}/>
                                    {this.props.muplo.currentFile && this.props.muplo.urlnpwp && this.props.muplo.progress2 > 0 &&(
                                    <div className="progress" >
                                        <div
                                        className="progress-bar progress-bar-info progress-bar-striped"
                                        role="progressbar"
                                        aria-valuenow={this.props.muplo.progress2}
                                        aria-valuemin="0"
                                        aria-valuemax="100"
                                        style={{ width: this.props.muplo.progress2 + "%" }}
                                        >
                                        {this.props.muplo.progress2}%
                                        </div>
                                    </div>
                                    )}
                                    <Form.Text className='text-danger text-center' hidden={!warappid}><i>App ID Failed</i></Form.Text><button hidden={!warappid} className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Kembali'>Klik Disini</button>
                                    <Button variant='success' id='npwp' size='sm' hidden={!selectedFilesNpwp} onClick={uploadFileDoc}>Submit NPWP</Button>
                                    <Form.Text  id='npwpu' className='text-danger text-center' hidden={!selectedFilesNpwp}><i>Jika dokumen sudah sesuai, silahkan klik tombol Submit NPWP</i></Form.Text>
                                    <Form.Text className='text-success text-center'><small><i>Wajib dalam format JPEG/PNG/GIF atau Ambil Foto</i></small></Form.Text>
                                </Card.Body>
                            </Card>
                            </div>
                            </Col>
                        </Row>
                        <Row hidden={jenisp}>
                        <Col md >
                            <div className="p-2 col-example text-center">
                            <Card>
                                <Card.Body>
                                    <Row>
                                        <Col md ='7'>
                                            <div className="d-flex left-content-start">
                                                <table style={{width:'100%'}}>
                                                <tbody>
                                                <tr>
                                                    <td style={{width:'80%',textAlign:'left'}}>Slip Gaji Bulan Terakhir/Surat Ket. Penghasilan{jenisp?'':' *'}</td>
                                                    <td style={{width:'20%',textAlign:'right'}}>
                                                        <input style={{display: 'none'}} type="file" name="ingaji" accept="application/pdf"  onChange={handleChangeGaji} 
                                                        ref={fileInputg => this.fileInputg = fileInputg} />
                                                        <Button variant='primary' onClick={() => this.fileInputg.click()} size='sm' disabled={!isEnabGaji} >Browse</Button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                </table>
                                            </div>
                                        </Col>
                                        <Col md>
                                            <Form.Text hidden={jenisp} className='text-success text-center'><i>{(!this.props.muplo.urlgaji?<a target='_blank' rel='noreferrer noopener' href={URL_API+'/docgji/'+this.props.muplo.appid}>{this.props.muplo.urlgaji?'':'View PDF'}</a>:this.props.muplo.urlgaji)}</i></Form.Text>
                                        </Col>
                                        <Col md>
                                            <Button variant='success' id='upgaji' size='sm' hidden={!selectedFilesGaji} onClick={uploadFileDoc}>Upload</Button>
                                        </Col>
                                    </Row>
                                    <hr hidden={!selectedFilesGaji}/>
                                    {this.props.muplo.currentFile && this.props.muplo.urlgaji && this.props.muplo.progress3 > 0 && (
                                    <div className="progress" style={{ margin:'auto', height:'9px', width:'200px'}} >
                                        <div
                                        className="progress-bar progress-bar-info progress-bar-striped"
                                        role="progressbar"
                                        aria-valuenow={this.props.muplo.progress3}
                                        aria-valuemin="0"
                                        aria-valuemax="100"
                                        style={{ background:'limegreen',width: this.props.muplo.progress3 + "%" }}
                                        >
                                        <small>{this.props.muplo.progress3}%</small>
                                        </div>
                                    </div>
                                    )}
                                    <Form.Text className='text-success text-center'><small><i>Wajib dalam format PDF, untuk SKP wajib memiliki tanggal penerbian surat</i></small></Form.Text>
                                </Card.Body>
                            </Card>
                            </div>
                            </Col>
                        </Row>
                        <Row hidden={!jenisp}>
                        <Col md>
                            <div className="p-2 col-example text-center">
                            <Card>
                                <Card.Body>
                                    <Row>
                                        <Col md='7'>
                                            <div className="d-flex left-content-start">
                                                <table style={{width:'100%'}}>
                                                <tbody>
                                                <tr>
                                                    <td style={{width:'80%',textAlign:'left'}}>Dokumen Legalitas Usaha (Optional)</td>
                                                    <td style={{width:'20%',textAlign:'right'}}>
                                                        <input style={{display: 'none'}} type="file" name="inlegal" acceptedFiles="application/pdf"  onChange={handleChangeLegal} 
                                                        ref={fileInputl => this.fileInputl = fileInputl} />
                                                        <Button variant='primary' onClick={() => this.fileInputl.click()} size='sm' disabled={!jenisp} >Browse</Button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                </table>
                                            </div>
                                        </Col>
                                        <Col md>
                                            <Form.Text hidden={!jenisp} className='text-success text-center'><i>{(!this.props.muplo.urllegal?<a target='_blank' rel='noreferrer noopener' href={URL_API+'/doclgl/'+this.props.muplo.appid}>{this.props.muplo.urllegal?'':'View PDF'}</a>:this.props.muplo.urllegal)}</i></Form.Text>
                                        </Col>
                                        <Col md>
                                            <Button variant='success' id='uplegal' size='sm' hidden={!selectedFilesLegal} onClick={uploadFileDoc}>Upload</Button>
                                        </Col>
                                    </Row>
                                    <hr hidden={!selectedFilesLegal}/>
                                    {this.props.muplo.currentFile && this.props.muplo.urllegal && this.props.muplo.progress4 > 0 && (
                                    <div className="progress" style={{ margin:'auto', height:'9px', width:'200px'}}>
                                        <div
                                        className="progress-bar progress-bar-info progress-bar-striped"
                                        role="progressbar"
                                        aria-valuenow={this.props.muplo.progress4}
                                        aria-valuemin="0"
                                        aria-valuemax="100"
                                        style={{background:'limegreen', width: this.props.muplo.progress4 + "%" }}
                                        >
                                        <small>{this.props.muplo.progress4}%</small>
                                        </div>
                                    </div>
                                    )}
                                </Card.Body>
                            </Card>
                            </div>
                            </Col>
                        </Row>
                        <Row hidden={!jenisp}>
                        <Col md>
                            <div className="p-2 col-example text-center">
                            <Card>
                                <Card.Body>
                                    <Row>
                                        <Col md='7'>
                                            <div className="d-flex left-content-start">
                                                <table style={{width:'100%'}}>
                                                <tbody>
                                                <tr>
                                                    <td style={{width:'80%',textAlign:'left'}}>Mutasi Rekening 3 Bulan Terakhir {jenisp?' *':''}</td>
                                                    <td style={{width:'20%',textAlign:'right'}}>
                                                        <input style={{display: 'none'}} type="file" name="inmutasi" accept="application/pdf" onChange={handleChangeMutasi} 
                                                        ref={fileInputm => this.fileInputm = fileInputm} />
                                                        <Button variant='primary' onClick={() => this.fileInputm.click()} size='sm' disabled={!isEnabmMutasi} >Browse</Button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                </table>
                                            </div>
                                        </Col>
                                        <Col md>
                                            <Form.Text hidden={!jenisp} className='text-success text-center'><i>{(!this.props.muplo.urlmutasi?<a target='_blank' rel='noreferrer noopener' href={URL_API+'/docmts/'+this.props.muplo.appid}>{this.props.muplo.urlmutasi?'':'View PDF'}</a>:this.props.muplo.urlmutasi)}</i></Form.Text>
                                        </Col>
                                        <Col md>
                                            <Button variant='success' id='upmutasi' size='sm' hidden={!selectedFilesMutasi} onClick={uploadFileDoc}>Upload</Button>
                                        </Col>
                                    </Row>
                                    <hr hidden={!selectedFilesMutasi}/>
                                    {this.props.muplo.currentFile && this.props.muplo.urlmutasi && this.props.muplo.progress5 > 0 && (
                                    <div className="progress" style={{ margin:'auto', height:'9px', width:'200px'}}>
                                        <div
                                        className="progress-bar progress-bar-info progress-bar-striped"
                                        role="progressbar"
                                        aria-valuenow={this.props.muplo.progress5}
                                        aria-valuemin="0"
                                        aria-valuemax="100"
                                        style={{ background:'limegreen', width: this.props.muplo.progress5 + "%" }}
                                        >
                                        <small>{this.props.muplo.progress5}%</small>
                                        </div>
                                    </div>
                                    )}
                                    <Form.Text className='text-success text-center'><small><i>Wajib upload print/halaman rekening tabungan 3 bulan terakhir dalam format PDF<br/>Contoh : pengajuan bulan Agustus, wajib melampirkan bulan Mei, Juni, Juli</i></small></Form.Text>
                                </Card.Body>
                            </Card>
                            </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <div className="p-2 col-example text-justify" 
                                dangerouslySetInnerHTML={{__html: "<small><b>Dokumen yang dilampirkan WAJIB :" 
                                +"<br>1. Dalam Format JPEG/PNG/GIF dan PDF"
                                +"<br>2. Max. size 5 MB per Dokumen"
                                +"<br>3. Tidak terpotong"
                                +"<br>4. Tidak buram"
                                +"<br>5. Jelas terbaca</b></small>"
                                }}/>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start" >
                                <table style={{width:'100%'}} >
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}} hidden={isHideStatus}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={isHideStatus}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='submit' 
                                            id='continue'
                                            disabled={this.isEnableSubmit() || disLjt}
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true /*isHideStatus*/}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={isHideStatus}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={!isHideStatus}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='saveupload'
                                            disabled={this.isEnableSubmit() || disLjt}
                                            onClick={handleRevUpdate}
                                            title='Save Upload'>SUBMIT</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformUploadDoc
//<Form.Text className='text-muted text-center' hidden={isEnabled}><i>Pengajuan Pembiayaan diatas 50jt</i></Form.Text>
//<Image src={(!this.props.muplo.urlnpwp?'./assets/idcardempty.png':this.props.muplo.urlnpwp)} width="50%" />
//<br/><i><small>(Wajib 6 bulan jika tidak memiliki Dok Legalitas Usaha bagi Self Employee)</small></i>
/*
                                                        <TextField
                                                            id="originalFileName"
                                                            type="file"
                                                            inputProps={{ acceptedFiles: '.jpeg, .pdf' }}
                                                            required
                                                            label="Dokumen"
                                                            name="inlegal"
                                                            onChange={handleChangeLegal}
                                                            size="small"
                                                            variant="standard"
                                                        />
*/
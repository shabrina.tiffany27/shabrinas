import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header';
import {TextField, MenuItem} from '@material-ui/core/';
import ModalConfirm from './ModalConfirm';
import NumberFormat from 'react-number-format';
//import { ToastContainer } from 'react-toastify';

export class EformKerabat extends Component {
    continue = e => {
        e.preventDefault();
        this.props.handleSaveTemp();
        this.props.nextStep(1);
    }
    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    componentDidMount() {
        this.props.getWilayah('prov');
        this.props.getWilayah('dati2','kt');
        this.props.getWilayah('kec','kt');
        this.props.getWilayah('kel','kt');
        this.props.onTop();  
        this.props.loadBdgUsh();
    }
    
    render() {
        //const numOnly = /^[0-9\b]+$/;
        const { handleChangeK, handleChangeF, handleSaveTemp } = this.props
        const prov = (this.props.mjob.prov?this.props.mjob.prov:[]);
        const kab = (this.props.mjob.kabk?this.props.mjob.kabk:[]);
        const kec = (this.props.mjob.keck?this.props.mjob.keck:[]);
        const kel = (this.props.mjob.kelk?this.props.mjob.kelk:[]);
        const notlpLenght = (this.props.mfam.kerabat.notlpf.length === 0 || this.props.mfam.kerabat.notlpf.length >= 10 && this.props.mfam.kerabat.notlpf.length < 14)
        const checkNoTlp = () => {
            if(this.props.mfam.kerabat.notlpf.length > 0) {
                if(this.props.mfam.kerabat.notlpf.length < 10) {
                    return "Nomor Telepon tidak valid, digit terlalu sedikit"
                }
                else if (this.props.mfam.kerabat.notlpf.length > 13) {
                    return "Nomor Telepon tidak valid, digit terlalu banyak"
                }
            }
        }
        const checkNoHp = () => {
            if(this.props.mfam.kerabat.nohpf.length > 0) {
                if(this.props.mfam.kerabat.nohpf[0] !== '0' || this.props.mfam.kerabat.nohpf[1] !== '8'){
                    return 'Nomor HP tidak valid, harap diawali dengan 08'
                }
                else if(this.props.mfam.kerabat.nohpf.length < 10) {
                    return "Nomor HP tidak valid, digit terlalu sedikit"
                }
                else if (this.props.mfam.kerabat.nohpf.length > 13) {
                    return"Nomor HP tidak valid, digit terlalu banyak"
                }
            }
        }
        
        return (
            <div>
                {console.log('gajinya dibayar lewat ', this.props.mjob.pekerjaan.pbygaji)}
                <Header prod = {this.props.mjob.kd_produk} eprod = {this.props.mjob.etypeproduk}/>
                <Container >
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.mjob.batal}/>
                <Form onSubmit={this.continue} /*noValidate*/>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'50%',textAlign:'left'}}><h5>Data Pekerjaan Pemohon</h5></td><td style={{width:'50%',textAlign:'right'}}>Page 2-2</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                            <div className="p-2 col-example text-center"
                                dangerouslySetInnerHTML={{__html: "<b>Alamat Kantor</b>"
                                }}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group >
                                    <TextField
                                    label="Provinsi"
                                    required
                                    fullWidth
                                    value ={this.props.mjob.pekerjaan.dt1ktr}
                                    name="dt1ktr"
                                    onChange={handleChangeK}
                                    variant="standard"
                                    size="small"
                                    select>
                                        {
                                        prov.map((h) => 
                                        (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                        }
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Kabupaten / Kota"
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.mjob.pekerjaan.dt2ktr} 
                                        name="dt2ktr"
                                        onChange={handleChangeK}
                                        select>
                                            {
                                            kab.map((h) => 
                                            (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                            }
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Kecamatan"
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.mjob.pekerjaan.kcmtnktr} 
                                        name="kcmtnktr"
                                        onChange={handleChangeK}
                                        select>
                                            {
                                            kec.map((h) => 
                                            (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                            }
                                    </TextField>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Kelurahan" 
                                        variant="standard" 
                                        required
                                        size="small"
                                        fullWidth
                                        value={this.props.mjob.pekerjaan.klrhktr}
                                        name="klrhktr"
                                        onChange={handleChangeK}
                                        select>
                                            {
                                            kel.map((h) => 
                                            (<MenuItem key={h} value={h}>{h}</MenuItem>))
                                            }
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md='6'>
                                <Form.Group>
                                    <TextField
                                    label="Alamat Kantor"
                                    required
                                    fullWidth
                                    value ={this.props.mjob.pekerjaan.almktr}
                                    name="almktr"
                                    onChange={handleChangeK}
                                    variant="standard"
                                    size="small"
                                    helperText='Diisi nama jalan dan nomor'
                                    />
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField disabled
                                    label="Kode Pos"
                                    //type="number"
                                    //required
                                    fullWidth
                                    value ={this.props.mjob.pekerjaan.kdposktr}
                                    name="kdposktr"
                                    onChange={handleChangeK}
                                    variant="standard"
                                    size="small"
                                    helperText={this.props.mjob.loading && (
                                        <span className="spinner-border spinner-border-sm"></span>
                                     )}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'50%',textAlign:'left'}}><h5>Kerabat yang bisa dihubungi</h5><small>*Tidak tinggal serumah dengan nasabah</small></td><td style={{width:'50%',textAlign:'right'}}></td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                                <Form.Group>
                                    <TextField
                                        label="Nama Lengkap"
                                        required
                                        fullWidth
                                        size="small"
                                        value={this.props.mfam.kerabat.namaktpf}
                                        name="namaktpf"
                                        onChange={handleChangeF}
                                        variant="standard"
                                        //helperText="Tinggal"
                                    />
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField id="fhubn"
                                        variant="standard" 
                                        required
                                        fullWidth
                                        size="small"
                                        value={this.props.mfam.kerabat.hubnsbf}
                                        onChange={handleChangeF}
                                        label="Hubungan Dengan Nasabah"
                                        name="hubnsbf"
                                        select>
                                            <MenuItem value={'1'}>Orang Tua</MenuItem>
                                            <MenuItem value={'2'}>Saudara Kandung</MenuItem>
                                            <MenuItem value={'3'}>Anak Kandung</MenuItem>
                                            <MenuItem value={'4'}>Paman/Bibi</MenuItem>
                                            <MenuItem value={'5'}>Kakek/Nenek</MenuItem>
                                            <MenuItem value={'6'}>Ipar</MenuItem>
                                            <MenuItem value={'7'}>Mertua</MenuItem>
                                            <MenuItem value={'8'}>Menantu</MenuItem>
                                            <MenuItem value={'9'}>Keponakan</MenuItem>
                                            <MenuItem value={'10'}>Sepupu</MenuItem>
                                    </TextField>
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <TextField 
                                        label="Nomor Telepon Rumah"
                                        required
                                        fullWidth
                                        size="small"
                                        value={this.props.mfam.kerabat.notlpf}
                                        name="notlpf"
                                        onChange={handleChangeF}
                                        variant="standard"
                                        //placeholder="(555) 5555-5555"
                                        error = {checkNoTlp()}
                                        helperText={notlpLenght? "Dapat diisi dengan No.HP jika tidak ada No.Telp Rumah":checkNoTlp()}
                                    />
                                </Form.Group>
                            </Col>
                            <Col md>
                                <Form.Group>
                                    <NumberFormat customInput={TextField}
                                        // format="################"
                                        hinttext=""
                                        placeholder=""
                                        label="Nomor HP"
                                        //type="number"
                                        required
                                        fullWidth
                                        size="small"
                                        value={this.props.mfam.kerabat.nohpf}
                                        name="nohpf"
                                        onChange={handleChangeF}
                                        variant="standard"
                                        error={checkNoHp()}
                                        helperText={checkNoHp()}
                                        allowLeadingZeros = {true}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-dark btn-sm" 
                                            type='button' 
                                            id='back'
                                            onClick={this.back}
                                            title='Kembali'>Kembali</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            type='submit' 
                                            id='continue'
                                            disabled={checkNoTlp() || checkNoHp()}
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformKerabat

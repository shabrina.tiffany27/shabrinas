import React, { Component } from 'react'
import "./Prohajj.css"

export class Footer extends Component {
    render() {
        const style = { width: "30px" }
        return (
            <div className="main-footer">
                <div className="container">
                    <div className="row">
                        
                        {/*<div className="col">
                            <b>Muamalat Tower</b>
                            <ul className="list-unstyled">
                                <li><span className="small">Jl.Prof Dr Satrio, Kav.18</span></li>
                                <li><span className="small">Kuningan Timur, Setiabudi</span></li>
                                <li><span className="small">Jakarta Selatan 12940</span></li>
                                <li><span className="small">Telp: (021)80666000</span></li>
                                <li><span className="small">Fax: (021)80666001</span></li>
                            </ul>
                        </div>*/}
                        <div className="col">
                            <a href="https://wa.me/6281280651800" rel='noopener noreferrer' className="float" target="_blank">
                            <img src="./assets/WA.png" style={ style } className="rounded float-center" label='Salma' alt='WhatsApp' /></a>
                            <ul className="list-unstyled text-center">
                                <li><span className="small">WhatsApp</span></li>
                                <li><span className="small">0812-8065-1800</span></li>
                            </ul>
                        </div>
                        <div className="col">
                            <a href="mailto:info@bankmuamalat.co.id" rel='noopener noreferrer' className="float" target="_blank">
                            <img src="./assets/Email.png" style={ style } className="rounded float-center" label='Salma' alt='Email' /></a>
                            <ul className="list-unstyled text-center">
                                <li><span className="small">Email</span></li>
                                <li><span className="small">info@bankmuamalat.co.id</span></li>
                            </ul>
                        </div>
                        <div className="col">
                            <a href="https://www.instagram.com/bank.muamalat/" rel='noopener noreferrer' className="float" target="_blank">
                            <img src="./assets/Instagram.png" style={ style } className="rounded float-center" label='Salma' alt='Instagram' /></a>
                            <ul className="list-unstyled text-center">
                                <li><span className="small">Instagram</span></li>
                                <li><span className="small">@bank.muamalat</span></li>
                            </ul>
                        </div>
                        <div className="col">
                            <a href="https://twitter.com/BankMuamalat" rel='noopener noreferrer' className="float" target="_blank">
                            <img src="./assets/Twitter.png" style={ style } className="rounded float-center" label='Salma' alt='Twitter' /></a>
                            <ul className="list-unstyled text-center">
                                <li><span className="small">Twitter</span></li>
                                <li><span className="small">@bankmuamalat</span></li>
                            </ul>
                        </div>
                        <div className="col">
                            <a href="https://www.facebook.com/BankMuamalatIndonesia" rel='noopener noreferrer' className="float" target="_blank">
                            <img src="./assets/Facebook.png" style={ style } className="rounded float-center" label='Salma' alt='Facebook' /></a>
                            <ul className="list-unstyled text-center">
                                <li><span className="small">Facebook</span></li>
                                <li><span className="small">Bank Muamalat</span></li>
                            </ul>
                        </div>
                        <div className="col">
                            <a href="https://www.youtube.com/channel/UCtqkVl3ce3k2PoIITVQJ8_g" rel='noopener noreferrer' className="float" target="_blank">
                            <img src="./assets/Youtube.png" style={ style } className="rounded float-center" label='Salma' alt='Youtube' /></a>
                            <ul className="list-unstyled text-center">
                                <li><span className="small">Youtube</span></li>
                                <li><span className="small">Bank Muamalat</span></li>
                            </ul>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <p className="col-sm">
                        <span className="xsmall">Bank Muamalat terdaftar dan diawasi oleh Otoritas Jasa Keuangan<br/>
                            &copy;2020 PT Bank Muamalat Tbk. | All Rights Reserved.
                        </span>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer

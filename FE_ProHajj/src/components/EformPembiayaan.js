import React, { Component } from 'react'
import {Container, Row, Col, Card, Form } from 'react-bootstrap'
import './Prohajj.css';
import Header from './Header'
import {TextField, Select, InputLabel, FormControl} from '@material-ui/core/';
import { ToastContainer, toast } from 'react-toastify';
import NumberFormat from 'react-number-format';
import ModalConfirm from './ModalConfirm';
import ProHajjServices from '../services/ProHajjServices'
 
export class EformPembiayaan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mssge : '',val : ''
    }}
    refreshPage() { window.location.reload(false); }
    continue = e => {
        e.preventDefault();
        if(this.state.sameNik !== true){
            this.props.handleSaveTemp();
            this.props.nextStep(1);
        } else{
            toast.error('Nik KTP tidak sesuai')
        }
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep(1);
    }
    insertnol = (e) => {
        var snol = '';
        if (e < 10){
            snol = '0'+e;
        }else {
            snol = e;
        }
        return snol;
      }
    dateNow = (e) => {
         //value={ this.dateNow(this.props.form1b.pemohon.tgllhr) } 
        //var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",  "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        var sdate = new Date();
        var tgl = new Date(e);
        var sRet = null
        if (e === '') {
            sRet =  sdate.getFullYear()+ "-"+ this.insertnol(sdate.getMonth()+1) + "-" +this.insertnol(sdate.getDate());
        }else{
            sRet =  tgl.getFullYear()  + "-" + this.insertnol(tgl.getMonth()+1) + "-" + this.insertnol(tgl.getDate()) ;
        }
        return sRet;
      }

    componentDidMount() {
        this.props.onTop();
    }

    fPlafond= (e) => {
        var porsi = this.props.pbypmhn.jmlpor;
        var mountp = 25000000;
        var max = porsi * mountp; 
        var vle = (e?parseInt(e):parseInt(e.replace(/[^\w,]/gi, "").replace(/[a-z]/gi,'')));
        var msg = null;
        var val = null;
        if(porsi !== '' && vle > max) {
            msg = 'Jumlah Porsi : '+porsi+', Maksimal Plafond yg bisa diajukan '+this.props.toCurrency(max);
            val = '';
            
        }else{
            msg = '';
            val = vle ;
        }
            return { msg, val };
    }

    plaholPlafon() {
        var porsi = this.props.pbypmhn.jmlpor;
        var mountp = 25000000;
        var max = porsi * mountp; 
        
        //return max;
        return this.props.toCurrency(max);
    }
    
    mPlafond= (e) => {
        let vle = e.replace(/[^\w,]/gi, "").replace(/[a-z]/gi,'')
        if (vle.substring(0,1) !== 0 && parseInt(vle) < 5000000) {
            this.setState({mssge : 'Minimum Plafond yg diajukan Rp. 5.000.000', val: ''}) ;
        }else{
            this.setState({mssge : '',val: ''});
        }
    }
    
    fPorsi= (e) => {
        var max = 4;
        var msg = null;
        var val = null;
        if(e > max) {
            msg = 'Maksimal 4 Porsi Haji yang diajukan';
            val = '';
        }else{
            msg = '';
            val = e;
        }
        return { msg, val};
    }

    cekNik = () => {
        ProHajjServices.postNik(this.props.flayer.accnumktp)
        .then((res) => {
            if(this.props.pbypmhn.pemohon.nikktp !== res.data.no_KTP){
                // toast.error('Nik KTP tidak sesuai')
                this.setState({sameNik:true})
            } else{
                this.setState({sameNik:false})
            }
        })
    }

    render() {
        const { handleChangeN, handleChangeP, emailValidation, handleSaveTemp, calculate_age} = this.props;
        let plafond = this.fPlafond(this.props.pbypmhn.plafond);
        let msgPlafond = (plafond.msg?plafond.msg:this.state.mssge);
        let valPlafond = (plafond.val?plafond.val:this.state.val);
        let porsi = this.fPorsi(this.props.pbypmhn.jmlpor);
        let msgPorsi = (porsi.msg?porsi.msg:'');
        let valPorsi = porsi.val;
        const npwpLength = (this.props.pbypmhn.pemohon.npwp.length > 0 && this.props.pbypmhn.pemohon.npwp.length < 15) || this.props.pbypmhn.message !== ''
        const isEnabled =  (this.props.pbypmhn.plafond > 50000000)
        //const npwpLength =((this.props.pbypmhn.pemohon.npwp?this.props.pbypmhn.pemohon.npwp.length !== 15:this.props.pbypmhn.pemohon.npwp) && this.props.pbypmhn.pemohon.npwp !== '')
        const mailError = (this.props.pbypmhn.pemohon.email?emailValidation(this.props.pbypmhn.pemohon.email):'');
        const svDisabled = (msgPlafond || mailError || this.props.pbypmhn.pemohon.email === '' ||  this.props.pbypmhn.pemohon.namalkp === '' || this.props.pbypmhn.pemohon.nikktp === '' || this.props.pbypmhn.pemohon.tgllhr === '')
        const nikktperror = (this.props.pbypmhn.pemohon.nikktp.length > 0 && this.props.pbypmhn.pemohon.nikktp.length < 16) || this.props.pbypmhn.message !== ''
        // const errusia = (calculate_age(this.props.pbypmhn.pemohon.tgllhr)<11);
        // const usiacheck = (errusia ?'Silahkan check Tahun Lahir':'Format : Tanggal / Bulan / Tahun (dd/mm/yyyy)');
        const namaLengkapLength = (this.props.pbypmhn.pemohon.namalkp.length > 0 && this.props.pbypmhn.pemohon.namalkp.length > 255) || this.props.pbypmhn.message !== ''
        const nmnoglrLength = (this.props.pbypmhn.pemohon.namanoglr.length > 0 && this.props.pbypmhn.pemohon.namanoglr.length > 255) || this.props.pbypmhn.message !== ''
        const checknik = (nikktperror ? 'NIK Harus 16 Digit' : '');
        const usiacheck = () => {
            if (calculate_age(this.props.pbypmhn.pemohon.tgllhr)<21) {
                return 'Minimum umur saat pembiayaan jatuh tempo adalah 21 tahun'
            } else if(calculate_age(this.props.pbypmhn.pemohon.tgllhr) + (this.props.pbypmhn.tenor/12)> 65){
                return 'Maksimum umur saat pembiayaan jatuh tempo adalah 65 tahun'
            }
        }
        const checknpwp = (npwpLength ? 'Nomor NPWP harus 15 Digit' : 'Hanya Untuk Pengajuan > 50');
        const checkNoHp = () => {
            if(this.props.pbypmhn.pemohon.nohp1.length > 0) {
                if(this.props.pbypmhn.pemohon.nohp1[0] !== '0' || this.props.pbypmhn.pemohon.nohp1[1] !== '8'){
                    return 'Nomor HP tidak valid, harap diawali dengan 08'
            }
                else if(this.props.pbypmhn.pemohon.nohp1.length < 10) {
                    return "Nomor HP tidak valid, digit terlalu sedikit"
                }
                else if (this.props.pbypmhn.pemohon.nohp1.length > 13) {
                    return"Nomor HP tidak valid, digit terlalu banyak"
                }
            }
        }
        const nohp1Length = (this.props.pbypmhn.pemohon.nohp1.length === 0 || this.props.pbypmhn.pemohon.nohp1.length >= 10 && this.props.pbypmhn.pemohon.nohp1.length < 14)
        const nohp1Msge = () => {
            if(this.props.pbypmhn.pemohon.nohp1.length === 0 || this.props.pbypmhn.pemohon.nohp1[0] === '0' && this.props.pbypmhn.pemohon.nohp1[1] === '8'){
                return 'Harap pastikan nomor HP aktif pada Whatsapp dan SMS'
            } else {
                return 'Nomor HP tidak valid, harap diawali dengan 08'
            }
        }
        const nohp2Lenght = (this.props.pbypmhn.pemohon.nohp2.length === 0 || this.props.pbypmhn.pemohon.nohp2.length >= 10 && this.props.pbypmhn.pemohon.nohp2.length < 14)
        const nohp2Msge = () => {
            if(this.props.pbypmhn.pemohon.nohp2.length === 0 || this.props.pbypmhn.pemohon.nohp2[0] === '0' && this.props.pbypmhn.pemohon.nohp2[1] === '8'){
                return 'Harap pastikan nomor HP aktif pada Whatsapp dan SMS'
            } else {
                return 'Nomor HP tidak valid, harap diawali dengan 08'
            }
        }
        const checkNoHp2 = () => {
            if(this.props.pbypmhn.pemohon.nohp2.length > 0) {
                if(this.props.pbypmhn.pemohon.nohp2[0] !== '0' || this.props.pbypmhn.pemohon.nohp2[1] !== '8'){
                    return 'Nomor HP tidak valid, harap diawali dengan 08'
                }
                else if(this.props.pbypmhn.pemohon.nohp2.length < 10) {
                    return "Nomor HP tidak valid, digit terlalu sedikit"
                }
                else if (this.props.pbypmhn.pemohon.nohp2.length > 13) {
                    return "Nomor HP tidak valid, digit terlalu banyak"
                }
            } 
        }

        return (
            <div>
                <Header prod = {this.props.pbypmhn.kd_produk} eprod = {this.props.pbypmhn.etypeproduk}/>
                <Container>
                <ModalConfirm 
                    handleModalbtl = {this.props.handleModalbtl}
                    mbtl = {this.props.pbypmhn.batal}/>
                <Form onSubmit={this.continue} >
                <Card className="shadow p-3 mb-5 mr-1 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'50%',textAlign:'left'}}><h5>Fasilitas Pembiayaan</h5></td><td style={{width:'50%',textAlign:'right'}}>Page 1-1</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row > 
                            <Col md>
                            <Form.Group>
                                <FormControl 
                                variant="standard" 
                                required
                                size="small"
                                fullWidth>
                                <InputLabel >Jumlah Porsi Haji yang Diajukan</InputLabel>
                                    <Select                                    
                                    native
                                    value ={valPorsi}
                                    onChange={handleChangeN("jmlpor")}
                                    label="Jumlah Porsi Haji yang Diajukan"
                                    name="jmlpor"
                                    placeholder='Maximum 4 Porsi Haji yang Diajukan'
                                    //inputProps={{style: {  fontSize: '16px'}}}
                                        >
                                        <option value={""} ></option>
                                        <option value={1}>1 Orang</option>
                                        <option value={2}>2 Orang</option>
                                        <option value={3}>3 Orang</option>
                                        <option value={4}>4 Orang</option>
                                    </Select>
                                </FormControl>
                                <Form.Text className='text-danger text-left' id="jmlpor"><small>{msgPorsi}</small></Form.Text>
                            </Form.Group>
                            </Col>
                            <Col md>
                            <Form.Group>
                                <FormControl 
                                variant="standard" 
                                required
                                size="small"
                                fullWidth>
                                <InputLabel >Jangka Waktu Pembiayaan</InputLabel>
                                    <Select                                    
                                    native
                                    value={this.props.pbypmhn.tenor} 
                                    onChange={handleChangeN('tenor')}
                                    label="Jangka Waktu Pembiayaan"
                                    name="tenor"
                                    //inputProps={{style: {  fontSize: '16px'}}}
                                        >
                                        <option value={""} ></option>
                                        <option value={12}>12 bulan</option>
                                        <option value={24}>24 bulan</option>
                                        <option value={36}>36 bulan</option>
                                        <option value={48}>48 bulan</option>
                                        <option value={60}>60 bulan</option>
                                    </Select>
                                </FormControl>
                            </Form.Group>
                            </Col>
                            <Col md>
                            <Form.Group>
                                <NumberFormat customInput={TextField} 
                                    label="Plafond Qardh"
                                    required
                                    fullWidth
                                    size="small"
                                    onBlur={(e) => (this.mPlafond(e.target.value))}
                                    disabled={!valPorsi}
                                    value={valPlafond}
                                    placeholder={this.plaholPlafon()}
                                    name="plafond"
                                    onChange={handleChangeN("plafond")}
                                    variant="standard"
                                    error={Boolean(msgPlafond)}
                                    helperText={(!msgPlafond?'Min. Rp5.000.000 - Maks. Rp100.000.000':msgPlafond)}
                                     thousandSeparator={'.'} 
                                     decimalSeparator={','}
                                     prefix={'Rp'} 
                                />
                            </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                    <Card.Header>
                    <div className="d-flex left-content-start">
                        <table style={{width:'100%'}}>
                        <tbody>
                        <tr>
                            <td style={{width:'50%',textAlign:'left'}}><h5>Data Pemohon</h5></td><td style={{width:'50%',textAlign:'right'}}></td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md>
                                <Form.Group>
                                <TextField
                                    label="Nama Lengkap Sesuai KTP"
                                    required
                                    fullWidth
                                    value ={this.props.pbypmhn.pemohon.namalkp}
                                    name="namalkp"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    // error={Boolean(namaLengkapLength)}
                                />
                                </Form.Group>
                            </Col>
                            <Col md={4}>
                                <Form.Group>
                                <TextField
                                    label="Nama Tanpa Singkatan dan Gelar"
                                    required
                                    fullWidth
                                    value ={this.props.pbypmhn.pemohon.namanoglr}
                                    name="namanoglr"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    size="small"
                                    // error={Boolean(nmnoglrLength)}
                                />
                                </Form.Group>
                            </Col>
                            <Col md={2}>
                                <Form.Group>
                                    <TextField
                                    label="NIK e-KTP"
                                    //type="number"
                                    required
                                    fullWidth
                                    size="small"
                                    value={this.props.pbypmhn.pemohon.nikktp}
                                    name="nikktp"
                                    onChange={(e) => {
                                        handleChangeP(e)
                                        this.cekNik()
                                    }}
                                    variant="standard"
                                    error={Boolean(nikktperror)}
                                    helperText={checknik}
                                />
                                <Form.Text className='text-danger text-left'>{this.props.pbypmhn.loading && (
                                    <span className="spinner-border spinner-border-sm"></span>
                                )}{this.props.pbypmhn.message}</Form.Text>
                                </Form.Group>
                            </Col>
                            <Col md={3}>
                                <Form.Group>
                                <TextField
                                    label="Tempat Lahir"
                                    required
                                    fullWidth
                                    size="small"
                                    value={this.props.pbypmhn.pemohon.tmplhr}
                                    name="tmplhr"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    //helperText={msgPlafond}
                                />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                        <Col md>
                                <Form.Group>
                                    <TextField
                                        id="date"
                                        label="Tanggal Lahir"
                                        name="tgllhr"
                                        type="date"
                                        required
                                        fullWidth
                                        size="small"
                                        onChange={handleChangeP} 
                                        value={this.props.pbypmhn.pemohon.tgllhr}
                                        InputLabelProps={{ shrink: true }} 
                                        //defaultValue={this.dateNow(this.props.form1b.pemohon.tgllhr)}
                                        //helperText="Format : Tanggal / Bulan / Tahun (dd/mm/yyyy)"
                                        variant="standard"
                                        error={usiacheck()}
                                        helperText={usiacheck()}
                                    />
                                </Form.Group>
                            </Col>
                            <Col md>
                                 <Form.Group>
                                    <TextField 
                                    label="Nomor NPWP"
                                    //type="number"
                                    required
                                    fullWidth
                                    size="small"
                                    disabled={!isEnabled}
                                    value={this.props.pbypmhn.pemohon.npwp}
                                    name="npwp"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    error={Boolean(npwpLength)}
                                    helperText={checknpwp}
                                    />
                                </Form.Group>
                            </Col>
                            <Col md >
                            <Form.Group>
                                <TextField
                                    label="Nomor HP 1"
                                    //type="number"
                                    required
                                    fullWidth
                                    size="small"
                                    value={this.props.pbypmhn.pemohon.nohp1}
                                    name="nohp1"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    error = {checkNoHp()}
                                    helperText={nohp1Length ? nohp1Msge() : checkNoHp()}
                                />
                            </Form.Group>
                            </Col>
                            <Col md>
                            <Form.Group>
                                <TextField
                                    label="Nomor HP 2"
                                    //type="number"
                                    required
                                    fullWidth
                                    size="small"
                                    value={this.props.pbypmhn.pemohon.nohp2}
                                    name="nohp2"
                                    onChange={handleChangeP}
                                    variant="standard"
                                    error = {checkNoHp2()}
                                    helperText={nohp2Lenght ? nohp2Msge() : checkNoHp2()}
                                />
                            </Form.Group>
                            </Col>
                            <Col md>
                              <Form.Group >
                                <TextField
                                  label="Alamat Email"
                                  type='email'
                                  required
                                  fullWidth
                                  size="small"
                                  value={this.props.pbypmhn.pemohon.email}
                                  name="email"
                                  onChange={handleChangeP}
                                  variant="standard"
                                  placeholder='email@domain.com'
                                  error={Boolean(mailError)}
                                  helperText={mailError}
                                />
                              </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        {this.props.loadspin && (
                        <span className="spinner-grow spinner-grow-sm" style={{color: "green"}}/>                       
                        )}{!this.props.loadspin && (
                        <div className="d-flex left-content-start">
                                <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-primary btn-sm" 
                                            disabled={msgPlafond || nikktperror || npwpLength || usiacheck() || checkNoHp() || checkNoHp2() || npwpLength}
                                            type='submit' 
                                            id='continue'
                                            //onClick={this.continue}
                                            title='Lanjut'>Lanjut</button>
                                    </td>
                                    <td style={{textAlign:'center'}} hidden={true}>
                                        <button className="btn btn-outline-success btn-sm" 
                                            disabled={svDisabled}
                                            type='button' 
                                            id='savedraft'
                                            onClick={handleSaveTemp}
                                            title='Save Draft'>Save</button>
                                    </td>
                                    <td style={{textAlign:'center'}}>
                                        <button className="btn btn-outline-danger btn-sm" 
                                            type='button'
                                            id='batal'
                                            onClick={this.props.handleModalbtl}
                                            title='Batal'>Batal</button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                        </div>)}
                    </Card.Footer>
                </Card>
                </Form>
                </Container>
            </div>
        )
    }
}

export default EformPembiayaan

import React, { Component } from 'react'
import {Container, Row, Col, Card, Modal,Image,Form } from 'react-bootstrap';
import dataBankLain from '../csvjson.json';
//import Box from '@material-ui/core/Box';

export class ProhajjReview extends Component {
    render() { 
            const isPlafond = this.props.mdata.plafond > 50000000;
            const isSame = this.props.mdata.pemohon.almnowktp==='sda'
            const almtsaatini = (isSame?'Sesuai KTP':'Berbeda dengan KTP')
            const surt = (this.props.mdata.pemohon.almsurat)
            const surat = (surt==='10'?'Alamat KTP':surt==='20'?'Alamat Tinggal':surt==='30'?'Alamat Kantor':surt==='40'?'Alamat Email':'')
            const pddval = (this.props.mdata.pemohon.pendidikan)
            const pendidikan = (pddval==='00'?'SD':pddval==='01'?'SMP':pddval==='02'?'SMA':pddval==='03'?'Diploma':pddval==='04'?
                                'S-1':pddval==='05'?'S-2':pddval==='06'?'S-3':'Lainnya')
            //const gender = (this.props.mdata.pemohon.jklm==='L'?'Pria':this.props.mdata.pemohon.jklm==='P'?'Wanita':'')
            const stsnk = (this.props.mdata.pemohon.stskwn)
            const stskawin = (stsnk==='10'?'Menikah':stsnk==='20'?'Lajang':stsnk==='50'?'Menikah (Pisah Harta)':stsnk==='30'?'Cerai Hidup':stsnk==='40'?'Cerai Mati':'')
            const negara = (this.props.mdata.pemohon.wngr==='WNA'?'WNA':'WNI')
            const jskrj = (this.props.mdata.pekerjaan.jnspkjfos)
            const jenisp = (jskrj==='2' || jskrj==='4')
            const jeniskerja = (jskrj==='1'?'Karyawan':jskrj==='2'?'Self Employee':jskrj==='3'?'Profesional (Karyawan)':jskrj==='4'?'Profesional (Self Employee)':'')
            const stskerja = (jenisp ?'Pemilik Usaha':this.props.mdata.pekerjaan.stspkj==='CON'?'Karyawan Tetap':this.props.mdata.pekerjaan.stspkj==='FREE' ? 'Karyawan Kontrak' : '')
            console.log('status pekerjaan review: ',this.props.mdata.pekerjaan.stspkj)
            console.log('bidang ktr: ',this.props.mdata.pekerjaan.bidangktr)
            console.log('gajinya: ',this.props.mdata.pekerjaan.pbygaji)
            const ktinstsi = (this.props.mdata.pekerjaan.ktgrinst)
            const ktlain = (this.props.mdata.pekerjaan.ktgrinstlain)
            const instansi = (ktinstsi==='1'?'Pemerintah':ktinstsi==='2'?'BUMN':ktinstsi==='3'?'Swasta Asing':ktinstsi==='4'?'Swasta Nasional':ktinstsi==='5'?'TNI/POLRI':ktinstsi==='99'?ktlain:'')
            const nfincome = (this.props.mdata.pekerjaan.phasiltdktetap)
            const incomenf = (nfincome==='1'?'<Rp10 Juta':nfincome==='2'?'>Rp10 Juta <Rp50 Juta':nfincome==='3'?'>Rp50 Juta':'')
            const nfpluar = (this.props.mdata.pekerjaan.pluartetap)
            const pluarnf = (nfpluar==='1'?'<Rp10 Juta':nfpluar==='2'?'>Rp10 Juta <Rp50 Juta':nfpluar==='3'?'>Rp50 Juta':'')
            const aincome = (this.props.mdata.pekerjaan.phasiltambah)
            const lincome = (this.props.mdata.pekerjaan.phasiltambahlain)
            const incomeadd = (aincome==='1'?'Kerja Paruh Waktu':aincome==='2'?'Hasil Usaha':aincome==='3'?'Hasil Sewa':
                                aincome==='4'?'Deviden':aincome==='5'?'Investasi':aincome==='6'?'Warisan':
                                aincome==='0'?lincome:'')
            const hubnf = (this.props.mdata.kerabat.hubnsbf)
            const hubungan = (hubnf==='1'?'Orang Tua':hubnf==='2'?'Saudara Kandung':hubnf==='3'?'Anak Kandung':
                                hubnf==='4'?'Paman/Bibi':hubnf==='5'?'Kakek/Nenek':hubnf==='6'?'Ipar':
                                hubnf==='7'?'Mertua':hubnf==='8'?'Menantu':hubnf==='9'?'Keponakan':
                                hubnf==='10'?'Sepupu':'')
            const URL_API = this.props.mdata.urlimgdoc;
            const bdgUsha = this.props.mjob.bdgush;
            let bdgushrvw = (this.props.mdata.pekerjaan.bidangktr === '' ? '' : bdgUsha.find(o => o.bidangcd === this.props.mdata.pekerjaan.bidangktr).bidangnama);
            let pbygjlain = dataBankLain.find(o => o.BANK_CODE === this.props.mdata.pekerjaan.pbygajikode).BANK_DESC;
            const pbygj = (this.props.mdata.pekerjaan.pbygaji === 'Bank Muamalat' || this.props.mdata.pekerjaan.pbygaji === 'Tunai') ? this.props.mdata.pekerjaan.pbygaji : pbygjlain

          return (
            <Modal 
                size='xl'
                show={this.props.mview}
                onHide={()=>this.props.handleModalrview}
                backdrop="static"
                keyboard={false}>
            <Modal.Body>
                
                <Container >
                    <div className="p-2 col-example text-center">
                        <h5>REVIEW DATA</h5>
                    </div>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <div className="p-2 col-example text-center" dangerouslySetInnerHTML={{__html: "<b>FASILITAS PEMBIAYAAN</b>"}}/>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{cellpadding:'0', cellspacing:'0', width:'100%'}} >
                                <tbody>
                                <tr >
                                    <td>Jumlah Porsi Haji yang Diajukan</td>
                                    <td align='right'><i><b>{this.props.mdata.jmlpor} Orang</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Jangka Waktu Pembiayaan</td>
                                    <td align='right'><i><b>{this.props.mdata.tenor} bulan</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Plafond Qardh</td>
                                    <td align='right'><i><b>{this.props.toCurrency(this.props.mdata.plafond)}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <div className="p-2 col-example text-center" dangerouslySetInnerHTML={{__html: "<b>DATA PEMOHON</b>"}}/>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nama Lengkap Sesuai KTP</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.namalkp}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nama Tanpa Singkatan dan Gelar</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.namanoglr}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>NIK e-KTP</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.nikktp}</b></i> </td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Tempat Lahir</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.tmplhr}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nomor NPWP</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.npwp}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nomor Rekening</td>
                                    <td align='right'><i><b>{this.props.mdata.accnumktp}</b></i> </td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nomor HP 1 (WA)</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.nohp1}</b></i> </td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nomor HP 2</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.nohp2}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Alamat Email</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.email}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>No.Telp. Rumah</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.notlpnow}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Pendidikan</td>
                                    <td align='right'><i><b>{pendidikan}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Jenis Kelamin</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.jklm}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Status Perkawinan</td>
                                    <td align='right'><i><b>{stskawin}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kewarganegaraan</td>
                                    <td align='right'><i><b>{negara}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Status Kependudukan</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.stspenduk}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Agama</td>
                                    <td align='right'><i><b>{'ISLAM'}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nama Gadis Ibu Kandung</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.ibukdg}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        </font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Alamat Sesuai KTP</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.almktp}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>RT / RW</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.ktprt+' / '+this.props.mdata.pemohon.ktprw}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kelurahan</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.klrhktp}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.kcmtktp}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kabupaten / Kota</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.dt2ktp}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Provinsi</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.dt1ktp}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kode Pos</td>
                                    <td align='right'><i><b>{this.props.mdata.pemohon.kdposktp}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Alamat Domisili</td>
                                    <td align='right'><i><b>{isSame? this.props.mdata.pemohon.almktp : this.props.mdata.pemohon.almnow}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>RT / RW</td>
                                    <td align='right'><i><b>{isSame? this.props.mdata.pemohon.ktprt+' / '+this.props.mdata.pemohon.ktprw : this.props.mdata.pemohon.nowrt+ '/' +this.props.mdata.pemohon.nowrw}</b></i></td>
                                    {/* <td align='right'><i><b>{isSame? this.props.mdata.pemohon.ktprt : this.props.mdata.pemohon.nowrt +' / '+ isSame? this.props.mdata.pemohon.ktprt : this.props.mdata.pemohon.nowrw}</b></i></td> */}
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kelurahan</td>
                                    <td align='right'><i><b>{isSame? this.props.mdata.pemohon.klrhktp : this.props.mdata.pemohon.klrhnow}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td align='right'><i><b>{isSame? this.props.mdata.pemohon.kcmtktp : this.props.mdata.pemohon.kcmtnow}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kabupaten / Kota</td>
                                    <td align='right'><i><b>{isSame? this.props.mdata.pemohon.dt2ktp : this.props.mdata.pemohon.dt2now}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Provinsi</td>
                                    <td align='right'><i><b>{isSame? this.props.mdata.pemohon.dt1ktp : this.props.mdata.pemohon.dt1now}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kode Pos</td>
                                    <td align='right'><i><b>{isSame? this.props.mdata.pemohon.kdposktp : this.props.mdata.pemohon.kdposnow}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%', marginTop: "24px"}}>
                                <tbody>
                                <tr>
                                    <td>Alamat saat ini</td>
                                    <td align='right'><i><b>{almtsaatini}</b></i></td>
                                </tr>
                                </tbody>
                        </table>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Alamat Surat Menyurat</td>
                                    <td align='right'><i><b>{surat}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                </Card>

                <Card className="shadow p-3 mb-5 bg-white rounded">
                <div className="p-2 col-example text-center" dangerouslySetInnerHTML={{__html: "<b>DATA PEKERJAAN PEMOHON</b>"}}/>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Jenis Pekerjaan</td>
                                    <td align='right'><i><b>{jeniskerja}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Status Pekerjaan</td>
                                    <td align='right'><i><b>{stskerja}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nama Perusahaan</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.namaprsh}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kategori Instansi</td>
                                    <td align='right'><i><b>{instansi}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Bidang Usaha</td>
                                    <td align='right'><i><b>{bdgushrvw}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Jabatan</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.jabatan}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md>
                            <font size='2'>
                            <table style={{width:'100%'}}>
                                    <tbody>
                                    <tr>
                                        <td>Lama Bekerja</td>
                                        <td align='right'><i><b>{this.props.mdata.pekerjaan.lmkrjathn+' Tahun, '+this.props.mdata.pekerjaan.lmkrjabln+' Bulan'}</b></i></td>
                                    </tr>
                                    </tbody>
                            </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Pendapatan Per Bulan</td>
                                    <td align='right'><i><b>{this.props.toCurrency(this.props.mdata.pekerjaan.pendptn)}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Pembayaran Gaji/Usaha</td>
                                    <td align='right'><i><b>{pbygj}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Penghasilan Tidak Tetap Per Bulan</td>
                                    <td align='right'><i><b>{incomenf}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Pengeluaran Tetap Per Bulan</td>
                                    <td align='right'><i><b>{pluarnf}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td align='right'><i><b></b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Informasi Penghasilan Tambahan</td>
                                    <td align='right'><i><b>{incomeadd}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>No.Telp. Kantor</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.tlpktrkd+' - '
                                                            +this.props.mdata.pekerjaan.tlpktr+' Ext.'
                                                            +this.props.mdata.pekerjaan.extktr}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>No.Telp. HRD</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.tlphrdkd+' - '
                                                            +this.props.mdata.pekerjaan.tlphrdktr+' Ext.'
                                                            +this.props.mdata.pekerjaan.exthrd}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Alamat Kantor</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.almktr}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kelurahan</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.klrhktr}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.kcmtnktr}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kabupaten / Kota</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.dt2ktr}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Provinsi</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.dt1ktr}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Kode Pos</td>
                                    <td align='right'><i><b>{this.props.mdata.pekerjaan.kdposktr}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <div className="p-2 col-example text-center" dangerouslySetInnerHTML={{__html: "<b>KERABAT YANG BISA DIHUBUNGI</b>"}}/>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nama Lengkap</td>
                                    <td align='right'><i><b>{this.props.mdata.kerabat.namaktpf}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Hubungan Dengan Nasabah</td>
                                    <td align='right'><i><b>{hubungan}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nomor Telepon Rumah</td>
                                    <td align='right'><i><b>{this.props.mdata.kerabat.notlpf}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md='4'>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Nomor HP</td>
                                    <td align='right'><i><b>{this.props.mdata.kerabat.nohpf}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <div className="p-2 col-example text-center" dangerouslySetInnerHTML={{__html: "<b>Deklarasi Common Reporting Standards (CRS) & (FATCA)</b>"}}/>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td rowspan="2">Anda memiliki Permanent Residence/Green Card?</td>
                                    <td rowspan="2" align='center'><i><b>{this.props.mdata.funding.fcrgcard}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td align='left'>{this.props.mdata.funding.fcrgcard==='YA'?'Negara':''}</td>
                                    <td align='right'><i><b>{this.props.mdata.funding.fcrgcard==='YA'?this.props.mdata.funding.fcrgcardcountry:''}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td rowspan="2"> </td>
                                    <td rowspan="2" align='center'><i><b> </b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td rowspan="2">Anda memiliki kewajiban pelaporan pajak di Negara luar Indonesia?</td>
                                    <td rowspan="2" align='center'><i><b>{this.props.mdata.funding.fctax}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td align='left'>{this.props.mdata.funding.fctax==='YA'?'Negara':''}</td>
                                    <td align='right'><i><b>{this.props.mdata.funding.fctax==='YA'?this.props.mdata.funding.fctaxcountry:''}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td align='left'>{this.props.mdata.funding.fctax==='YA'?'Tax Identification Number':''}</td>
                                    <td align='right'><i><b>{this.props.mdata.funding.fctax==='YA'?this.props.mdata.funding.fctaxid:''}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Apakah Anda memberikan surat Instruksi di Bank manapun untuk melakukan transfer dana pada rekening yang terdapat pada Bank di luar negara Indonesia?</td>
                                    <td align='right'><i><b>{this.props.mdata.funding.fctrsletter}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td align='right'><i><b></b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td align='right'><i><b></b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <div className="p-2 col-example text-center" dangerouslySetInnerHTML={{__html: "<b>Pembukaan Rekening Pembiayaan & Pendaftaran Porsi Haji</b>"}}/>
                <Row>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Jenis Produk</td>
                                    <td align='right'><i><b>{this.props.mdata.funding.jnsproduk}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Type Rekening</td>
                                    <td align='right'><i><b>{this.props.mdata.funding.typeacc}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Mata Uang</td>
                                    <td align='right'><i><b>{this.props.mdata.funding.currency}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                <Row>
                    <Col md='4'>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Sumber Dana</td>
                                    <td align='right'><i><b>{this.props.mdata.funding.sumberdana}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md='4'>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td>Tujuan Pembukaan Rekening</td>
                                    <td align='right'><i><b>{this.props.mdata.funding.tujuanacc}</b></i></td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                </Card>
                <Card className="shadow p-3 mb-5 bg-white rounded">
                <div className="p-2 col-example text-center" dangerouslySetInnerHTML={{__html:"<b>DOKUMEN NASABAH</b>" }}/>
                <Row>
                    <Col md='4'>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr><td align='center'><b>Image e-KTP</b></td></tr>
                                <tr><td align='center'>
                                    <div className="Image">
                                        <Image src={(!this.props.mdata.urlktp?URL_API+'/files/'+this.props.mdata.appid:this.props.mdata.urlktp)} width="50%" />
                                    </div>
                                </td></tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md hidden={!isPlafond}>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td align='center'><b>Image NPWP</b></td>
                                </tr>
                                <tr><td align='center'>
                                    <div className="Image">
                                        <Image src={isPlafond?(!this.props.mdata.urlnpwp?URL_API+'/filenpwp/'+this.props.mdata.appid:this.props.mdata.urlnpwp):'./assets/idcardempty.png'} width="50%" />
                                        <Form.Text className='text-muted text-center'><i>Pengajuan Pembiayaan diatas 50jt</i></Form.Text>
                                    </div>
                                </td></tr>
                                </tbody>
                        </table></font>
                    </Col>
                    <Col md>
                        <font size='2'>
                        <table style={{width:'100%'}}>
                                <tbody>
                                <tr>
                                    <td align='center'><b>Dokumen PDF</b></td>
                                </tr>
                                <tr hidden={jenisp}>
                                    <td align='left'>Slip Gaji Terakhir/Surat Ket. Penghasilan</td>
                                    <td align='right'>
                                        <button className="btn btn-outline-success btn-sm" 
                                        type='button' 
                                        disabled={jenisp}
                                        onClick={()=> window.open(URL_API+'/docgji/'+this.props.mdata.appid, "_blank")}
                                        title='view'><small>View</small></button>
                                    </td>
                                </tr>
                                <tr><td></td></tr>
                                <tr hidden={!jenisp}>
                                    <td align='left'>Dokumen Legalitas Usaha</td>
                                    <td align='right'>
                                        <button className="btn btn-outline-success btn-sm" 
                                        type='button' 
                                        disabled={!jenisp}
                                        onClick={()=> window.open(URL_API+'/doclgl/'+this.props.mdata.appid, "_blank")}
                                        title='view'><small>View</small></button>
                                    </td>
                                </tr>
                                <tr><td></td></tr>
                                <tr hidden={!jenisp}>
                                    <td align='left'>Mutasi Rekening 3 Bulan Terakhir</td>
                                    <td align='right'>
                                        <button className="btn btn-outline-success btn-sm" 
                                        type='button' 
                                        disabled={!jenisp}
                                        onClick={()=> window.open(URL_API+'/docmts/'+this.props.mdata.appid, "_blank")}
                                        title='view'><small>View</small></button>
                                    </td>
                                </tr>
                                </tbody>
                        </table></font>
                    </Col>
                </Row>
                </Card>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                    <button className="btn btn-outline-success btn-sm" 
                            type='button' 
                            id="clsmodal"
                            onClick={this.props.handleModalrview}
                            title='Kembali'>KEMBALI</button>
            </Modal.Footer>
            </Modal>
        )
    }
}

export default ProhajjReview
/*
<Form.Text className='text-success text-center'>
                                        <i>{<a target='_blank' rel='noreferrer noopener' href={URL_API+'/doclgl/'+this.props.mdata.appid}>View PDF'</a>}</i></Form.Text>
*/
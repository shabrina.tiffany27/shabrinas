import React, { Component } from 'react'
import ReactjsOverlayLoader from 'reactjs-overlay-loader';
//import Footer from './Footer';
import Header from './Header'

export class Loader extends Component {
    render() {
       //const logo = (this.props.prod === this.props.eprod ?"./assets/Logo-ALIF-Ver-2.png":"./assets/logobmi.ico")
        //const ownprod = (this.props.prod === this.props.eprod ?"ALIF":"BMI")
        return (
            <div>
                <ReactjsOverlayLoader
                    loaderContent={<span style={{ color: "#ffffff"  }}>
                    <img src="./assets/logobmi.ico" className="rounded float-center" alt="Logo" /><small> BMI</small></span>}
                    isActive
                    style={{
                    //backgroundColor: "rgba(233, 30, 99, 0.81)",
                    position: "absolute",
                    
                    }}              
                  >  <Header prod = {this.props.prod} eprod = {this.props.eprod}/>        
                </ReactjsOverlayLoader>
            </div>
        )
    }
}

export default Loader
/*
<p><br/><img src="./assets/loader.gif" alt=''/></p>;
*/
